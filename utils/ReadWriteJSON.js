const fs = require('fs');

const readJSON = (filePath) => {
  const jsonData = fs.readFileSync(filePath, 'utf8');
  const jsonObject = JSON.parse(jsonData);
  return jsonObject;
};

const writeJSON = (jsonObject, saveTo) => {
  const jsonData = JSON.stringify(jsonObject, null, 2);
  fs.writeFileSync(saveTo, jsonData, 'utf8');
};

module.exports = {
  readJSON,
  writeJSON,
};

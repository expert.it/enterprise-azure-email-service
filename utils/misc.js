const { logger } = require('../config/config');

const timeOut = (func, time, config) => {
  const { errorMessageOnTimeout } = config;
  return new Promise(async (resolve, reject) => {
    let timeOutRef;
    const waitPromise = new Promise((_, reject) => {
      timeOutRef = setTimeout(() => {
        logger.error(`Rejecting with error: ${errorMessageOnTimeout}`);
        reject(new Error(errorMessageOnTimeout));
      }, time);
    });

    try {
      console.log(`Waiting for ${time / 1000} secs before rejecting`);
      const returnVal = await Promise.race([func(), waitPromise]);
      clearTimeout(timeOutRef);
      resolve(returnVal);
    } catch (err) {
      logger.error(`Timeout wrapper error`);
      // console.log(err.message);
      reject(err);
    }

    // await func()
    //   .then((returnVal) => {
    //     clearTimeout(wait);
    //     console.log('resolved');
    //     // reject(new Error(errorMessageOnTimeout));
    //     // reject(new Error('Test Error'));
    //     resolve(returnVal);
    //   })
    //   .catch((err) => {
    //     clearTimeout(waitPromise);
    //     console.log('rejected');
    //     reject(err);
    //   });
  });
};

// returns a random no. between 0-(n-1)
const random = (n) => Math.floor(Math.random() * n + 1) - 1;

// get a uid of {length} chars
const ALLOWED_CHARS =
  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~0123456789-=!@#$%^&*_+';
const getUID = (length = 24) => {
  let uid = '';
  for (i = 1; i <= length; i += 1) {
    uid += `${ALLOWED_CHARS.charAt(
      Math.floor(Math.random() * ALLOWED_CHARS.length),
    )}`;
  }
  return uid;
};

module.exports = {
  timeOut,
  random,
  getUID,
};

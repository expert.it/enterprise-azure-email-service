class Mutex {
  constructor() {
    this.owner = null;
    this.locked = false;
    this.queue = [];
    this.priorityQueue = [];
    this.npQueue = [];
    this.npPriorityQueue = [];
  }

  getState() {
    const state = {
      locked: this.locked,
      owner: this.owner,
      npQueue: {
        size: this.npQueue.length,
        // queue: this.npQueue,
      },
      // queue: this.queue,
      npPriorityQueue: {
        size: this.npPriorityQueue.length,
        queue: this.npPriorityQueue,
      },
      // priorityQueue: this.priorityQueue,
    };
    return state;
  }

  async acquire(options) {
    const { workerId, priority } = options;
    return new Promise((resolve) => {
      if (!this.locked) {
        this.locked = true;
        this.owner = workerId;
        resolve(workerId);
      } else {
        if (priority) {
          this.priorityQueue.push(resolve);
          this.npPriorityQueue.push(workerId);
        } else {
          this.queue.push(resolve);
          this.npQueue.push(workerId);
        }
      }
    });
  }

  release() {
    const prevOwner = this.owner;
    if (this.queue.length + this.priorityQueue.length > 0) {
      let nextResolve;
      let workerId;
      if (this.priorityQueue.length) {
        nextResolve = this.priorityQueue.shift();
        workerId = this.npPriorityQueue.shift();
      } else {
        nextResolve = this.queue.shift();
        workerId = this.npQueue.shift(workerId);
      }
      this.owner = workerId;
      nextResolve(workerId);
    } else {
      this.owner = null;
      this.locked = false;
    }
    return prevOwner;
  }
}

const acquireMutex = async (mutex, options) => {
  const { uid, threadId } = options;
  while (true) {
    const lockAcquiredBy = await mutex.acquire({
      workerId: `Acq: ${uid}`,
    });
    const isLockAcqSuccess = lockAcquiredBy === mutex.owner;
    // logger.success(isLockAcqSuccess);
    // console.log({
    //   requestedBy: lockAcquiredBy,
    //   currOwner: mutex.owner,
    // });
    if (isLockAcqSuccess) {
      break;
    }
    // logger.warning(
    //   `[${threadId}] (${uid}) Failed to acquire lock, release failed!`,
    // );
  }
};

module.exports = {Mutex, acquireMutex};

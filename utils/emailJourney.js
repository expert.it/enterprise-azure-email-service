const { EMAIL_JOURNEY_TIMELINES } = require('../data/emailTemplates');
const { getDaysInMS } = require('./customTime');

const getNextStepInStage = (currentStage, currentStep) => {
  currentStep = parseInt(currentStep);
  const nextStepExists = Boolean(
    EMAIL_JOURNEY_TIMELINES[currentStage]?.[currentStep + 1],
  );
  return {
    stage: currentStage,
    currentStep,
    nextStep: nextStepExists ? currentStep + 1 : null,
  };
};

const getNextFollowupTimeDelayInMS = (stage, step) => {
  const res =
    EMAIL_JOURNEY_TIMELINES[stage]?.[step]?.timeToWaitSincePrevContactInMS;
  return res ? res : getDaysInMS(100 * 365);
};

module.exports = {
  getNextStepInStage,
  getNextFollowupTimeDelayInMS,
};

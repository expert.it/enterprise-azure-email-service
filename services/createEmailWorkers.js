const os = require('os');
const path = require('path');
const { default: axios } = require('axios');
// const fetch = (...args) =>
//   import('node-fetch').then(({ default: fetch }) => fetch(...args));
const { ObjectId } = require('mongodb');
const { Worker, workerData } = require('node:worker_threads');
const Campaign_Model = require('../models/campaign');
const { Mutex, acquireMutex } = require('../utils/MutexLock');
const Queue = require('../utils/Queue');
const mailboxesApi = require('./mailBoxes');

const moment = require('moment-timezone');
const { random, getUID } = require('../utils/misc');
const { writeJSON } = require('../utils/ReadWriteJSON');
const {
  getHrsInMS,
  getMinsInMS,
  getMsInMins,
  getDaysFromMins,
  getCurrTimeObj,
} = require('../utils/customTime');
const {
  logger,
  azureResourcePath: resConfFilePath,
  API_SERVICE_BACKEND,
  emailConfig,
  workerConfig: {
    workerPerCore,
    POLL_EVERY_X_MINS,
    RETRY_ON_ERROR_AFTER_X_MINS,
    RETRY_FOR_RESOURCES_IN_X_MINS,
  },
  followUpConfig,
  azureEmailService: {
    RESOURCE_COOLING_TIME_IN_HRS,
    RESOURCE_RESET_TIME_IN_HRS,
  },
  mailBoxesRampUpSchedule,
} = require('../config/config');

const isActiveHours = () => {
  const currESTTimeObj = getCurrTimeObj({
    timeZone: emailConfig.activeHrs.tz,
  });
  // console.log(emailConfig.activeHrs);
  // console.log(currESTTimeObj);
  const active =
    currESTTimeObj.hrs >= emailConfig.activeHrs.start &&
    currESTTimeObj.hrs < emailConfig.activeHrs.end;
  // console.log(`isActiveHours: ${active}`);
  return active;
};

// const resConfFilePath = path.join(
//   __dirname,
//   '..',
//   'config/azureResourcesConfig.json',
// );

// const addEmailsToDB = async () => {
//   const resConfig = require(resConfFilePath);
//   const { resourcePool } = resConfig;
//   const reqStatus = [];
//   console.log('Adding resources to DB');
//   resourcePool.slice(0, 10).forEach((resource, index) => {
//     const { connStr, senderEmail } = resource;
//     const promise = axios.post(`${API_SERVICE_BACKEND}/mailboxes`, {
//       payload: {
//         connectionString: connStr,
//         senderEmail,
//       },
//     });
//     reqStatus.push(promise);
//   });
//   const result = await Promise.all(reqStatus);
//   // console.log(result.map((res) => res.data));
// };
// addEmailsToDB();

const assignMailboxesIfReq = async ({ _id, email }) => {
  const assignedMailboxes = await mailboxesApi.getMailBoxes({
    filter: {
      'assigned.campaignId': _id,
    },
    projection: {
      _id: 1,
      // senderEmail: 1,
      // emailsSent: 1,
    },
  });

  const reqMailBoxCount =
    emailConfig.MAILBOXES_PER_CAMPAIGN - assignedMailboxes?.length || 0;
  if (reqMailBoxCount > 0) {
    logger.focus(
      `Assigning ${reqMailBoxCount} mailboxes to campaign: ${_id} (${email})`,
    );
    // throw new Error('break');
    const assignmentRes = await mailboxesApi.assignMailBoxes({
      email,
      campaignId: _id,
      limit: reqMailBoxCount,
    });
    logger.warning('Assignment Status:');
    console.log(assignmentRes);
  }
};

const freeResources = () => {
  // const resConfig = require(resConfFilePath);
  // for (let i = 0; i < resConfig.resourcePool.length; i += 1) {
  //   resConfig.resourcePool[i].free = true;
  // }
  // for (let i = 0; i < resConfig.updatePool.length; i += 1) {
  //   resConfig.updatePool[i].free = true;
  // }
  // logger.success(`Static resources freed!`);
  // writeJSON(resConfig, resConfFilePath);

  mailboxesApi
    .updateMailBoxes({
      filter: {
        all: true,
      },
      update: {
        $set: {
          free: true,
        },
      },
    })
    .then((res) => {
      console.log(res);
      if (res) {
        logger.success(`Resources freed!`);
      } else {
        logger.error('Error resetting the resources');
      }
    });
};

const allocateAzureResource = async ({ campaignId: _id }) => {
  // const resConfig = require(resConfFilePath);
  // const { resourcePool } = resConfig;
  // // console.log(resConfFilePath)
  // // console.log(`Resource pool: ${resourcePool.length}`);

  if (!_id) {
    logger.error('No campaignId found to allocate resource');
    return [false, null, null];
  }
  const resourcePool = await mailboxesApi.getMailBoxes({
    filter: {
      'assigned.campaignId': _id,
    },
    // projection: {
    //   password: 0,
    // },
  });
  // console.log(resourcePool);

  console.log(`Getting mailoxes for campaign: (${_id})`);
  const availableRes = [];
  let now;
  for (let i = 0; i < (resourcePool?.length || 0); i += 1) {
    const resource = resourcePool[i];

    const discardMsg = (customMsg) => {
      console.log(`Discarded email resource <${resource.senderEmail}>`);
      customMsg && logger.error(`${customMsg}`);
    };

    const {
      free,
      cooling,
      nextEmailAt,
      lastActive: activeDate,
      lastReset: resetDate,
    } = resource;
    const nextEmailAllowedAt = new Date(nextEmailAt || 0).getTime();
    const lastReset = new Date(resetDate).getTime();
    const lastActive = new Date(activeDate).getTime();

    now = Date.now();

    if (nextEmailAllowedAt > now) {
      discardMsg(
        `Next email delayed, try after ${getMsInMins(
          nextEmailAllowedAt - now,
        )} mins`,
      );
      continue;
    }
    // check if colling period completed
    const coolingDone =
      lastActive + getHrsInMS(RESOURCE_COOLING_TIME_IN_HRS) <= now;
    // console.log(coolingDone);

    // const nextReset = lastReset + getHrsInMS(RESOURCE_RESET_TIME_IN_HRS);
    const nextReset = moment(lastReset + getHrsInMS(RESOURCE_RESET_TIME_IN_HRS))
      .utc()
      .set({ hour: 5, minute: 0, second: 0, millisecond: 0 })
      .valueOf();

    const diff = Math.max(0, nextReset - now);
    if (diff <= 0) {
      resource.emailsSent = 0;
      resource.followUpsSent = 0;
      // const tempLastReset = new Date();
      // tempLastReset.setUTCHours(0);
      // tempLastReset.setUTCMinutes(0);
      // tempLastReset.setUTCSeconds(0);
      // tempLastReset.setUTCMilliseconds(0);
      // resource.lastReset = tempLastReset.toISOString();
      resource.lastReset = new Date().toISOString();
    }
    // console.log(resource);

    // rampup the no. of emails that can be sent!
    const lastRampUpAt = new Date(resource.lastWarmupUpdate).getTime();
    if (now >= lastRampUpAt + getHrsInMS(24 * 7)) {
      // // rampup by 2 after every week
      // resource.maxAllowed = Math.min(
      //   emailConfig.maxAllowedEmailsPerDay,
      //   resource.maxAllowed + emailConfig.incEmailCapBy,
      // );
      if (mailBoxesRampUpSchedule.weeks[resource.inWeek + 1]) {
        resource.inWeek += 1;
        resource.maxAllowed = Math.min(
          emailConfig.maxAllowedEmailsPerDay,
          mailBoxesRampUpSchedule.weeks[resource.inWeek],
        );
      }
      resource.lastWarmupUpdate = Date.now();
    } else {
      const timeRemainingInMins = getMsInMins(
        lastRampUpAt + getHrsInMS(24 * 7) - Date.now(),
      );
      console.log(
        `RampingUp(+${emailConfig.incEmailCapBy}) after ${getDaysFromMins(
          timeRemainingInMins,
        )} days  (${timeRemainingInMins} mins) <${resource.senderEmail}>`,
      );
    }

    // if (!free || resource.emailsSent >= Math.floor(resource.maxAllowed / 2)) {
    if (!free || resource.emailsSent >= Math.floor(resource.maxAllowed)) {
      const resetTimeRemainingInMins = getMsInMins(nextReset - now);
      discardMsg(
        `Consumed max allowed emails, try after ${getDaysFromMins(
          resetTimeRemainingInMins,
        )} days  (${resetTimeRemainingInMins} mins)`,
      );
      continue;
    }
    if (cooling) {
      // if (lastActive + getHrsInMS(RESOURCE_COOLING_TIME_IN_HRS) > Date.now())
      //   continue;
      if (!coolingDone) {
        discardMsg(`In cooling period.`);
        continue;
      }
      resource.cooling = false;
    }

    // const now = Date.now();
    // while (emailsSent.length && emailsSent[0] + getHrsInMS(RESOURCE_COOLING_TIME_IN_HRS) <= now) {
    //   emailsSent.shift();
    // }

    // if (emailsSent.length < LIMIT_PER_HOUR) {
    //   resource.free = false;
    //   writeJSON(resConfig, resConfFilePath);
    //   return [true, i, resource];
    // }

    availableRes.push([i, resource]);
  }

  if (!Boolean(availableRes.length)) return [false, null, null];

  const [index, selectedRes] = availableRes[random(availableRes.length)];
  // console.log(availableRes);
  selectedRes.free = false;

  // console.log(selectedRes);
  // return [false, null, null];

  const resourceUpdateRes = await mailboxesApi.updateMailBoxes({
    filter: {
      _id: selectedRes._id,
    },
    update: {
      $set: selectedRes,
    },
  });
  // console.log(resourceUpdateRes);

  // writeJSON(resConfig, resConfFilePath);
  return [true, index, selectedRes];
};

const CORES = os.cpus().length;
// multiply by x, for x processes per CORE [not strict (managed by OS's context switching mechanism!)]
const MAX_WORKERS = (CORES - 1) * workerPerCore;
// const MAX_WORKERS = 1; #test

// to prevent race conditions
// **IMP: make sure to aquire lock before updating this(AVAILABLE_WORKERS) variable!
let AVAILABLE_WORKERS = MAX_WORKERS;
const workerMutex = new Mutex();

const set = new Set();
const queue = new Queue();
const objRefInQueue = new Map();

const emptyQueues = () => {
  set.clear();
  queue.clear();
  objRefInQueue.clear();
};

const deleteFromSet = (id, followUpReq = false, options = {}) => {
  const { followUpWorkerRefs } = options;
  return followUpReq ? followUpWorkerRefs.synchroniseSet(id) : set.delete(id);
};
const addToQueue = async (entry) => {
  // typedef
  // {
  //   _id: string,
  //   followUp: boolean
  // } = entry

  const { _id, followUp, myRefs } = entry;
  let followUpReq = false;
  if (followUp) {
    console.log(`[Follow-Up] (${_id}) Enqueue in process ...`);
    const now = new Date();
    let leadsRes;
    try {
      leadsRes = await Campaign_Model.aggregate([
        {
          $match: { _id: ObjectId(_id) },
        },
        {
          $unwind: '$leads',
        },
        {
          $lookup: {
            from: 'leads', // The collection to lookup from
            localField: 'leads', // The field containing ObjectId values
            foreignField: '_id', // The field in the referenced collection to match
            as: 'leads', // The name of the field to store the replaced array
          },
        },
        {
          $unwind: '$leads', // Unwind the array of ObjectId values
        },
        {
          $match: {
            'leads.emailStatus': 1,
            'leads.enrolledFor': { $ne: 'none' },
            'leads.nextFollowUpStep': { $ne: 'none' },
            $or: [
              { 'leads.nextFollowUpAt': { $lte: now } },
              {
                'leads.nextFollowUpAt': { $exists: false },
                'leads.sentAt': {
                  $lte: now,
                  // get contacts for follow-ups, contacted since on or after 26th Sep UTC.
                  $gt: new Date('2023-09-26T00:00:00.000Z'), // Mon Sep 26 2023 00:00:00 GMT+0000,
                },
              },
            ],
          },
        },
        {
          $sort: {
            'leads.nextFollowUpAt': 1,
            'leads.sentAt': 1,
          },
        },
        {
          $limit: followUpConfig.maxEmailPerRequest,
        },
        {
          $group: {
            _id: '$_id', // You might want to group by something meaningful here
            followUpCount: { $sum: 1 },
          },
        },
        {
          $project: {
            campaignId: '$_id',
            followUpCount: 1,
          },
        },
      ]);
    } catch (err) {
      console.log(err);
      return false;
    }
    // console.log(leadsRes);
    const { campaignId, followUpCount } = leadsRes[0] || [{}];
    followUpReq = Boolean(followUpCount);
    if (!followUpReq) return true;
  }
  const campInQ = objRefInQueue.get(_id);
  if (campInQ) {
    campInQ.followUpReq = followUpReq;
    if (followUp) campInQ.myRefs = myRefs;
  } else {
    const qObj = {
      _id,
      followUpReq,
    };
    if (followUp) qObj.myRefs = myRefs;
    objRefInQueue.set(_id, qObj);
    queue.enqueue(qObj);
    if (followUp && !queue.isEmpty()) {
      logger.warning(`[Folllow-Up] Campaign (${_id}) Spawning worker!`);
      spawnWorker();
    }
  }
  return true;
};
const getNextInQueue = () => {
  const qObj = queue.dequeue();
  objRefInQueue.delete(qObj._id);
  return qObj;
};

const inProcess = new Set();
const getServiceState = () => {
  const campaignIds = Array.from(inProcess.values());
  const state = {
    workers: {
      free: AVAILABLE_WORKERS,
      total: MAX_WORKERS,
      threadingFactor: workerPerCore,
    },
    nextWorker: {
      id: queue.peek() ? queue.headIndex : null,
      campaign: queue.peek(),
    },
    inProcess: {
      activeWorkerCount: inProcess.size,
      campaignIds_size: campaignIds.length,
      campaignIds,
    },
    queueRefLength: objRefInQueue.size,
    queueLength: Object.keys(queue.items).length,
    queue,
    setLength: set.size,
    // set: Array.from(set),
    mutex: workerMutex.getState(),
  };
  return state;
};

let socketInstance = null;
const statusHandler = (socket) => {
  socketInstance = socket;
  if (socketInstance !== null) {
    const email = socket.handshake.query.email;
    console.log(`Socket connected: ${email}`);
  }

  socket.on('disconnect', () => {
    const email = socket.handshake.query.email;
    console.log(`Socket disconnected: ${email}`);
  });
};

const spawnWorker = async () => {
  // estimate run time
  const startTime = Date.now();

  const uid = getUID();
  // while (true) {
  //   const lockAcquiredBy = await workerMutex.acquire({
  //     workerId: `Acq: ${uid}`,
  //   });
  //   const isLockAcqSuccess = lockAcquiredBy === workerMutex.owner;
  //   // logger.success(isLockAcqSuccess);
  //   // console.log({
  //   //   requestedBy: lockAcquiredBy,
  //   //   currOwner: workerMutex.owner,
  //   // });
  //   if (isLockAcqSuccess) {
  //     break;
  //   }
  //   // logger.warning(
  //   //   `(${uid}) Failed to acquire lock, release failed!`,
  //   // );
  // }
  await acquireMutex(workerMutex, { uid, threadId: null });
  logger.warning(`Lock acquired for spawning worker!`);

  try {
    if (AVAILABLE_WORKERS > 0) {
      if (queue.isEmpty()) {
        logger.warning(`Worker spawn requested for empty Q, rejected.`);
        const lockReleasedBy = workerMutex.release();
        return;
      }
      // const { _id: campaignId } = queue.peek();
      const { _id: campaignId, followUpReq, myRefs } = getNextInQueue();
      const [status, index, resource] = await allocateAzureResource({
        campaignId,
      });
      // console.log(resource);
      if (!status) {
        // set.delete(queue.dequeue());
        // const { _id, followUpReq, myRefs } = getNextInQueue();
        const _id = campaignId;
        // set.delete(_id);
        const setDelStatus = deleteFromSet(_id, followUpReq, {
          followUpWorkerRefs: myRefs,
        });
        console.log(`(${_id}) Set delete status: ${setDelStatus}`);

        // setTimeout(spawnWorker, getMinsInMS(RETRY_FOR_RESOURCES_IN_X_MINS));
        // logger.warning(
        //   `Campaign[${campaignId}] Resources not available, Retrying after ${RETRY_FOR_RESOURCES_IN_X_MINS} mins`,
        // );
        // logger.error(
        //   `Campaign (${campaignId}) Acq: ${lockAcquiredBy}, Rel: ${lockReleasedBy}`,
        // );
        logger.error(
          `Campaign (${campaignId}) resources not available, try later`,
        );
        if (!queue.isEmpty()) {
          logger.warning(`Campaign (${campaignId}) Spawning worker!`);
          spawnWorker();
        }
        const lockReleasedBy = workerMutex.release();
        return;
      }

      AVAILABLE_WORKERS -= 1;
      // const id = queue.dequeue();
      // const { _id: id, followUpReq, myRefs } = getNextInQueue();
      const id = campaignId;

      const workerScript = followUpReq
        ? './services/followUpService_NEW'
        : './services/emailService';
      const worker = new Worker(workerScript, {
        workerData: { campaignId: id, emailResource: JSON.stringify(resource) },
      });
      const threadIdCopy = worker.threadId;

      inProcess.add(id);

      logger.warning(
        `[${
          worker.threadId
        }] Worker assigned to campaign (${id}), Remaining: ${AVAILABLE_WORKERS}/${MAX_WORKERS} #workerAcqRel #workerAcq ${
          followUpReq && '#workerFollowup'
        }`,
      );
      console.log(`[${worker.threadId}] Worker initiated: ${id}`);
      worker.on('message', async ({ status, error, data }) => {
        // update realtime status
        if (status === 2) {
          socketInstance?.emit('emails-sent-status', {
            campaignId: id,
            status: data,
          });
          return;
        }

        // handle job completion
        if (status === 1) {
          console.log(`[${worker.threadId}] Process completed (${id})`);

          // const resConfig = require(resConfFilePath);
          // resConfig.resourcePool[index] = data;
          // writeJSON(resConfig, resConfFilePath);
          await mailboxesApi
            .updateMailBoxes({
              filter: {
                _id: resource._id,
              },
              update: {
                $set: data,
              },
            })
            .then((res) => {
              console.log(
                `[${worker.threadId}] Resource updated <${resource.senderEmail}>`,
              );
            });

          // // set.delete(id);
          // const setDelStatus = deleteFromSet(id, followUpReq, {
          //   followUpWorkerRefs: myRefs,
          // });
          // console.log(
          //   `[${worker.threadId}] (${id}) Set delete status: ${setDelStatus}`,
          // );

          // if (!queue.isEmpty()) {
          //   spawnWorker();
          // }

          logger.warning(
            `[${threadIdCopy}] Took ${parseFloat(
              getMsInMins(Date.now() - startTime),
            ).toFixed(2)} mins, to send email. #timeAnalysis`,
          );
        }
        // handle job failure
        else if (status === 0) {
          console.log(
            `[${worker.threadId}] Process failed (${id}): error -> ${error}`,
          );

          // const resConfig = require(resConfFilePath);
          // resConfig.resourcePool[index] = data;
          // writeJSON(resConfig, resConfFilePath);
          await mailboxesApi
            .updateMailBoxes({
              filter: {
                _id: resource._id,
              },
              update: {
                $set: data,
              },
            })
            .then((res) => {
              console.log(
                `[${worker.threadId}] Resource updated <${resource.senderEmail}>`,
              );
            });

          // const setDelStatus = deleteFromSet(id, followUpReq, {
          //   followUpWorkerRefs: myRefs,
          // });
          // console.log(
          //   `[${worker.threadId}] (${id}) Set delete status: ${setDelStatus}`,
          // );

          // console.log(
          //   `[${worker.threadId}] Retrying in ${RETRY_ON_ERROR_AFTER_X_MINS} min`,
          // );
          // setTimeout(() => {
          //   // this needs to change (append id at front instead at the back!)
          //   queue.enqueue(id);
          //   spawnWorker();
          // }, getMinsInMS(RETRY_ON_ERROR_AFTER_X_MINS)); // start workers after x min
        }
        await worker.terminate();
      });

      // worker.on('error', (error) => {
      //   logger.error(
      //     `[${threadIdCopy}] Worker terminated with error`,
      //   );
      //   console.log(error)
      // });

      worker.on('exit', async (code) => {
        console.log(
          `[${threadIdCopy}] Worker terminated with exit code - ${code}`,
        );

        await mailboxesApi
          .updateMailBoxes({
            filter: {
              _id: resource._id,
            },
            update: {
              $set: {
                free: true,
              },
            },
          })
          .then((res) => {
            console.log(
              `[${worker.threadId}] Resource freed <${resource.senderEmail}>`,
            );
          });

        inProcess.delete(id);

        // while (true) {
        //   const lockAcquiredBy = await workerMutex.acquire({
        //     workerId: id,
        //     priority: true,
        //   });
        //   const isLockAcqSuccess = lockAcquiredBy === workerMutex.owner;
        //   // logger.success(isLockAcqSuccess);
        //   // console.log({
        //   //   requestedBy: lockAcquiredBy,
        //   //   currOwner: workerMutex.owner,
        //   // });
        //   if (isLockAcqSuccess) {
        //     break;
        //   }
        //   // logger.warning(
        //   //   `[${threadIdCopy}] Failed to acquire lock, release failed!`,
        //   // );
        // }
        await acquireMutex(workerMutex, { uid, threadId: threadIdCopy });
        logger.warning(`[${threadIdCopy}] Lock acquired for releasing worker!`);

        AVAILABLE_WORKERS += 1;
        const lockReleasedBy = workerMutex.release();
        // logger.error(
        //   `[${threadIdCopy}] Acq: ${lockAcquiredBy}, Rel: ${lockReleasedBy}`,
        // );

        const setDelStatus = deleteFromSet(id, followUpReq, {
          followUpWorkerRefs: myRefs,
        });
        console.log(
          `[${threadIdCopy}] (${id}) Set delete status: ${setDelStatus}`,
        );

        logger.warning(
          `[${threadIdCopy}] Worker released: ${AVAILABLE_WORKERS}/${MAX_WORKERS} #workerAcqRel #workerRel`,
        );
        // logger.warning(`Workers, available: ${AVAILABLE_WORKERS}/${MAX_WORKERS}`);
        if (!queue.isEmpty()) {
          logger.warning(`[${threadIdCopy}] Spawning worker!`);
          spawnWorker();
        }
      });
    } else {
      logger.error(
        `Workers busy, available: ${AVAILABLE_WORKERS}/${MAX_WORKERS}`,
      );
    }
  } catch (err) {
    logger.error(`spawnWorker(): Exception after acquiring lock`);
    console.log(err);
  } finally {
    const lockReleasedBy = workerMutex.release();
  }
  // logger.error(
  //   `Campaign (${campaignId}) Acq: ${lockAcquiredBy}, Rel: ${lockReleasedBy}`,
  // );
};

const handleEnqueue = async (id, by) => {
  // logger.success('Set items:')
  // set.forEach((key) => logger.warning(key));
  // logger.error(`Set has ${id}: ${set.has(id.toString())}`);

  if (set.has(id)) return;
  set.add(id);
  // queue.enqueue(id);
  const enqueuStatus = await addToQueue({ _id: id });
  logger.focus(
    `Enqueue rquested for campaign (${id}) by '${by}': ${enqueuStatus}`,
  );
  await spawnWorker();
};

const startProcessing = async (data) => {
  let {
    _id: id,
    operationStat: { totalEmailed, leadsGen },
    clientEmail: email,
  } = data;
  const _id = id.toString();
  if (!totalEmailed) totalEmailed = 0;

  if (
    totalEmailed === leadsGen ||
    set.has(_id)
    // // allow list
    // ![
    //   // '650de1ece03ad30056f285be', // test id for video generation
    //   '6560d66694bc4a007a4dcc50',
    //   // daniel.henry@boardsi.com
    //   // '650af968d6bd720056000c63',
    //   // joshua+testing1@b2brocket.ai
    //   // '650de1ece03ad30056f285be',
    //   // '650de227e03ad30056f28618',
    //   // ivan_omelchenko@wow24-7.io
    //   // '64f9b0984fb0ab00573ca88a',
    //   // sdecosmo@amplifyhrm.com
    //   // '65008938e18c4f0056341b57',
    //   // dio@leadmark
    //   // '64bd9075c29f0400501e646f',
    //   // '64f9bb534fb0ab00573cb286',
    //   // '64e7171fe1b5eb46d438898e',
    //   // '649ef9b3dce08300502bc432',
    // ].includes(_id)
  ) {
    console.log(`Discarded: ${_id}`);
    return;
  }
  // console.log(totalEmailed, leadsGen);
  await assignMailboxesIfReq({ _id, email });
  await handleEnqueue(_id, 'Polling Service');
};

const pollDataForProcessing = async () => {
  if (!isActiveHours()) {
    emptyQueues();
    const currTimeObj = getCurrTimeObj({
      timeZone: emailConfig.activeHrs.tz,
    });
    logger.warning(
      `Emails are sent from ${emailConfig.activeHrs.start}hrs -> ${
        emailConfig.activeHrs.end
      }hrs (${emailConfig.activeHrs.tz} time). Current time, ${Object.values(
        currTimeObj,
      ).join(':')}`,
    );
    return;
  }
  const now = new Date().toUTCString();
  console.log(`Polling started @${now.split(',')[1]}`);
  try {
    const data = await Campaign_Model.find(
      {
        paused: false,
        campaignStatus: false,
        // testimonials: { $exists: true }
      },
      { _id: 1, operationStat: 1, clientEmail: 1 },
    );
    // console.log(data);

    if (!data.length) {
      logger.focus(`[Email Server] No campaigns found by polling service`);
      return;
    }

    data.forEach(async (campaign) => {
      await startProcessing(campaign._doc);
    });
  } catch (e) {
    console.log(e);
    console.log(`Polling failed with error: `, e.message);
  }
};

let clearPollInt = null;
const startPolling = () => {
  if (clearPollInt) {
    logger.error(`Polling already started`);
    return;
  }
  clearPollInt?.();
  logger.warning(`Polling happens every ${POLL_EVERY_X_MINS} mins`);
  pollDataForProcessing();
  const pollingInt = setInterval(
    pollDataForProcessing,
    getMinsInMS(POLL_EVERY_X_MINS),
  );
  clearPollInt = () => {
    clearInterval(pollingInt);
  };
  return clearPollInt;
};

const stopPolling = () => {
  try {
    if (!clearPollInt) {
      throw new Error('Polling not started yet');
    }
    clearPollInt();
    clearPollInt = null;
    emptyQueues();
    logger.warning(`Polling stopped`);
    return { status: true, message: 'Polling stopped' };
  } catch (e) {
    logger.error(`Error: stopPolling() -> ${e.message}`);
    return { status: false, message: e.message };
  }
};

module.exports = {
  freeResources,
  addToQueue,
  statusHandler,
  allocateAzureResource,
  assignMailboxesIfReq,
  getServiceState,
  startPolling,
  stopPolling,
  pollDataForProcessing,
};

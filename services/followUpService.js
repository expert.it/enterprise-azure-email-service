const mongoose = require('mongoose');
// const { ObjectId } = require('mongodb');
const Sent_Model = require('../models/sentEmails');
const Campaign_Model = require('../models/campaign');

const { parentPort, workerData, threadId } = require('node:worker_threads');

const { EmailClient } = require('@azure/communication-email');

const fetch = (...args) =>
  import('node-fetch').then(({ default: fetch }) => fetch(...args));

const {
  MongoURI: db,
  // AZURE_RES_CONN_STRING: connectionString,
} = require('../config/keys');

const {
  followUpConfig: {
    maxEmailPerRequest,
    TIMEOUT_EMAIL_SENDING_IN_SECS,
    SEND_EMAILS_AFTER_X_DAYS,
  },
  azureEmailService: { LIMIT_PER_HOUR },
  logger,
  API_SERVICE_BACKEND,
} = require('../config/config');

const {
  getCustomEmail,
  getGPT_Prompt,
  getFolllowUpMail,
} = require('../data/templates');
const { timeOut } = require('../utils/misc');

const { emailResource } = workerData;
const {
  id,
  connStr: connectionString,
  senderEmail,
  emailsSent,
} = emailResource;

const emailClient = new EmailClient(connectionString);
logger.focus(
  `[Follow Up] [${threadId}] (${workerData.campaignId}) Using azure resource: id -> ${id}: ${senderEmail}, Sent: ${emailsSent} / ${LIMIT_PER_HOUR}`,
);

const sendEmails = async ({
  campaign,
  recipient,
  // aiGen,
}) => {
  logger.focus(`Sending Email ...`);
  const followUpMail = getFolllowUpMail({
    recipient,
    // aiGen,
    campaign,
  });

  // console.log(followUpMail);

  const message = {
    senderAddress: senderEmail,
    content: {
      subject: followUpMail.subject,
      plainText: '',
      html: followUpMail.body,
    },
    recipients: {
      to: [recipient],
    },
    // attachments: [
    //   {
    //     name: path.basename(filePath),
    //     contentType: "text/plain",
    //     contentInBase64: readFileSync(filePath, "base64"),
    //   },
    // ],
  };

  const email = message.content.html;
  let waitInt;

  try {
    // logger.warning(
    //   `Email sending process begins for ${recipient.displayName} (${recipient.address})`,
    // );
    waitInt = setInterval(() => {
      logger.warning(
        `[${threadId}] Waiting for process completion ${recipient.displayName} (${recipient.address})`,
      );
    }, 10 * 1000);
    const poller = await timeOut(
      async () => await emailClient.beginSend(message),
      TIMEOUT_EMAIL_SENDING_IN_SECS * 1000,
      { errorMessageOnTimeout: 'Switch' },
    );
    const response = await poller.pollUntilDone();
    // console.log(response)
    // logger.warning(`Email sent to ${recipient.address}`);
    const { id, status, error } = response;
    // console.log(id)
    const sentEmailObj = {
      // this forms a part of the message id being recieved from leads
      partMessageId: id.split('-').join(''),
      campaignId: workerData.campaignId,
      from: message.senderAddress,
      to: recipient.address,
      email: {
        subject: message.content.subject,
        html: message.content.html,
      },
      followUp: true,
    };
    const newSentEmail = new Sent_Model(sentEmailObj);
    await newSentEmail.save().then((res) => {
      logger.warning(`Email saved to DB...`);
    });
    return [status, email, error];
  } catch (e) {
    // console.log(e);
    logger.error(e.message);
    // const deniedByProviderPattern = /Denied/
    if (e.message === 'Switch' || e.code === 'Denied') {
      return ['Switch', null, null];
    }

    const invalidEmail = new RegExp(/Invalid format for email address/, 'i');
    if (invalidEmail.test(e.message)) {
      return [
        'Blocked',
        "<b><span style='color:red;'>Invalid Email</span></b>",
        null,
      ];
    }

    const pattern = new RegExp(/EmailDroppedAllRecipientsSuppressed/, 'i');
    if (pattern.test(e.message)) {
      return [
        'Blocked',
        "<b><span style='color:red;'>Permission denied</span><br/>User may have opted out !</b>",
        null,
      ];
    } else {
      throw new Error(e.message);
    }
  } finally {
    clearInterval(waitInt);
  }
};

const main = async () => {
  try {
    await mongoose
      .connect(db, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
      })
      .then()
      .catch((err) => console.log(`[Follow Up] Worker [${threadId}]: `, err));

    const campaign = await Campaign_Model.findById(workerData.campaignId, {
      leads: 0,
    });

    if (!campaign) {
      throw new Error(
        `[Follow Up] Can't fetch campaign with id: ${workerData.campaignId}`,
      );
    }

    // let {
    //   senderName,
    //   campaignDesc,
    //   productDesc,
    //   organisationName: userOrgName,
    //   operationStat: { totalEmailed, leadsGen },
    // } = campaign._doc;

    const queryParamsStr = new URLSearchParams({
      daysOld: SEND_EMAILS_AFTER_X_DAYS,
      limit: maxEmailPerRequest,
    }).toString();
    const followUpLeads = await fetch(
      `${API_SERVICE_BACKEND}/getFollowUpLeads/${workerData.campaignId}?${queryParamsStr}`,
      {
        method: 'GET',
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      },
    );
    const jsonRes = await followUpLeads.json();
    const leads = jsonRes.leads;
    // console.log(leads);

    if (!Boolean(leads.length)) {
      logger.warning(`[Follow Up] [${threadId}] (${workerData.campaignId}) No follow ups required yet !`);
    } else {
      console.log(
        `[Follow Up] [${threadId}] (${workerData.campaignId}) Sending follow-up emails to next ${leads.length} leads`,
      );
    }

    for (let lead of leads) {
      const {
        firstName,
        lastName,
        email,
        linkedin_url,
        organization,
        industry,
      } = lead;
      const fullName = `${firstName} ${lastName}`;

      [emailStatus, sentEmail, sendError] = await sendEmails({
        campaign: campaign._doc,
        recipient: {
          address: email,
          displayName: fullName,
        },
        // aiGen,
      });

      if (emailStatus === 'Succeeded' || emailStatus === 'Blocked') {
        emailResource.emailsSent += 1;

        logger.success(
          `[Follow Up] Email sent to ${email} by (${id}: ${senderEmail})`,
        );

        const updateRes = await fetch(`${API_SERVICE_BACKEND}/updateLeads`, {
          method: 'PATCH',
          body: JSON.stringify({
            campaignId: workerData.campaignId,
            email,
            set: {
              followUpRequired: false,
            },
          }),
          headers: {
            'Content-type': 'application/json; charset=UTF-8',
          },
        });
        const jsonRes = await updateRes.json();
        // console.log(jsonRes);
        if (jsonRes.status) {
          console.log('[Follow Up] Lead updated');
        }

        if (emailResource.emailsSent >= LIMIT_PER_HOUR) {
          logger.focus(
            `[Follow Up] Switching resource (Hourly limit reached) (${id}: ${senderEmail})`,
          );
          emailResource.cooling = true;
          emailResource.lastActive = Date.now();
          break;
        }
      } else if (emailStatus === 'Switch') {
        logger.focus(
          `[Follow Up] Switching resource (Unresponsive) (${id}: ${senderEmail})`,
        );
        emailResource.cooling = true;
        emailResource.lastActive = Date.now();
        break;
      } else {
        console.log(emailStatus);
        logger.error(`[Follow Up] Sending emails failed, trying again !`);
        throw new Error(sendError);
      }
    }
    emailResource.free = true;
    parentPort.postMessage({
      status: 1,
      data: emailResource,
    });
  } catch (e) {
    console.log(e);
    emailResource.free = true;
    parentPort.postMessage({
      status: 0,
      error: e.message,
      data: emailResource,
    });
  }
};

if (require.main === module) {
  console.log(`[Follow Up] Worker: ${threadId}`);
  // console.log(workerData);
  main();
}

module.exports = {};

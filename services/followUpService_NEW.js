const mongoose = require('mongoose');
const { ObjectId } = require('mongodb');
const Sent_Model = require('../models/sentEmails');
const Campaign_Model = require('../models/campaign');
const mailboxesApi = require('./mailBoxes');
const nodemailer = require('nodemailer');
const util = require('node:util');
const stopExecutionError = `Force stop execution`;

const { parentPort, workerData, threadId } = require('node:worker_threads');

const fetch = (...args) =>
  import('node-fetch').then(({ default: fetch }) => fetch(...args));

const {
  MongoURI: db,
  // AZURE_RES_CONN_STRING: connectionString,
} = require('../config/keys');

const {
  followUpConfig: {
    maxEmailPerRequest,
    TIMEOUT_EMAIL_SENDING_IN_SECS,
    SEND_EMAILS_AFTER_X_DAYS,
  },
  azureEmailService: { LIMIT_PER_HOUR },
  loggerAdv,
  googleWebAppConfig,
  EMAIL_TRACKING_SERVICE,
  API_SERVICE_BACKEND,
} = require('../config/config');

const {
  getCustomEmail,
  getGPT_Prompt,
  getFolllowUpMail,
  getSubjectFromTemps,
  getTrackingPixel,
  getEmailFromSections,
  getEmailStatusLink,
  emailTemplates,
} = require('../data/templates');
const { timeOut, random } = require('../utils/misc');
const {
  getAccessToken,
  providerServiceMap,
  generatePersonalizedMsg,
  processEmailSections,
  sendEmails,
} = require('./emailService');

const prefix = '[Follow-Up]';
const print = console.log;
customLog = (str, options = {}) => {
  const { inspect } = options;
  let modStr = str;
  if (inspect) modStr = util.inspect(modStr, { depth: null });
  print(`${prefix} ${modStr}`);
};
const logger = loggerAdv(prefix);

// const { emailResource } = workerData;
// const {
//   id,
//   connStr: connectionString,
//   senderEmail,
//   emailsSent,
// } = emailResource;

// const emailClient = new EmailClient(connectionString);
// logger.focus(
//   `[${threadId}] (${workerData.campaignId}) Using azure resource: id -> ${id}: ${senderEmail}, Sent: ${emailsSent} / ${LIMIT_PER_HOUR}`,
// );

// const getNextFollowUpStep = (currStage, currStep) => {
//   const currStepCt = Number(currStep.slice('step'.length));
//   const nextAvail = Boolean(
//     ![null, undefined].includes(emailSeqStages[currStage][currStepCt + 1]),
//   );
//   return nextAvail ? `step${currStepCt + 1}` : 'none';
// };

// async function sendGMail(from, to, { subject, body, inReplyTo, labels }) {
//   const option = {
//     from,
//     to,
//     subject,
//     text: '',
//     html: body,
//     inReplyTo,
//     headers: {
//       'X-Gmail-Labels': labels?.join(',') || '', // Comma-separated list of labels
//     },
//   };

//   return emailClient.sendMail(option);
// }

// const sendEmails = async ({
//   prevEmailBody,
//   senderName,
//   userLink,
//   recipient,
//   aiGen,
//   additionalInfo,
//   userOrgName,
//   userCompanyLocation,
//   emailTemplateIndex,
// }) => {
//   // customLog(userOrgName);
//   // customLog(additionalInfo);

//   // const customEmail = getCustomEmail({
//   //   senderName,
//   //   recipient,
//   //   aiGen,
//   //   additionalInfo,
//   //   userOrgName,
//   // });
//   // customLog(customEmail);

//   // const promptToRemoveSpammyWords = getGPT_Prompt.replceSpammyWords(
//   //   customEmail.body,
//   // );

//   // logger.focus(`Filtering out spam words from generated email ...`);
//   // const spamFilteredBody = await generatePersonalizedMsg(
//   //   promptToRemoveSpammyWords,
//   // );
//   // customLog(spamFilteredBody);

//   const emailId = new ObjectId();
//   // customLog(emailId)

//   const emailTemplate = emailTemplates[emailTemplateIndex];
//   const staticTempsSubject = getSubjectFromTemps({ userOrgName });
//   // throw new Error('break');

//   let senderEmailName = senderEmail.split('@')[0].split('.').join(' ');
//   senderEmailName =
//     senderEmailName.slice(0, 1).toUpperCase() + senderEmailName.slice(1);

//   const message = {
//     senderAddress: senderEmail,
//     // content: {
//     //   subject: customEmail.subject,
//     //   plainText: customEmail.body,
//     //   html: spamFilteredBody,
//     // },
//     content: {
//       // subject: aiGen.subject,
//       subject: staticTempsSubject,
//       plainText: '',
//       html: prevEmailBody,
//       // html: wholeEmail.body,
//       // html: aiGen.wholeEmail,
//     },
//     recipients: {
//       to: [recipient],
//     },
//     // attachments: [
//     //   {
//     //     name: path.basename(filePath),
//     //     contentType: "text/plain",
//     //     contentInBase64: readFileSync(filePath, "base64"),
//     //   },
//     // ],
//   };

//   if (!prevEmailBody) {
//     const link = getEmailStatusLink({
//       customerObj: additionalInfo,
//       emailId,
//     });

//     let wholeEmail = getEmailFromSections({
//       sections: aiGen.emailSections,
//       link,
//       userOrgName,
//       senderName: senderEmailName,
//       userCompanyLocation,
//     });
//     // customLog(wholeEmail);

//     const trackingPixelStr = getTrackingPixel({
//       recipient,
//       customerObj: additionalInfo,
//       emailId,
//     });

//     wholeEmail.body += `<br/>${trackingPixelStr}`;

//     wholeEmail.body = wholeEmail.body.replace(
//       new RegExp(`${userOrgName}`, 'ig'),
//       `<a href=${link} target="_blank"><b>${userOrgName}</b></a>`,
//     );
//     wholeEmail.body = wholeEmail.body.replace(
//       new RegExp(/\[\s*\w+(?:\s+\w+)*\s*\]/, 'ig'),
//       ``,
//     );
//     wholeEmail.body = wholeEmail.body.replace(new RegExp(/#UNKNOWN/, 'ig'), ``);

//     // customLog(wholeEmail);
//     // throw new Error('break');

//     // never use a pattern more than once without resetting lastIndex
//     // eg: pattern.lastIndex = 0;
//     // const replaceTexts = [
//     //   [
//     //     new RegExp(`${userOrgName}`, 'ig'),
//     //     `<a href=${link} target="_blank"><b>${userOrgName}</b></a>`,
//     //   ],
//     //   [new RegExp(/\[\s*\w+(?:\s+\w+)*\s*\]/, 'ig'), ``],
//     //   [new RegExp(/#UNKNOWN/, 'ig'), ``],
//     // ];
//     // replaceTexts.map(([oldText, newText]) => {
//     //   wholeEmail.body.replace(oldText, newText);
//     // });

//     // aiGen.wholeEmail += `<br/>${trackingPixelStr}`;
//     // customLog(aiGen.wholeEmail);
//     // customLog(wholeEmail.body);
//     // throw new Error('break');

//     message.content.html = wholeEmail.body;
//   }

//   const neutralizedEmail = prevEmailBody;
//   if (prevEmailBody) {
//     message.content.html = message.content.html.replace(
//       `#`,
//       `${EMAIL_TRACKING_SERVICE}`,
//     );
//   }
//   // customLog(message.content);
//   // throw new Error('break');

//   let waitInt;

//   try {
//     // logger.warning(
//     //   `Email sending process begins for ${recipient.displayName} (${recipient.address})`,
//     // );
//     waitInt = setInterval(() => {
//       logger.warning(
//         `[${threadId}] Waiting for process completion ${recipient.displayName} (${recipient.address})`,
//       );
//     }, 10 * 1000);

//     logger.focus(
//       `[${threadId}] (${workerData.campaignId}) Sending Email to ${recipient.address}`,
//     );
//     // const poller = await timeOut(

//     const info = await timeOut(
//       async () => {
//         // return await emailClient.beginSend(message)
//         return await sendGMail(senderEmail, recipient.address, {
//           subject: message.content.subject,
//           body: message.content.html,
//           labels: [additionalInfo.leadId, workerData.campaignId],
//         });
//       },
//       TIMEOUT_EMAIL_SENDING_IN_SECS * 1000,
//       { errorMessageOnTimeout: 'Switch' },
//     );

//     // const response = await poller.pollUntilDone();
//     // customLog(response)
//     // logger.warning(`Email sent to ${recipient.address}`);
//     // const { id, status, error } = response;

//     // customLog(info);
//     // {
//     //   accepted: [ 'eric.shakun@adweek.com' ],
//     //   rejected: [],
//     //   ehlo: [
//     //     'SIZE 35882577',
//     //     '8BITMIME',
//     //     'AUTH LOGIN PLAIN XOAUTH2 PLAIN-CLIENTTOKEN OAUTHBEARER XOAUTH',
//     //     'ENHANCEDSTATUSCODES',
//     //     'PIPELINING',
//     //     'CHUNKING',
//     //     'SMTPUTF8'
//     //   ],
//     //   envelopeTime: 1148,
//     //   messageTime: 889,
//     //   messageSize: 1652,
//     //   response: '250 2.0.0 OK  1693908609 x2-20020a170902fe8200b001bf6ea340a9sm8914038plm.159 - gsmtp',
//     //   envelope: {
//     //     from: 'leadingly-test8@leadingly.io',
//     //     to: [ 'eric.shakun@adweek.com' ]
//     //   },
//     //   messageId: '<cbfe0c24-de83-4d75-63cf-5c2908f09aae@leadingly.io>'
//     // }

//     // customLog(id);
//     const sentEmailObj = {
//       // this forms a part of the message id being recieved from leads
//       _id: emailId,
//       // partMessageId: id.split('-').join(''),
//       messageId: info.messageId,
//       campaignId: workerData.campaignId,
//       from: message.senderAddress,
//       to: recipient.address,
//       leadId: additionalInfo.leadId,
//       email: {
//         subject: message.content.subject,
//         text: message.content.plainText,
//         // html: message.content.html,
//         html: neutralizedEmail,
//       },
//     };
//     const newSentEmail = new Sent_Model(sentEmailObj);
//     await newSentEmail.save().then((res) => {
//       logger.warning(
//         `[${threadId}] (${workerData.campaignId}) Email saved to DB... (${recipient.address})`,
//       );
//       // customLog(res._id);
//       logger.warning(
//         `[${threadId}] (${workerData.campaignId}) Email id: ${res._id}, (${recipient.address})`,
//       );
//     });
//     // return [status, email, error];
//     return [
//       info.accepted[0] === recipient.address ? 'Succeeded' : 'Failed',
//       neutralizedEmail,
//       null,
//       emailId,
//     ];
//   } catch (e) {
//     // customLog(e);
//     logger.warning(`sendEmails() catch:`);
//     logger.error(e.message);
//     // const deniedByProviderPattern = /Denied/
//     if (e.message === 'Switch' || e.code === 'Denied') {
//       return ['Switch', null, null, null];
//     }

//     const invalidEmail = new RegExp(/Invalid format for email address/, 'i');
//     if (invalidEmail.test(e.message)) {
//       return [
//         'Blocked',
//         "<b><span style='color:red;'>Invalid Email</span></b>",
//         null,
//         null,
//       ];
//     }

//     const pattern = new RegExp(/EmailDroppedAllRecipientsSuppressed/, 'i');
//     if (pattern.test(e.message)) {
//       return [
//         'Blocked',
//         "<b><span style='color:red;'>Permission denied</span><br/>User may have opted out !</b>",
//         null,
//         null,
//       ];
//     } else {
//       throw new Error(e.message);
//     }
//   } finally {
//     clearInterval(waitInt);
//   }
// };

const main = async () => {
  try {
    await mongoose
      .connect(db, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
      })
      .then()
      .catch((err) => customLog(`Worker [${threadId}]: `, err));

    const campaign = await Campaign_Model.findById(workerData.campaignId, {
      leads: 0,
      avoidEmailToDomains: 0,
    });

    if (!campaign) {
      throw new Error(`Can't fetch campaign with id: ${workerData.campaignId}`);
    }

    // let {
    //   senderName,
    //   campaignDesc,
    //   productDesc,
    //   organisationName: userOrgName,
    //   operationStat: { totalEmailed, leadsGen },
    // } = campaign._doc;
    let {
      senderName,
      senderTitle,
      link: linkObj,
      campaignDesc,
      productDesc,
      organisationName: userOrgName,
      companyName: customUserOrgName,
      companyLocation: userCompanyLocation,
      operationStat: { totalEmailed, leadsGen },
    } = campaign._doc;

    // const queryParamsStr = new URLSearchParams({
    //   daysOld: SEND_EMAILS_AFTER_X_DAYS,
    //   limit: maxEmailPerRequest,
    // }).toString();
    // const followUpLeads = await fetch(
    //   `${API_SERVICE_BACKEND}/getFollowUpLeads/${workerData.campaignId}?${queryParamsStr}`,
    //   {
    //     method: 'GET',
    //     headers: {
    //       'Content-type': 'application/json; charset=UTF-8',
    //     },
    //   },
    // );
    // const jsonRes = await followUpLeads.json();
    // const leads = jsonRes.leads;
    // // customLog(leads);

    const now = new Date();
    const leadsRes = await Campaign_Model.aggregate([
      {
        $match: { _id: ObjectId(workerData.campaignId) },
      },
      {
        $unwind: '$leads',
      },
      {
        $lookup: {
          from: 'leads', // The collection to lookup from
          localField: 'leads', // The field containing ObjectId values
          foreignField: '_id', // The field in the referenced collection to match
          as: 'leads', // The name of the field to store the replaced array
        },
      },
      {
        $unwind: '$leads', // Unwind the array of ObjectId values
      },
      {
        $match: {
          'leads.emailStatus': 1,
          'leads.enrolledFor': { $ne: 'none' },
          'leads.nextFollowUpStep': { $ne: 'none' },
          $or: [
            { 'leads.nextFollowUpAt': { $lte: now } },
            {
              'leads.nextFollowUpAt': { $exists: false },
              'leads.sentAt': {
                $lte: now,
                // get contacts for follow-ups, contacted since on or after 26th Sep UTC.
                $gt: new Date('2023-09-26T00:00:00.000Z'), // Mon Sep 26 2023 00:00:00 GMT+0000,
              },
            },
          ],
        },
      },
      {
        $sort: {
          'leads.nextFollowUpAt': 1,
          'leads.sentAt': 1,
        },
      },
      {
        $limit: maxEmailPerRequest,
      },
      {
        $group: {
          _id: '$_id', // You might want to group by something meaningful here
          leads: { $push: '$leads' },
        },
      },
      {
        $project: {
          // leads: { $slice: ['$leads', 0, maxEmailPerRequest] },
          leads: 1,
        },
      },
    ]);

    // customLog(leadsRes);
    const leads = leadsRes[0]?.leads || [];
    customLog('Leads fetched:');
    // customLog(leads.map((lead) => lead.email).join(' | '));
    // customLog(leads, { inspect: true });

    if (!Boolean(leads.length)) {
      logger.warning(
        `[${threadId}] (${workerData.campaignId}) No follow ups required yet !`,
      );
    } else {
      customLog(
        `[${threadId}] (${workerData.campaignId}) Sending follow-up emails to next ${leads.length} leads`,
      );
    }
    // throw new Error(stopExecutionError);

    for (let lead of leads) {
      const {
        _id: leadId,
        emailStatus,
        email,
        firstName,
        lastName,
        enrolledFor,
        nextFollowUpStep,
        emailSeqStatus,
        description,
        organization,
        linkedin_url,
        industry,
        address,
      } = lead;
      if (
        // don't email to test leads
        [
          'gautam@leadingly.io',
          'shiva@leadingly.io',
          'noah@leadingly.io',
          'noah@b2brocket.ai',
        ].includes(email)
      ) {
        continue;
      }
      const fullName = `${firstName} ${lastName}`;
      const recipient = { address: email, displayName: fullName };
      const additionalInfo = {
        leadId: leadId.toString(),
        campaignId: workerData.campaignId,
        firstName,
        lastName,
        description,
        organization,
        linkedin_url,
        // industry,
        address,
      };

      const nextFollowUpStepNum = nextFollowUpStep?.slice('step'.length);
      const lastExecStepNum = Number(nextFollowUpStepNum || 1) - 1;
      const lastExecStep = 'step' + lastExecStepNum;
      let prevFollowUpEmailBody, lastEmailStatus;
      if (lastExecStepNum) {
        const lastEmailId =
          emailSeqStatus?.[enrolledFor]?.prospect?.[lastExecStep];
        // customLog(lastEmailId);
        lastEmailStatus = await Sent_Model.findOne({ _id: lastEmailId }, {});
        // customLog(lastEmailStatus);
        prevFollowUpEmailBody = lastEmailStatus?.emailBody;
      }

      let variationReq = false || !Boolean(prevFollowUpEmailBody);

      if (
        Boolean(
          lastEmailStatus?.opened?.length + lastEmailStatus?.clicked?.length,
        )
      ) {
        variationReq = true;
      }

      let aiGen;
      // if opened or clicked generate a new email!
      if (variationReq) {
        logger.warning(
          `(${workerData.campaignId}) Generating new email! (${email})`,
        );
        // gen new email!
        aiGen = {
          // subject: null,
          // introPitch: null,
          // body: null,
          // conclusion: null,
          // wholeEmail: null,
          emailSections: null,
        };
        const aiParams = {
          // subject: { productDesc },
          // introPitch: {
          //   campaignDesc,
          //   productDesc,
          //   customerObj: additionalInfo,
          // },
          // body: { userOrgName, campaignDesc, productDesc },
          // conclusion: { userOrgName, customerObj: additionalInfo },
          // wholeEmail: {
          //   userLink: linkObj?.enabled ? linkObj?.url || '' : '',
          //   senderName,
          //   senderTitle,
          //   userOrgName,
          //   recipient,
          //   campaignDesc,
          //   productDesc,
          //   customerObj: additionalInfo,
          // },
          emailSections: {
            userLink: linkObj?.enabled ? linkObj?.url || '' : '',
            // senderName,
            senderName:
              senderNameAtResource ||
              (() => {
                let senderEmailName = senderEmail
                  .split('@')[0]
                  .split('.')
                  .join(' ');
                senderEmailName =
                  senderEmailName.slice(0, 1).toUpperCase() +
                  senderEmailName.slice(1);
                return senderEmailName;
              })(),
            userOrgName: customUserOrgName || userOrgName,
            recipient,
            campaignDesc,
            productDesc,
            customerObj: additionalInfo,
            emailTemplateIndex: random(emailTemplates.length),
          },
        };

        // const personalizedMsg = await generatePersonalizedMsg(
        //   userOrgName,
        //   additionalInfo,
        // );

        // customLog(aiParams);

        logger.focus(
          `[${threadId}] (${workerData.campaignId}) Fetching AI ... <${recipient.address}>`,
        );
        const aiPromises = Object.keys(aiGen).map(async (key) => {
          const prompt = getGPT_Prompt[key](aiParams[key]);
          // customLog(prompt);
          let aiRes = await generatePersonalizedMsg(prompt);
          if (key === 'emailSections') {
            aiRes = processEmailSections(aiRes);
          }
          return aiRes;
        });

        const contentArray = await Promise.all(aiPromises);
        Object.keys(aiGen).forEach((key, index) => {
          aiGen[key] = contentArray[index];
        });

        // customLog(aiGen);
        // customLog(aiGen.wholeEmail);
        // customLog(aiGen.emailSections);
        // throw new Error('break');
        // followUpEmailBody =
      }

      [emailStatus, sentEmail, sendError, emailId] = await sendEmails({
        prevEmailBody: variationReq ? null : prevFollowUpEmailBody,
        senderName,
        recipient,
        aiGen,
        additionalInfo,
        userOrgName: customUserOrgName || userOrgName,
        userCompanyLocation,
        emailTemplateIndex: aiParams.emailSections.emailTemplateIndex,
      });

      if (emailStatus === 'Succeeded' || emailStatus === 'Blocked') {
        emailResource.emailsSent += 1;
        emailResource.followUpsSent += 1;

        logger.success(`Email sent to ${email} by (${id}: ${senderEmail})`);

        const nextStep = getNextFollowUpStep(nextFollowUpStep);
        const leadUpRes = await leadModel.updateOne(
          {
            // email: recipient.address,
            // campaignIds: { $in: [ObjectId(workerData.campaignId)] },
            _id: ObjectId(additionalInfo.leadId),
          },
          {
            $set: {
              emailBody: sentEmail,
              nextFollowUpAt:
                Date.now() +
                getHrsInMS(24 * emailSeqStages.initial[nextStep] || 1e10),
              [`emailSeqStatus.${enrolledFor}.prospect.${nextFollowUpStep}`]:
                ObjectId(emailId),
              nextFollowUpStep: nextStep,
            },
          },
        );
        // customLog(leadUpRes);

        // const updateRes = await fetch(`${API_SERVICE_BACKEND}/updateLeads`, {
        //   method: 'PATCH',
        //   body: JSON.stringify({
        //     campaignId: workerData.campaignId,
        //     email,
        //     set: {
        //       nextFollowUpAt:
        //         Date.now() + getHrsInMS(24 * emailSeqStages.initial[nextStep]),
        //       nextFollowUpStep: nextStep,
        //       ...(nextStep === 'none' && { enrolledFor: 'none' }),
        //       // followUpRequired: false,
        //     },
        //   }),
        //   headers: {
        //     'Content-type': 'application/json; charset=UTF-8',
        //   },
        // });
        // const jsonRes = await updateRes.json();
        // // customLog(jsonRes);
        // if (jsonRes.status) {
        //   customLog('Lead updated');
        // }

        await mailboxesApi
          .updateMailBoxes({
            filter: {
              _id: emailResource._id,
            },
            update: [
              {
                $set: {
                  // emailsSent: { $add: ['$emailsSent', 1] },
                  emailsSent: emailResource.emailsSent,
                },
              },
            ],
          })
          .then((res) => {
            if (!res) {
              logger.error(
                `[${emailResource.senderEmail}] Error updating email count `,
              );
              return;
            }
            customLog(`[${emailResource.senderEmail}] email count updated!`);
            customLog(res);
          })
          .catch((err) => {
            logger.error(
              `[${emailResource.senderEmail}] Error updating email count `,
            );
            customLog(err);
          });

        if (
          // emailResource.followUpsSent >=
          emailResource.emailsSent >=
          Math.min(Math.floor(emailResource.maxAllowed), LIMIT_PER_HOUR)
          // Math.min(Math.floor(emailResource.maxAllowed / 2), LIMIT_PER_HOUR)
        ) {
          logger.focus(
            `[${threadId}] (${workerData.campaignId}) Switching resource (Hourly/Daily limit reached) (${id}: ${senderEmail})`,
          );
          emailResource.cooling = true;
          emailResource.lastActive = Date.now();
          break;
        }
      } else if (emailStatus === 'Switch') {
        logger.focus(
          `Switching resource (Unresponsive) (${id}: ${senderEmail})`,
        );
        emailResource.cooling = true;
        emailResource.lastActive = Date.now();
        break;
      } else {
        customLog(emailStatus);
        logger.error(`Sending emails failed, trying again !`);
        throw new Error(sendError);
      }
    }
    emailResource.free = true;
    parentPort.postMessage({
      status: 1,
      data: emailResource,
    });
  } catch (e) {
    customLog(e);
    emailResource.free = true;
    parentPort.postMessage({
      status: 0,
      error: e.message,
      data: emailResource,
    });
  }
};

if (require.main === module) {
  customLog(`Worker: ${threadId}`);
  // customLog(workerData, { inspect: true });
  // return;

  var { emailResource } = workerData;
  var {
    _id: id,
    // connectionString
    // id,
    // connStr: connectionString,
    senderEmail,
    fullName: senderNameAtResource,
    emailsSent,
    provider,
    provider_data,
    maxAllowed,
  } = emailResource;

  // var emailClient = new EmailClient(connectionString);
  var emailClient = null;

  getAccessToken(provider_data.refresh_token)
    .then((access_token) => {
      emailClient = nodemailer.createTransport({
        service: providerServiceMap[provider],
        auth: {
          type: 'OAuth2',
          user: senderEmail,
          accessToken: access_token,
          refreshToken: provider_data.refresh_token,
          clientId: googleWebAppConfig.client_id,
          clientSecret: googleWebAppConfig.client_secret,
        },
      });

      logger.focus(
        `[${threadId}] (${
          workerData.campaignId
        }) Using resource: id -> ${id}: ${senderNameAtResource} <${senderEmail}>, Sent: ${emailsSent} / ${Math.floor(
          maxAllowed,
          // maxAllowed / 2,
        )}`,
      );
      main();
    })
    .catch((err) => {
      logger.error(`catch: getAccessToken()`);
      logger.error(err);
    });
}

module.exports = {};

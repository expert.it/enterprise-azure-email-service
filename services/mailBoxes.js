const { default: axios } = require('axios');
const { API_SERVICE_BACKEND, emailConfig } = require('../config/config');
const mailboxesModel = require('../models/mailboxes');

const assignMailBoxes = async ({ email, campaignId, limit }) => {
  try {
    // const res = await axios.get(`${API_SERVICE_BACKEND}/assignMailBoxes`, {
    //   params: {
    //     email,
    //     campaignId,
    //     limit,
    //   },
    // });

    let reqMailBoxes = emailConfig.MAILBOXES_PER_CAMPAIGN;
    if (!isNaN(parseInt(limit))) {
      reqMailBoxes = parseInt(limit);
    }
    const bulkWriteQuery = [];
    for (let i = 0; i < reqMailBoxes; i++) {
      bulkWriteQuery.push({
        updateOne: {
          filter: {
            'assigned.email': null,
          },
          update: {
            'assigned.email': email,
            'assigned.campaignId': campaignId,
          },
        },
      });
    }
    let bulkWriteRes = await mailboxesModel.bulkWrite(bulkWriteQuery);
    const { nModified, nUpserted } = bulkWriteRes;
    const data = { nModified, nUpserted };
    return data;
  } catch (err) {
    throw new Error(err);
  }
};

const getMailBoxes = async ({ filter, projection }) => {
  try {
    // const res = await axios.post(`${API_SERVICE_BACKEND}/mailboxes/read`, {
    //   payload: {
    //     filter,
    //     projection,
    //     // limit: mailBoxes.MAILBOXES_PER_CAMPAIGN,
    //   },
    // });

    let limit = emailConfig.MAILBOXES_PER_CAMPAIGN;
    let page = 1;

    if (!filter) filter = {};
    if (!projection) projection = {};
    
    const emails = await mailboxesModel
      .find(filter, projection)
      .sort({ createdAt: -1 })
      .skip((page - 1) * limit)
      .limit(limit);

    // const { data, error } = res.data;
    // if (error) {
    //   throw new Error(error);
    // }
    return emails;
  } catch (err) {
    throw new Error(err);
  }
};

const updateMailBoxes = async ({ filter, update }) => {
  try {
    // const res = await axios.patch(`${API_SERVICE_BACKEND}/mailboxes`, {
    //   payload: {
    //     filter,
    //     update,
    //   },
    // });

    if (!Boolean(Object.keys(filter).length)) {
      throw new Error('No filter options passed');
    }
    const reset = false;
    const resetQuery = {
      emailsSent: 0,
      cooling: false,
      free: true,
      nextEmailAt: 0,
      'assigned.email': null,
      'assigned.campaignId': null,
    };
    if (filter.all) {
      delete filter.all;
    }
    update = reset ? resetQuery : update;
    const updateRes = await mailboxesModel.updateMany(filter, update);
    const { n, nMatched, nModified, nUpserted } = updateRes;
    const data = { n, nMatched, nModified, nUpserted };

    // const { data, error } = res.data;
    // if (error) {
    //   throw new Error(error);
    // }
    return data;
  } catch (err) {
    throw new Error(err);
  }
};

module.exports = {
  assignMailBoxes,
  getMailBoxes,
  updateMailBoxes,
};

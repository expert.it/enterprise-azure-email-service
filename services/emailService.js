const mongoose = require('mongoose');
const { ObjectId } = require('mongodb');
const Sent_Model = require('../models/sentEmails');
const Campaign_Model = require('../models/campaign');
const leadModel = require('../models/lead');
const { parentPort, workerData, threadId } = require('node:worker_threads');
const { EmailClient } = require('@azure/communication-email');
const mailboxesApi = require('./mailBoxes');
const nodemailer = require('nodemailer');

const fetch = (...args) =>
  import('node-fetch').then(({ default: fetch }) => fetch(...args));

// const readline = require('node:readline'); // This uses the promise-based APIs
// const { stdin: input, stdout: output } = require('node:process');

const {
  MongoURI: db,
  // AZURE_RES_CONN_STRING: connectionString,
} = require('../config/keys');

const {
  workerConfig: {
    maxEmailPerRequest,
    maxEmailsPerDomain,
    MIN_LEADS,
    TIMEOUT_EMAIL_SENDING_IN_SECS,
    TIMEOUT_AI_CONTENT_GEN_IN_SECS,
  },
  followUpConfig: {},
  azureEmailService: { LIMIT_PER_HOUR },
  logger,
  OPEN_AI,
  API_SERVICE_BACKEND,
  workerConfig,
  googleWebAppConfig,
  EMAIL_TRACKING_SERVICE,
  emailConfig,
  API_SERVICE_BACKEND_2,
} = require('../config/config');

const { timeOut, random } = require('../utils/misc');
const { getHrsInMS, getMinsInMS } = require('../utils/customTime');

const {
  getCustomEmail,
  getTrackingPixel,
  getGPT_Prompt,
  getEmailStatusLink,
  getEmailFromSections,
  getSubjectFromTemps,
  emailTemplates,
  templateVariables,
  disclaimerFormatted,
  spamDict,
  emailSignature,
  options,
} = require('../data/templates');
const { strip, htmlToText, htmlToDOM } = require('../utils/parser');
const { writeJSON } = require('../utils/ReadWriteJSON');
const { default: axios } = require('axios');
const {
  getNextStepInStage,
  getNextFollowupTimeDelayInMS,
} = require('../utils/emailJourney');
const mailboxesModel = require('../models/mailboxes');

const regexChars = [
  '^',
  '$',
  '.',
  '|',
  '?',
  '*',
  '+',
  '(',
  ')',
  '[',
  ']',
  '{',
  '}',
];
const spamPatterns = [
  `\\$\\d+(\\.\\d+)?[TtBbMmKk]?`, // $100, $100.00, $100.00M, $100.00B, $100.00K, 100, 100.00, 100.00M, 100.00B, 100.00K
  `\\d+(\\.\\d+)?%`, // 100%, 100.00%
];
const spamListArr = spamDict.split(',').map((word) => {
  word = word.trim().toLowerCase();
  word = String.raw`${word}`;
  regexChars.forEach((char) => {
    word = word.replace(char, `\\${char}`);
  });
  return word;
});
// writeJSON(spamListArr, 'spamListArr.json');

const getSpamList = ({ email }, options = {}) => {
  const { ignoreList = [], isHTML = false } = options;
  if (isHTML) email = htmlToText(email);
  let spamList = [];

  spamPatterns.forEach((pattern) => {
    spamList.push(...(email.match(new RegExp(pattern, 'ig')) || []));
  });

  spamListArr.forEach((spamWord) => {
    // const pattern = new RegExp(`\\b\\w*${spamWord}\\w*\\b`, 'ig');
    const pattern = new RegExp(
      // `(?<=^|\\b|\\s|[,.()])${spamWord}(?=\\b|\\s|[,.()]|$)`,
      `(?<=\\W)${spamWord}(?=\\W)`,
      'ig',
    );
    // console.log(pattern);
    let matches = email.match(pattern) || [];
    // matches.length && console.log(matches);
    // if (spamBool) {
    spamList.push(...matches.map((match) => match.trim()));
    // }
  });
  spamList = spamList.filter((word) => Boolean(word?.length));
  spamList = spamList.filter((word) => !ignoreList.includes(word));
  return Array.from(new Set(spamList));
};

const BLOCKED_EMAILS = [
  'gautamsir076@gmail.com',
  'gautam@leadingly.io',
  'shiva@leadingly.io',
  'noah@leadingly.io',
  'noah@b2brocket.ai',
];
async function sendGMail(
  from,
  to,
  { subject, body, inReplyTo, labels, headers },
) {
  // console.log(
  //   from,
  //   to,
  //   // subject,
  //   // body,
  //   inReplyTo,
  //   labels,
  // );
  const testEmails = [
    // 'shiva@leadingly.io',
    // 'shiva@b2brocket.ai',
    // 'shivanshu981@gmail.com',
    // 'shivanshugupta120@gmail.com',
    // 'gautam@leadingly.io',
    // 'gautamsir076@gmail.com',
    // 'gautamchaurasia501@gmail.com',
    // 'noah@success.ai',
    // 'noah@leadingly.io',
    // 'noah@nftbrandsinc.com',
    // 'gregory@leadmorphix.com',
    // 'Momoloul@icloud.com', //inbox
  ];

  // select a random email from testEmails
  // const selEmail = testEmails[random(testEmails.length)];

  // select all emails from testEmails
  const selEmail = testEmails.join(',');

  selEmail && logger.warning(`TEST MODE: Sending email to ${selEmail}`);

  const option = {
    from,
    // to,
    to: selEmail || to,
    subject,
    text: '',
    html: body,
    // html: '<h1>Hi!</h1>',
    inReplyTo,
    headers: {
      'X-Gmail-Labels': labels?.join(',') || '', // Comma-separated list of labels
      ...headers,
    },
  };

  return emailClient.sendMail(option);
}

const processEmailSections = (inputText) => {
  const pattern = /\*\*(.*?)\*\*/g;
  const sections = {};
  let match;
  while ((match = pattern.exec(inputText)) !== null) {
    const sectionName = match[1];
    const startIndex = match.index + match[0].length;
    let endIndex = pattern.lastIndex;

    // Calculate the end index for the current section
    if (pattern.lastIndex < inputText.length) {
      endIndex = inputText.indexOf('**', pattern.lastIndex);
      if (endIndex === -1) {
        endIndex = inputText.length;
      }
    }

    const sectionText = inputText.slice(startIndex, endIndex).trim();
    sections[sectionName.toLowerCase()] = sectionText;
  }
  return sections;
};

const generateAIEmail = async (campaignInfo, leadInfo) => {
  const aiGen = {
    subject: null,
    // introPitch: null,
    // body: null,
    // conclusion: null,
    wholeEmail: null,
  };

  const {
    link: linkObj,
    senderName,
    senderTitle,
    userOrgName,
    recipient,
    campaignDesc,
    productDesc,
  } = campaignInfo;
  const aiParams = {
    subject: { productDesc },
    // introPitch: {
    //   campaignDesc,
    //   productDesc,
    //   customerObj: additionalInfo,
    // },
    // body: { userOrgName, campaignDesc, productDesc },
    // conclusion: { userOrgName, customerObj: additionalInfo },
    wholeEmail: {
      userLink: linkObj?.enabled ? linkObj?.url || '' : '',
      senderName,
      senderTitle,
      userOrgName,
      recipient,
      campaignDesc,
      productDesc,
      customerObj: leadInfo,
    },
  };

  const aiPromises = Object.keys(aiGen).map((key) => {
    const prompt = getGPT_Prompt[key](aiParams[key]);
    // console.log(prompt);
    return generatePersonalizedMsg(prompt);
  });

  const contentArray = await Promise.all(aiPromises);
  Object.keys(aiGen).forEach((key, index) => {
    aiGen[key] = contentArray[index];
  });
  // console.log(aiGen);
  return aiGen;
};

const generatePersonalizedMsg = async (content) => {
  // const content = getGPT_Prompt(userOrgName, payload);
  const prompt = [{ role: 'user', content }];
  const requestBody = {
    model: 'gpt-3.5-turbo',
    messages: prompt,
    temperature: 0.6,
    // max_tokens: 4096,
    top_p: 1,
    frequency_penalty: 0.1,
    presence_penalty: 0.9,
    stop: ['user:', 'assistant:'],
  };
  let result = await timeOut(
    async () =>
      await fetch(OPEN_AI.URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'api-key': `${OPEN_AI.API_KEY}`,
          Authorization: `Bearer ${OPEN_AI.API_KEY}`,
        },
        body: JSON.stringify(requestBody),
      }),
    TIMEOUT_AI_CONTENT_GEN_IN_SECS * 1000,
    { errorMessageOnTimeout: 'AI taking too long to respond' },
  );
  // let result = await fetch(OPEN_AI.URL, {
  //   method: 'POST',
  //   headers: {
  //     'Content-Type': 'application/json',
  //     Authorization: `Bearer ${OPEN_AI.API_KEY}`,
  //   },
  //   body: JSON.stringify(requestBody),
  // });

  // console.log(result);
  const res_headers = result.headers;
  const rate_limit_dets = {};
  Object.entries(res_headers.raw()).forEach(([key, value]) => {
    if (key.startsWith('x-ratelimit')) {
      rate_limit_dets[key] = value.join(', ');
    }
  });
  // console.log(rate_limit_dets);
  result = await result.json();
  const ai_msg = result.choices[0].message;
  return ai_msg.content;
};

const generatePersonalizedVideo = async (args) => {
  const { companyId, campaignId, email, urls } = args;
  try {
    const genVidRes = await axios.post(
      `https://b2brocket-hippo-video-183e2c663396.herokuapp.com/api/v1/main/saveHippoVideo`,
      {
        companyId,
        campaignId,
        email,
        urls,
      },
    );
    if (genVidRes.status === 200) {
      // console.log(genVidRes.data);
      return true;
    }
  } catch (err) {
    logger.error(`Error: generatePersonalizedVideo()`);
    console.log(`${err.response.status}: ${err.response.statusText}`);
    return false;
  }
};

const sendEmails = async ({
  campaign,
  userLink,
  recipient,
  aiGen,
  additionalInfo,
  userOrgName,
  userCompanyLocation,
  emailTemplateIndex,
}) => {
  let spamList,
    spamReplCount = 0;

  const address = [];
  if (additionalInfo?.address) {
    ['city', 'state'].forEach((key) => {
      if (additionalInfo.address[key]) {
        address.push(additionalInfo.address[key]);
      }
    });
  }
  const addressStr = address[0] || address[1] || '';

  // req prospect personalized video

  // // test data
  // additionalInfo.website_domains = additionalInfo.website_domains || [
  //   'specialtymarineproducts.net',
  // ];

  let genVideoRes = false;
  if (additionalInfo?.website_domains?.length) {
    additionalInfo.website_domains = additionalInfo.website_domains.map(
      (domain) => {
        return 'https://' + domain;
      },
    );
    const params = {
      urls: additionalInfo?.website_domains,
      campaignId: workerData.campaignId,
      leadId: additionalInfo.leadId,
      email: recipient.address,
    };
    let mxRetry = 2;
    while (mxRetry && !genVideoRes) {
      genVideoRes = await generatePersonalizedVideo(params);
      genVideoRes &&
        logger.success(`[${threadId}] Video requested successfully!`);
      mxRetry -= 1;
    }
  } else {
    logger.focus(`[${threadId}] No website domains found to generate video!`);
  }

  // console.log(userOrgName);
  // console.log(additionalInfo);

  // const customEmail = getCustomEmail({
  //   senderName,
  //   recipient,
  //   aiGen,
  //   additionalInfo,
  //   userOrgName,
  // });
  // console.log(customEmail);

  // const promptToRemoveSpammyWords = getGPT_Prompt.replceSpammyWords(
  //   customEmail.body,
  // );

  // logger.focus(`Filtering out spam words from generated email ...`);
  // const spamFilteredBody = await generatePersonalizedMsg(
  //   promptToRemoveSpammyWords,
  // );
  // console.log(spamFilteredBody);

  const emailId = new ObjectId();
  // console.log(emailId)

  // let senderEmailName = senderEmail.split('@')[0].split('.').join(' ');
  // senderEmailName =
  //   senderEmailName.slice(0, 1).toUpperCase() + senderEmailName.slice(1);

  let emailTemplate = emailTemplates[emailTemplateIndex];

  const emailDOM_temp = htmlToDOM(emailTemplate);
  emailDOM_temp
    .querySelectorAll(`#${genVideoRes ? 'text-chat' : 'text-video'}`)
    .forEach((node) => {
      // console.log(node.textContent);
      node.innerHTML = '';
    });
  emailTemplate = emailDOM_temp.body.innerHTML;
  // console.log(emailTemplate);
  // throw new Error('break');

  console.log(
    `[${threadId}] (${workerData.campaignId}) Using template @ index: ${emailTemplateIndex}`,
  );
  const staticTempsSubject = getSubjectFromTemps({ userOrgName });
  // throw new Error('break');

  const link =
    // userLink ||
    await getEmailStatusLink({
      customerObj: additionalInfo,
      emailId,
    });
  // console.log('link ->>', link);

  const trackingPixelStr = getTrackingPixel({
    leadId: additionalInfo.leadId,
    // recipient,
    // customerObj: additionalInfo,
    emailId,
  });

  const varMap = {
    // pFName: 'Gautam',
    pFName: additionalInfo.firstName || '',
    pTitle: additionalInfo.description || 'Member',
    pLocation: addressStr || '',
    pCompany: additionalInfo.organization || '',
    pIndustry: additionalInfo.industry || aiGen.emailSections.pindustry || '',
    pMission: aiGen.emailSections.pmission || '',
    pAchievements: aiGen.emailSections.pachievement || '',
    pExpertise: aiGen.emailSections.pexpertise || '',
    pImprovement: aiGen.emailSections.pareaofimprovement || '',
    userOrgName: userOrgName || '',
    title: campaign.senderTitle || '',
    description: aiGen.emailSections.description || '',
    productDesc: aiGen.emailSections.productdescription || '',
    expertise: aiGen.emailSections.expertise || '',
    caseStudy: aiGen.emailSections.casestudies || '',
    challenges: aiGen.emailSections.challenges || '',
    valueProposition: aiGen.emailSections.valueproposition || '',
    connection: aiGen.emailSections.connection || '',
    // sign:
    //   `<p>__________<br/>Best regards,<br/>${campaign.senderName}<br/>${userOrgName}</p>` ||
    //   '',
    // disclaimer:
    //   disclaimerFormatted({
    //     userOrgName,
    //     leadId: additionalInfo.leadId,
    //     link,
    //   }) || '',
    sign: '',
    disclaimer: '',
  };
  // console.log(varMap);
  // throw new Error('break');

  const staticVars = [
    'pFName',
    'pLocation',
    'pCompany',
    'userOrgName',
    'title',
  ];

  templateVariables.forEach(({ pattern, section }) => {
    // console.log(pattern, section, varMap[section]);
    const modTemp = emailTemplate.replace(
      new RegExp(pattern, 'ig'),
      // section === 'userOrgName' ? `<b>${varMap[section]}</b>` : varMap[section],
      varMap[section],
    );
    if (modTemp) {
      emailTemplate = modTemp;
    } else {
      logger.error(`${new RegExp(pattern)} ${section}`);
      console.log(varMap[section]);
    }
  });
  // console.log(emailTemplate);
  // throw new Error('break');

  const linkMatches = emailTemplate.match(/#salesGPT#(.*?)#!salesGPT#/g);
  // console.log(linkMatches);
  const replacements = linkMatches.map((match) => {
    return {
      content: strip(strip(match, '#salesGPT#'), '#!salesGPT#'),
      repl: match,
    };
  });
  // console.log(replacements);
  replacements.forEach(({ content, repl }) => {
    emailTemplate = emailTemplate.replace(
      repl,
      `<a href=${link} title=${link}>${content}</a>`,
    );
  });
  // console.log(emailTemplate);
  // throw new Error('break');

  let wholeEmail = {
    body: emailTemplate,
  };

  // let wholeEmail = getEmailFromSections({
  //   sections: aiGen.emailSections,
  //   link,
  //   leadId: additionalInfo.leadId,
  //   userOrgName,
  //   senderName: senderNameAtResource || senderEmailName,
  //   userCompanyLocation,
  // });
  // // console.log(wholeEmail);

  // wholeEmail.body += `${trackingPixelStr}`;

  // const linkStr = `<a href=${link} target="_blank"><b>Book your call</b></a>`;
  // wholeEmail.body = wholeEmail.body.replace(
  //   new RegExp(/\[(here|link)\]/, 'ig'),
  //   linkStr,
  // );

  // wholeEmail.body = wholeEmail.body.replace(
  //   new RegExp(`${userOrgName}`, 'ig'),
  //   `<a href=${link} target="_blank"><b>${userOrgName}</b></a>`,
  // );
  wholeEmail.body = wholeEmail.body.replace(
    new RegExp(/\[\s*\w+(?:\s+\w+)*\s*\]/, 'ig'),
    ``,
  );
  // wholeEmail.body = wholeEmail.body.replace(new RegExp(/#UNKNOWN/, 'ig'), ``);

  // console.log(wholeEmail.body);
  logger.focus(
    `[${threadId}] (${workerData.campaignId}) Generating Email ... <${recipient.address}>`,
  );

  // // with blackList!
  // // const spamList = spamDict.split(', ');
  // // const spamList = [''];
  // let improvisedEmail;
  // while (spamList.length) {
  //   improvisedEmail = await generatePersonalizedMsg(
  //     getGPT_Prompt.improveEmail({
  //       emailBody: wholeEmail.body,
  //       spamList: spamList.splice(0, 350).join(','),
  //     }),
  //   );
  //   logger.focus(
  //     `[${threadId}] (${workerData.campaignId}) Remaining spam words: ${spamList.length}`,
  //   );
  //   wholeEmail.body = improvisedEmail;
  // }
  // console.log(wholeEmail.body);

  // console.log(
  //   getGPT_Prompt.improveEmail({
  //     emailTemplate: emailTemplates[emailTemplateIndex],
  //     emailBody: wholeEmail.body,
  //   }),
  // );
  let improvisedEmail;
  improvisedEmail = await generatePersonalizedMsg(
    getGPT_Prompt.improveEmail({
      emailTemplate: emailTemplates[emailTemplateIndex],
      emailBody: wholeEmail.body,
    }),
  );
  wholeEmail.body = improvisedEmail;
  // console.log(wholeEmail.body);
  // throw new Error('break');

  staticVars.forEach((tempVar) => {
    let value = varMap[tempVar];
    if (!value) return;
    regexChars.forEach((char) => {
      value = value.replaceAll(char, `\\${char}`);
    });
    const pattern = new RegExp(`\\b${value}\\b`, 'g');
    wholeEmail.body = wholeEmail.body.replace(
      pattern,
      `<span id="${tempVar}">${value}</span>`,
    );
  });
  // console.log(wholeEmail.body);
  // throw new Error('break');

  spamList = getSpamList(
    { email: wholeEmail.body },
    {
      isHTML: true,
      // ignoreList: ['salesGPT']
    },
  );
  logger.warning(`Spam word count in email: ${spamList.length}`);
  console.log(spamList);
  // throw new Error('break');

  logger.warning('Fetching suitable alternatives ...');
  let spamAlternatives = await generatePersonalizedMsg(
    getGPT_Prompt.getSpamAlternatives({
      spamList,
      emailBody: wholeEmail.body,
    }),
  );
  // console.log(spamAlternatives);
  spamAlternatives = JSON.parse(spamAlternatives);
  // const spamAltFoundCount = Object.values(spamAlternatives).reduce(
  //   (acc, curr) => {
  //     curr.length && acc++;
  //     return acc;
  //   },
  //   0,
  // );
  // logger.warning(
  //   `Found alternatives to ${spamAltFoundCount}/${spamList.length} spam words`,
  // );

  // logger.warning('Unfiltered alternatives:');
  // console.log(spamAlternatives);
  const filteredAlternatives = {};
  Object.entries(spamAlternatives).forEach(([spamWord, alternatives]) => {
    let tempSpamList = getSpamList({ email: alternatives.join() });
    // console.log(tempSpamList);
    tempSpamList = tempSpamList.map((word) => {
      regexChars.forEach((char) => {
        word = word.replace(char, `\\${char}`);
      });
      return word;
    });
    if (!tempSpamList.length) {
      filteredAlternatives[spamWord] = alternatives;
      return;
    }
    const pattern = new RegExp(tempSpamList.join('|'), 'ig');
    // console.log(pattern);
    filteredAlternatives[spamWord] = alternatives.filter((alt) => {
      pattern.lastIndex = 0;
      return !pattern.test(alt);
    });
  });
  // logger.warning('Filtered alternatives:');
  // console.log(filteredAlternatives);
  // throw new Error('break');

  const spamAltFoundCount = Object.values(filteredAlternatives).reduce(
    (acc, curr) => {
      curr.length && acc++;
      return acc;
    },
    0,
  );
  logger.warning(
    `Found alternatives to ${spamAltFoundCount}/${spamList.length} spam words`,
  );

  // console.log(wholeEmail.body);
  // Object.entries(filteredAlternatives).forEach(([pattern, alternatives]) => {
  //   const regexFormBoolpattern = pattern.startsWith('$')
  //   regexChars.forEach((char) => {
  //     pattern = pattern.replace(char, `\\${char}`);
  //   });
  //   // console.log(pattern, alternatives);
  //   if (!alternatives.length) return;
  //   const alternative = alternatives[random(alternatives.length)];
  //   // console.log(pattern, alternative);
  //   const regexStrFromPattern = regexFormBoolpattern ? `${pattern}\\b` : `\\b${pattern}\\b`;
  //   const modTemp = wholeEmail.body.replace(
  //     new RegExp(regexStrFromPattern, 'ig'),
  //     alternative,
  //   );
  //   spamReplCount += 1;
  //   if (modTemp) {
  //     wholeEmail.body = modTemp;
  //   } else {
  //     logger.error(`${new RegExp(pattern)} ${alternative}`);
  //     console.log(alternatives);
  //   }
  // });
  // logger.warning(`Replaced ${spamReplCount} spam words`);

  if (spamList) {
    logger.focus(
      `[${threadId}] (${workerData.campaignId}) Removing spam words ... <${recipient.address}>`,
    );
    const singleAlt = [];
    Object.entries(filteredAlternatives).forEach(([pattern, alternatives]) => {
      singleAlt.push({
        string: pattern,
        alternative: alternatives[random(alternatives.length)],
      });
    });

    const removeSpamPrompt = getGPT_Prompt.removeSpam({
      emailBody: wholeEmail.body,
      // spamList: spamList.splice(0, 350).join(', '),
      spamAlt: singleAlt,
    });
    // console.log(removeSpamPrompt);
    improvisedEmail = await generatePersonalizedMsg(removeSpamPrompt);
    wholeEmail.body = improvisedEmail;
  } else {
    logger.warning('No spam words detected in email!');
  }
  // console.log(wholeEmail.body);

  const emailDOM = htmlToDOM(wholeEmail.body);
  // console.log(emailDOM.body.innerHTML);
  staticVars.forEach((tempVar) => {
    const value = varMap[tempVar];
    emailDOM.querySelectorAll(`#${tempVar}`).forEach((node) => {
      // console.log(node.textContent);
      node.innerHTML = value;
    });
  });
  wholeEmail.body = emailDOM.body.innerHTML;
  // console.log(wholeEmail.body);
  // throw new Error('break');

  spamList = getSpamList(
    { email: wholeEmail.body },
    {
      isHTML: true,
      // ignoreList: []
    },
  );
  logger.warning(
    `Spam word count in email (after removing spam): ${spamList.length}`,
  );
  console.log(spamList);
  // throw new Error('break');

  // never use a pattern more than once without resetting lastIndex
  // eg: pattern.lastIndex = 0;
  // const replaceTexts = [
  //   [
  //     new RegExp(`${userOrgName}`, 'ig'),
  //     `<a href=${link} target="_blank"><b>${userOrgName}</b></a>`,
  //   ],
  //   [new RegExp(/\[\s*\w+(?:\s+\w+)*\s*\]/, 'ig'), ``],
  //   [new RegExp(/#UNKNOWN/, 'ig'), ``],
  // ];
  // replaceTexts.map(([oldText, newText]) => {
  //   wholeEmail.body.replace(oldText, newText);
  // });

  // aiGen.wholeEmail += `${trackingPixelStr}`;
  // console.log(aiGen.wholeEmail);
  // console.log(wholeEmail.body);
  // throw new Error('break');

  wholeEmail.body += emailSignature({ campaign, userOrgName });
  wholeEmail.body += disclaimerFormatted({
    userOrgName,
    leadId: additionalInfo.leadId,
    link,
  });
  const emailBodyWithoutTrackingPixel = wholeEmail.body; // to prevent false trackings when viewed by admins!
  wholeEmail.body += trackingPixelStr;
  console.log(wholeEmail.body);
  // throw new Error('break');

  const message = {
    senderAddress: senderEmail,
    // content: {
    //   subject: customEmail.subject,
    //   plainText: customEmail.body,
    //   html: spamFilteredBody,
    // },
    content: {
      // subject: aiGen.subject,
      subject: staticTempsSubject,
      plainText: '',
      html: wholeEmail.body,
      // html: aiGen.wholeEmail,
    },
    recipients: {
      to: [recipient],
    },
    // attachments: [
    //   {
    //     name: path.basename(filePath),
    //     contentType: "text/plain",
    //     contentInBase64: readFileSync(filePath, "base64"),
    //   },
    // ],
  };

  let waitInt;
  try {
    // logger.warning(
    //   `Email sending process begins for ${recipi`ent.displayName} (${recipient.address})`,
    // );
    waitInt = setInterval(() => {
      logger.warning(
        `[${threadId}] Waiting for process completion ${recipient.displayName} (${recipient.address})`,
      );
    }, 10 * 1000);

    logger.focus(
      `[${threadId}] (${workerData.campaignId}) Sending Email to ${recipient.address}`,
    );
    // const poller = await timeOut(

    const info = await timeOut(
      async () => {
        const unsubscribeURL = options({
          userOrgName,
          leadId: additionalInfo.leadId,
          link,
        }).unsubscribeURL;

        // return await emailClient.beginSend(message)
        return await sendGMail(
          { name: campaign.senderName, address: senderEmail },
          recipient.address,
          {
            subject: message.content.subject,
            body: message.content.html,
            // labels: [additionalInfo.leadId, workerData.campaignId],
            labels: ['B2BRocket', userOrgName],
            headers: {
              'List-Unsubscribe': `<${unsubscribeURL}>`,
            },
          },
        );
      },
      TIMEOUT_EMAIL_SENDING_IN_SECS * 1000,
      { errorMessageOnTimeout: 'Switch' },
    );
    const emailSentStatus =
      info.accepted[0].toLowerCase() === recipient.address.toLowerCase()
        ? 'Succeeded'
        : // : info.rejected[0].toLowerCase() === recipient.address.toLowerCase()
          // ? 'Blocked'
          'Failed';
    // throw new Error('break');

    // const response = await poller.pollUntilDone();
    // console.log(response)
    // logger.warning(`Email sent to ${recipient.address}`);
    // const { id, status, error } = response;

    // console.log(info);
    // {
    //   accepted: [ 'eric.shakun@adweek.com' ],
    //   rejected: [],
    //   ehlo: [
    //     'SIZE 35882577',
    //     '8BITMIME',
    //     'AUTH LOGIN PLAIN XOAUTH2 PLAIN-CLIENTTOKEN OAUTHBEARER XOAUTH',
    //     'ENHANCEDSTATUSCODES',
    //     'PIPELINING',
    //     'CHUNKING',
    //     'SMTPUTF8'
    //   ],
    //   envelopeTime: 1148,
    //   messageTime: 889,
    //   messageSize: 1652,
    //   response: '250 2.0.0 OK  1693908609 x2-20020a170902fe8200b001bf6ea340a9sm8914038plm.159 - gsmtp',
    //   envelope: {
    //     from: 'leadingly-test8@leadingly.io',
    //     to: [ 'eric.shakun@adweek.com' ]
    //   },
    //   messageId: '<cbfe0c24-de83-4d75-63cf-5c2908f09aae@leadingly.io>'
    // }

    // console.log(id);
    if (emailSentStatus === 'Succeeded') {
      endTime = Date.now();
      const sentEmailObj = {
        // this forms a part of the message id being recieved from leads
        _id: emailId,
        // partMessageId: id.split('-').join(''),
        messageId: info.messageId,
        campaignId: workerData.campaignId,
        from: message.senderAddress,
        to: recipient.address,
        leadId: additionalInfo.leadId,
        email: {
          subject: message.content.subject,
          text: message.content.plainText,
          html: emailBodyWithoutTrackingPixel,
        },
      };
      const newSentEmail = new Sent_Model(sentEmailObj);
      await newSentEmail.save().then((res) => {
        logger.warning(
          `[${threadId}] (${workerData.campaignId}) Email saved to DB... (${recipient.address})`,
        );
        // console.log(res._id);
        logger.warning(
          `[${threadId}] (${workerData.campaignId}) email_id: ${res._id}, (${recipient.address})`,
        );
      });
    }
    // return [status, email, error];
    return [
      emailSentStatus, // 'Succeeded' | 'Blocked' | 'Failed'
      // neutralizedEmail,
      emailBodyWithoutTrackingPixel,
      null,
      emailId,
    ];
  } catch (e) {
    // console.log(e);
    logger.warning(`sendEmails() catch:`);
    logger.error(e.message);
    // const deniedByProviderPattern = /Denied/
    if (e.message === 'Switch' || e.code === 'Denied') {
      return ['Switch', null, null, null];
    }

    const invalidEmail = new RegExp(
      /Invalid format for email address|all recipients were rejected/,
      'i',
    );
    if (invalidEmail.test(e.message)) {
      return [
        'Blocked',
        "<b><span style='color:red;'>Invalid Email</span></b>",
        null,
        null,
      ];
    }

    const pattern = new RegExp(/EmailDroppedAllRecipientsSuppressed/, 'i');
    if (pattern.test(e.message)) {
      return [
        'Blocked',
        "<b><span style='color:red;'>Permission denied</span><br/>User may have opted out !</b>",
        null,
        null,
      ];
    }

    throw e;
  } finally {
    clearInterval(waitInt);
  }
};

const main = async () => {
  try {
    await mongoose
      .connect(db, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
      })
      .then()
      .catch((err) => console.log(`Worker [${threadId}]: `, err));

    const campaign = await Campaign_Model.findById(workerData.campaignId, {
      leads: 0,
    });

    if (!campaign) {
      throw new Error(
        `[${threadId}] Can't fetch campaign with id: ${workerData.campaignId}`,
      );
    }

    let {
      senderName,
      senderTitle,
      link: linkObj,
      campaignDesc,
      productDesc,
      testimonials,
      organisationName: userOrgName,
      companyName: customUserOrgName,
      companyLocation: userCompanyLocation,
      operationStat: { totalEmailed, leadsGen },
      avoidEmailToDomains,
    } = campaign._doc;

    // userOrgName = customUserOrgName = 'B2B Rocket';
    // campaignDesc =
    //   'B2B Rocket is an AI-driven email marketing agency that automates B2B meeting scheduling by sending hyper-personalized emails to ideal prospects, leading to increased revenue opportunities.';
    // productDesc = `supercharge your sales & book new revenue with AI. Are you ready to 10x your sales opportunities?`;
    // testimonials =
    //   'helped GlobalGeeks generate $10m in revenue in 2020 and got NFT Brands 50 meetings every month in 2023.';

    if (!totalEmailed) totalEmailed = 0;

    // console.log(avoidEmailToDomains);
    const domainCountInit = [];
    avoidEmailToDomains?.forEach((obj) => {
      const threshold = new Date(obj.lastSent).getTime() + getHrsInMS(24);
      // console.log(threshold, Date.now());
      if (threshold > Date.now()) return;
      domainCountInit.push([obj.domain, obj.count]);
    });
    // console.log(domainCountInit);
    const emailsPerDomainCount = new Map(domainCountInit);

    // console.log([
    //   totalEmailed,
    //   Math.min(leadsGen - totalEmailed, maxEmailPerRequest),
    // ]);

    // const { leads } = await Campaign_Model.findById(workerData.campaignId)
    //   .slice('leads', [
    //     totalEmailed,
    //     Math.min(leadsGen - totalEmailed, maxEmailPerRequest),
    //   ]) // [skip, limit]
    //   .exec();

    let domainPattern = [...emailsPerDomainCount.keys()];
    let patternSubs = [];
    const subsLim = 500;
    while (domainPattern?.length) {
      patternSubs.push(domainPattern.splice(0, subsLim)?.join('|'));
    }
    // console.log(patternSubs);
    // throw new Error('break');

    // domainPattern = domainPattern?.join('|') ? domainPattern?.join('|') : null;
    // console.log(`[${threadId}] ${[...emailsPerDomainCount.keys()].length}`);
    const totalLeads = await Campaign_Model.aggregate([
      {
        $match: { _id: ObjectId(workerData.campaignId) },
      },
      {
        $unwind: '$leads',
      },
      {
        $lookup: {
          from: 'leads', // The collection to lookup from
          localField: 'leads', // The field containing ObjectId values
          foreignField: '_id', // The field in the referenced collection to match
          as: 'leads', // The name of the field to store the replaced array
        },
      },
      {
        $unwind: '$leads', // Unwind the array of ObjectId values
      },
      {
        $match: {
          'leads.unsubscribe': { $ne: true },
          'leads.email': { $nin: [null, ''] },
          'leads.emailStatus': 0,
        },
      },
      ...patternSubs.map((pattern) => ({
        $match: {
          $expr: {
            $and: [
              // {
              //   $ne: ['$leads.email', null], // Check if leads.email is equal to 0
              // },
              // {
              //   $eq: ['$leads.emailStatus', 0], // Check if leads.emailStatus is equal to 0
              // },
              {
                $not: {
                  $regexMatch: {
                    input: '$leads.email',
                    regex: pattern,
                    options: 'i',
                  },
                },
              },
            ],
          },
        },
      })),
      {
        $count: 'total',
      },
    ]);
    const totalLeadsCount = totalLeads[0]?.total || 0;
    console.log(
      `[${threadId}] (${workerData.campaignId}) Total available leads: ${totalLeadsCount}`,
    );

    const leadsRes = await Campaign_Model.aggregate([
      {
        $match: { _id: ObjectId(workerData.campaignId) },
      },
      {
        $unwind: '$leads',
      },
      {
        $lookup: {
          from: 'leads', // The collection to lookup from
          localField: 'leads', // The field containing ObjectId values
          foreignField: '_id', // The field in the referenced collection to match
          as: 'leads', // The name of the field to store the replaced array
        },
      },
      {
        $unwind: '$leads', // Unwind the array of ObjectId values
      },
      {
        $match: {
          'leads.unsubscribe': { $ne: true },
          'leads.email': { $nin: [null, ''] },
          'leads.emailStatus': 0,
        },
      },
      ...patternSubs.map((pattern) => ({
        $match: {
          $expr: {
            $and: [
              // {
              //   $ne: ['$leads.email', null], // Check if leads.email is equal to 0
              // },
              // {
              //   $eq: ['$leads.emailStatus', 0], // Check if leads.emailStatus is equal to 0
              // },
              {
                $not: {
                  $regexMatch: {
                    input: '$leads.email',
                    regex: pattern,
                    options: 'i',
                  },
                },
              },
            ],
          },
        },
      })),
      {
        $limit: maxEmailPerRequest,
        // $limit: 35,
      },
      {
        $group: {
          _id: '$_id', // You might want to group by something meaningful here
          leads: { $push: '$leads' },
        },
      },
      // ...patternSubs.map((pattern) => ({
      //   $project: {
      //     leads: {
      //       $filter: {
      //         input: '$leads', // Specify the array field to filter
      //         as: 'lead',
      //         cond: {
      //           $and: [
      //             {
      //               $ne: ['$$lead.email', null], // Check if leads.email is equal to 0
      //             },
      //             {
      //               $eq: ['$$lead.emailStatus', 0], // Check if leads.emailStatus is equal to 0
      //             },
      //             {
      //               $not: {
      //                 $regexMatch: {
      //                   input: '$$lead.email',
      //                   regex: pattern,
      //                   options: 'i',
      //                 },
      //               },
      //             },
      //           ],
      //         }, // Specify the condition to filter by
      //       },
      //     },
      //   },
      // })),
      {
        $project: {
          // leads: { $slice: ['$leads', 0, maxEmailPerRequest] },
          leads: 1,
        },
      },
    ]);

    // console.log(leadsRes);
    let leads = leadsRes[0]?.leads || [];

    leads = leads.filter((lead) => !BLOCKED_EMAILS.includes(lead.email));
    const allLeadsCount = leads.length;

    // filter out unsubscribed emails
    const emailSubStatus = await axios.post(
      `${API_SERVICE_BACKEND_2}/emailSubStatus`,
      {
        payload: {
          emails: leads.map((lead) => lead.email),
        },
      },
    );
    const unsubscribedEmails = new Set(
      emailSubStatus.data?.data?.optout?.map(({ email }) => email),
    );
    // unsubscribedEmails.add(''); // add test emails

    // console.log(Array.from(unsubscribedEmails));
    if (unsubscribedEmails.size) {
      logger.warning('Unsubscribed emails:');
      console.log(Array.from(unsubscribedEmails));
      const unsubscribedleadObjs = leads.filter((lead) => {
        return unsubscribedEmails.has(lead.email);
      });
      const batchUpdateQuery = unsubscribedleadObjs.map(({ _id }) => {
        return {
          updateOne: {
            filter: { _id },
            update: {
              emailStatus: 2, // Blocked
            },
          },
        };
      });
      // console.log(batchUpdateQuery);
      const batchUpRes = await leadModel
        .bulkWrite(batchUpdateQuery, { ordered: false })
        .then((res) => {
          return res;
        })
        .catch((err) => {
          console.log(err);
        });
      // console.log('**Batch update res');
      // console.log(batchUpRes.result);

      totalEmailed += batchUpdateQuery.length;
      Campaign_Model.updateOne(
        {
          _id: workerData.campaignId,
          // 'leads.emailStatus': 0,
          // 'leads.email': recipient.address,
        },
        {
          $set: {
            // 'leads.$.emailStatus':
            //   emailStatus === 'Succeeded'
            //     ? 1
            //     : emailStatus === 'Blocked'
            //     ? 2
            //     : null,
            // 'leads.$.emailBody': sentEmail,
            // 'leads.$.followUpRequired': true,
            // 'leads.$.sentAt': Date.now(),
            'operationStat.totalEmailed': totalEmailed,
            campaignStatus:
              totalEmailed >= MIN_LEADS && totalEmailed === leadsGen,
            // avoidEmailToDomains: newAvoidEmailList,
          },
          // $inc: { 'operationStat.totalEmailed': 1 },
        },
        // {
        //   arrayFilters: [{ 'elem.email': { $in: emailsList } }],
        // },
      )
        .then((res) => {})
        .catch((err) => {
          console.log(err);
        });
    }
    leads = leads.filter((lead) => !unsubscribedEmails.has(lead.email));

    logger.warning(
      `[${threadId}] (${workerData.campaignId}) Filtered out ${
        allLeadsCount - leads.length
      } unsubscribed emails`,
    );
    console.log(`[${threadId}] (${workerData.campaignId}) Emails fetched:`);
    console.log(leads.map((lead, index) => `${index}. ${lead.email}`));

    // let user select recipient index
    // let emailIndex;
    // const rl = readline.createInterface({ input, output });
    // while (!emailIndex) {
    //   // emailIndex = parseInt(await rl.question('Select email index: '));
    //   emailIndex = parseInt(
    //     await new Promise((resolve) => {
    //       rl.question('Select email index: ', (answer) => {
    //         console.log(`Answer: ${answer}`);
    //         resolve(answer);
    //       });
    //     }),
    //   );
    //   console.log('Input: ', emailIndex);
    // }
    // rl.close();
    // leads = [leads[emailIndex]];

    // randomly select a lead to send email
    const recipientIndex = random(leads.length);
    leads = [...(leads[recipientIndex] ? [leads[recipientIndex]] : [])];
    console.log(`[${threadId}] (${workerData.campaignId}) Selected email:`);
    console.log(leads.map((lead) => `${recipientIndex}. ${lead.email}`));

    const emailRecipients = [];
    const promptData = [];
    const emailsList = [];
    const leadCopy = [];
    leads.forEach((lead) => {
      const {
        _id: leadId,
        emailStatus,
        email,
        firstName,
        lastName,
        description,
        organization,
        website_domains,
        linkedin_url,
        industry,
        address,
      } = lead;
      if (
        // don't email to test leads
        BLOCKED_EMAILS.includes(email)
      ) {
        return;
      }
      if (!emailStatus) {
        emailRecipients.push({
          address: email,
          displayName: `${firstName} ${lastName}`,
        });
        promptData.push({
          leadId: leadId.toString(),
          campaignId: workerData.campaignId,
          firstName,
          lastName,
          description: (description || '').trim(),
          // description: strip(strip((description || '').trim(), '\\('), '\\)'),
          organization,
          website_domains,
          linkedin_url,
          // industry,
          address,
        });
        emailsList.push(email);
        leadCopy.push(lead);
      }
    });

    // console.log(`[${threadId}] ${emailRecipients}`);
    console.log(
      `[${threadId}] (${workerData.campaignId}) Sending emails to next ${emailsList.length} leads`,
    );

    for (let i = 0; i < emailRecipients.length; i += 1) {
      const { paused } = await Campaign_Model.findOne(
        { _id: workerData.campaignId },
        { paused: 1 },
      );

      if (paused) {
        logger.error(
          `[${threadId}] (${workerData.campaignId}) Campaign Paused`,
        );
        break;
      }

      const allLeadData = leadCopy[i];
      const additionalInfo = promptData[i];
      // console.log(additionalInfo);
      // additionalInfo.firstName = 'Momo Loul';
      const recipient = emailRecipients[i];
      // console.log(recipient)

      if (!recipient.address) continue;

      const tempDomainParts =
        recipient.address?.split('@')?.[1]?.split('.') || [];
      tempDomainParts.pop();
      const emailDomain = tempDomainParts.join('.');
      // console.log(emailsPerDomainCount);
      // if (emailsPerDomainCount.get(emailDomain) >= maxEmailsPerDomain) {
      //   logger.warning(
      //     `[${threadId}] (${workerData.campaignId}) Limit reached for max allowed emails to domain: [${emailDomain}] - (${recipient.address})`,
      //   );
      //   continue;
      // }

      const aiGen = {
        // subject: null,
        // introPitch: null,
        // body: null,
        // conclusion: null,
        // wholeEmail: null,
        emailSections: null,
      };
      const aiParams = {
        // subject: { productDesc },
        // introPitch: {
        //   campaignDesc,
        //   productDesc,
        //   customerObj: additionalInfo,
        // },
        // body: { userOrgName, campaignDesc, productDesc },
        // conclusion: { userOrgName, customerObj: additionalInfo },
        // wholeEmail: {
        //   userLink: linkObj?.enabled ? linkObj?.url || '' : '',
        //   senderName,
        //   senderTitle,
        //   userOrgName,
        //   recipient,
        //   campaignDesc,
        //   productDesc,
        //   customerObj: additionalInfo,
        // },
        emailSections: {
          userLink: linkObj?.enabled ? linkObj?.url || '' : '',
          // senderName,
          senderName:
            senderNameAtResource ||
            (() => {
              let senderEmailName = senderEmail
                .split('@')[0]
                .split('.')
                .join(' ');
              senderEmailName =
                senderEmailName.slice(0, 1).toUpperCase() +
                senderEmailName.slice(1);
              return senderEmailName;
            })(),
          userOrgName: customUserOrgName || userOrgName,
          recipient,
          campaignDesc,
          productDesc,
          testimonials,
          customerObj: additionalInfo,
          emailTemplateIndex: random(emailTemplates.length),
        },
      };

      // const personalizedMsg = await generatePersonalizedMsg(
      //   userOrgName,
      //   additionalInfo,
      // );

      // console.log(aiParams);

      logger.focus(
        `[${threadId}] (${workerData.campaignId}) Fetching AI ... <${recipient.address}>`,
      );
      const aiPromises = Object.keys(aiGen).map(async (key) => {
        const prompt = getGPT_Prompt[key](aiParams[key]);
        // console.log(prompt);
        let aiRes = await generatePersonalizedMsg(prompt);
        if (key === 'emailSections') {
          aiRes = processEmailSections(aiRes);
        }
        return aiRes;
      });

      const contentArray = await Promise.all(aiPromises);
      Object.keys(aiGen).forEach((key, index) => {
        aiGen[key] = contentArray[index];
      });

      // console.log(aiGen);
      // console.log(aiGen.wholeEmail);
      // console.log(aiGen.emailSections);
      // throw new Error('break');

      // await new Promise((resolve, _) => {
      //   console.log('Waiting for 5 secs');
      //   setTimeout(() => {
      //     resolve('Done waiting!');
      //   }, 5000);
      // });

      let emailStatus, sendError;
      [emailStatus, sentEmail, sendError, emailId] = await sendEmails({
        userLink: linkObj?.enabled ? linkObj?.url || '' : '',
        campaign: {
          senderName: aiParams.emailSections.senderName,
          senderTitle,
          campaignDesc,
          productDesc,
          testimonials,
        },
        recipient,
        aiGen,
        additionalInfo,
        userOrgName: customUserOrgName || userOrgName,
        userCompanyLocation,
        emailTemplateIndex: aiParams.emailSections.emailTemplateIndex,
      });

      if (emailStatus === 'Succeeded' || emailStatus === 'Blocked') {
        const currCt = emailsPerDomainCount.get(emailDomain);
        emailsPerDomainCount.set(emailDomain, (isNaN(currCt) ? 0 : currCt) + 1);

        emailResource.emailsSent += 1;

        logger.success(
          `[${threadId}] (${workerData.campaignId}) Email sent to ${
            recipient.address
          } by (${id}: ${senderEmail}), Status: ${
            emailResource.emailsSent
            // }/${Math.floor(emailResource.maxAllowed / 2)}`,
          }/${Math.floor(emailResource.maxAllowed)}`,
        );

        totalEmailed += 1;
        // throw new Error('break');

        const newAvoidEmailList = [];
        // uncomment this if you want to limit emails to domain!
        // emailsPerDomainCount.forEach((count, domain) => {
        //   // console.log(count, domain);
        //   newAvoidEmailList.push({
        //     domain,
        //     count,
        //     lastSent: Date.now(),
        //   });
        // });
        // console.log(newAvoidEmailList);

        const journeyDets = getNextStepInStage(
          allLeadData.enrolledFor,
          allLeadData.nextFollowUpStep,
        );
        // console.log(JSON.stringify(journeyDets));
        // throw new Error('break');

        const leadUpRes = await leadModel.updateOne(
          {
            // email: recipient.address,
            // campaignIds: { $in: [ObjectId(workerData.campaignId)] },
            _id: ObjectId(additionalInfo.leadId),
          },
          {
            $set: {
              emailStatus:
                emailStatus === 'Succeeded'
                  ? 1
                  : emailStatus === 'Blocked'
                  ? 2
                  : null,
              emailBody: sentEmail,
              nextFollowUpStep: journeyDets.nextStep,
              nextFollowUpAt:
                Date.now() +
                getNextFollowupTimeDelayInMS(
                  journeyDets.stage,
                  journeyDets.nextStep,
                ),
              [`emailSeqStatus.${journeyDets.stage}.prospect.${journeyDets.currentStep}`]:
                ObjectId(emailId),
              followUpRequired: true,
              sentAt: Date.now(),
            },
          },
        );
        // console.log(leadUpRes);

        const updateRes = await Campaign_Model.updateOne(
          {
            _id: workerData.campaignId,
            // 'leads.emailStatus': 0,
            // 'leads.email': recipient.address,
          },
          {
            $set: {
              // 'leads.$.emailStatus':
              //   emailStatus === 'Succeeded'
              //     ? 1
              //     : emailStatus === 'Blocked'
              //     ? 2
              //     : null,
              // 'leads.$.emailBody': sentEmail,
              // 'leads.$.followUpRequired': true,
              // 'leads.$.sentAt': Date.now(),
              'operationStat.totalEmailed': totalEmailed,
              campaignStatus:
                totalEmailed >= MIN_LEADS && totalEmailed === leadsGen,
              avoidEmailToDomains: newAvoidEmailList,
            },
            // $inc: { 'operationStat.totalEmailed': 1 },
          },
          // {
          //   arrayFilters: [{ 'elem.email': { $in: emailsList } }],
          // },
        );

        const maxGapInMins = emailConfig.maxEmailingGapInMins;
        const nextEmailAfterInMins = Math.max(2, random(maxGapInMins + 1)); // returns a random num between '0 <-> arg-1'
        // const nextEmailAfterInMins = 0;
        emailResource.nextEmailAt =
          Date.now() + getMinsInMS(nextEmailAfterInMins);
        await mailboxesApi
          .updateMailBoxes({
            filter: {
              _id: emailResource._id,
            },
            update: [
              {
                $set: {
                  // emailsSent: { $add: ['$emailsSent', 1] },
                  emailsSent: emailResource.emailsSent,
                  nextEmailAt: emailResource.nextEmailAt,
                },
              },
            ],
          })
          .then((res) => {
            if (!res) {
              logger.error(
                `[${emailResource.senderEmail}] Error updating email count `,
              );
              return;
            }
            console.log(`[${emailResource.senderEmail}] email count updated!`);
            console.log(res);
          })
          .catch((err) => {
            logger.error(
              `[${emailResource.senderEmail}] Error updating email count `,
            );
            console.log(err);
          });

        // // req prospect personalized video

        // // // test data
        // // additionalInfo.website_domains = additionalInfo.website_domains || [
        // //   'specialtymarineproducts.net',
        // // ];

        // if (
        //   emailStatus === 'Succeeded' &&
        //   additionalInfo?.website_domains?.length
        // ) {
        //   additionalInfo.website_domains = additionalInfo.website_domains.map(
        //     (domain) => {
        //       return 'https://' + domain;
        //     },
        //   );
        //   const params = {
        //     urls: additionalInfo?.website_domains,
        //     campaignId: workerData.campaignId,
        //     leadId: additionalInfo.leadId,
        //     email: recipient.address,
        //   };
        //   let genVideoRes = false,
        //     mxRetry = 2;
        //   while (mxRetry && !genVideoRes) {
        //     genVideoRes = await generatePersonalizedVideo(params);
        //     genVideoRes &&
        //       logger.success(`[${threadId}] Video requested successfully!`);
        //     mxRetry -= 1;
        //   }
        // } else {
        //   logger.focus(
        //     `[${threadId}] No website domains found to generate video!`,
        //   );
        // }

        // release resources once campaign is completed
        if (totalEmailed >= workerConfig.MIN_LEADS) {
          await mailboxesApi
            .updateMailBoxes({
              filter: {
                'assigned.campaignId': workerData.campaignId,
              },
              update: {
                $set: {
                  'assigned.campaignId': null,
                  'assigned.email': null,
                },
              },
            })
            .then((res) => {
              if (!res) {
                logger.error(
                  `[${emailResource.senderEmail}] Can't relese resource!`,
                );
                return;
              }
              console.log(
                `[${emailResource.senderEmail}] Resource released successfully`,
              );
              console.log(res);
            })
            .catch((err) => {
              logger.error(
                `[${emailResource.senderEmail}] Can't relese resource!`,
              );
              console.log(err);
            });
        }

        parentPort.postMessage({
          status: 2,
          data: totalEmailed,
        });

        if (
          emailResource.emailsSent >=
          // Math.min(Math.floor(emailResource.maxAllowed / 2), LIMIT_PER_HOUR)
          Math.min(Math.floor(emailResource.maxAllowed), LIMIT_PER_HOUR)
        ) {
          logger.focus(
            `[${threadId}] (${workerData.campaignId}) Switching resource (Hourly/Daily limit reached) (${id}: ${senderEmail})`,
          );
          emailResource.cooling = true;
          emailResource.lastActive = Date.now();
          break;
        }
        console.log(
          `[${threadId}] (${workerData.campaignId}) Waiting for ${nextEmailAfterInMins} mins before sending next email! <${senderEmail}>`,
        );
        // if (!nextEmailAfterInMins || nextEmailAfterInMins <= 2) {
        if (nextEmailAfterInMins <= 2) {
          await new Promise((resolve, reject) => {
            setTimeout(resolve, getMinsInMS(nextEmailAfterInMins));
          });
          continue;
        }
        break;
      } else if (emailStatus === 'Switch') {
        logger.focus(
          `[${threadId}] (${workerData.campaignId}) Switching resource (Unresponsive) (${id}: ${senderEmail})`,
        );
        emailResource.cooling = true;
        emailResource.lastActive = Date.now();
        break;
      } else {
        logger.error(
          `[${threadId}] (${workerData.campaignId}) Sending emails failed, trying again!`,
        );
        console.log(emailStatus);
        // await new Promise((resolve, reject) => {
        //   console.log(`Waiting for 30 mins before proceeding!`);
        //   setTimeout(resolve, getMinsInMS(30));
        // });
        throw new Error(sendError);
      }
    }

    emailResource.free = true;
    parentPort.postMessage({
      status: 1,
      data: emailResource,
    });
  } catch (e) {
    console.log(`main() catch:`);
    console.log(e);
    emailResource.free = true;
    parentPort.postMessage({
      status: 0,
      error: e.message,
      data: emailResource,
    });
  }
};

const tokenStore = {};
const isAccessTokenValid = (generationTime) => {
  const currentTime = new Date();
  const elapsedMilliseconds = currentTime - generationTime;
  const elapsedHours = elapsedMilliseconds / 1000;
  return elapsedHours < 59 * 60 + 50; // Assume it can take 10s to update the db after fetching token form API.
};
const getAccessToken = async (refresh_token) => {
  // const result = await fetch(
  //   `${API_SERVICE_BACKEND}/mailboxes/refreshToken?refresh_token=${refresh_token}`,
  //   {
  //     method: 'GET',
  //     headers: {
  //       'Content-Type': 'application/json',
  //     },
  //     // body: JSON.stringify(requestBody),
  //   },
  // );

  if (
    tokenStore[refresh_token] &&
    isAccessTokenValid(tokenStore[refresh_token].generationTime)
  ) {
    // Validate access_token
    return tokenStore[refresh_token].accessToken;
  } else {
    const data = {
      client_id: emailConfig.googleWebAppConfig.client_id,
      client_secret: emailConfig.googleWebAppConfig.client_secret,
      refresh_token,
      grant_type: 'refresh_token',
    };
    // Send the POST request to exchange the code for an access token
    const tokenReq = await axios.post(
      emailConfig.googleWebAppConfig.token_url,
      data,
    );
    const token_obj = tokenReq.data;

    // const filter = { 'provider_data.refresh_token': refresh_token };
    // const update = { provider_data: { refresh_token, ...token_obj } };
    // await mailboxesModel.updateMany(filter, update);

    // const resJson = await result.json();
    // const { access_token } = resJson;
    const { access_token } = token_obj;
    tokenStore[refresh_token] = {
      accessToken: access_token,
      generationTime: new Date(),
    };

    return access_token;
  }
};

const providerServiceMap = {
  google: 'gmail',
};

if (require.main === module) {
  console.log(`Worker: ${threadId}`);
  workerData.emailResource = JSON.parse(workerData.emailResource);
  // console.log(workerData);
  var { emailResource } = workerData;
  var {
    _id: id,
    // connectionString
    // id,
    // connStr: connectionString,
    senderEmail,
    fullName: senderNameAtResource,
    emailsSent,
    provider,
    provider_data,
    maxAllowed,
  } = emailResource;

  // var emailClient = new EmailClient(connectionString);
  var emailClient = null;

  getAccessToken(provider_data.refresh_token)
    .then((access_token) => {
      emailClient = nodemailer.createTransport({
        service: providerServiceMap[provider],
        auth: {
          type: 'OAuth2',
          user: senderEmail,
          accessToken: access_token,
          refreshToken: provider_data.refresh_token,
          clientId: googleWebAppConfig.client_id,
          clientSecret: googleWebAppConfig.client_secret,
        },
      });

      logger.focus(
        `[${threadId}] (${
          workerData.campaignId
        }) Using resource: id -> ${id}: ${senderNameAtResource} <${senderEmail}>, Sent: ${emailsSent} / ${Math.floor(
          // maxAllowed / 2,
          maxAllowed,
        )}`,
      );
      main();
    })
    .catch((err) => {
      logger.error(`catch: getAccessToken()`);
      logger.error(err);
    });
}

module.exports = {
  generatePersonalizedMsg,
  generateAIEmail,
  processEmailSections,
  sendGMail,
  sendEmails,
  providerServiceMap,
  getAccessToken,
};

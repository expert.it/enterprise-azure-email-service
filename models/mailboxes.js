const mongoose = require('mongoose');
const { mailBoxes } = require('../config/config');

const mailboxesSchema = new mongoose.Schema({
  connectionString: {
    type: String,
    default: null,
  },
  // password: {
  //   type: String,
  //   default: '#undefined',
  // },
  provider: {
    type: String,
    dafault: null,
  },
  provider_data: {
    type: Object,
    default: {
      // expires_in: Number(seconds)
    },
  },
  senderEmail: {
    type: String,
    required: true,
  },
  fullName: {
    // must be matching with senderEmail
    type: String,
    default: '',
  },
  createdBy: {
    type: String,
    default: 'auto',
  },
  assigned: {
    type: Object,
    default: {
      // email
      // campaignId
    },
  },
  emailsSent: {
    type: Number,
    default: 0,
  },
  followUpsSent: {
    type: Number,
    default: 0,
  },
  inWeek: {
    type: Number,
    default: 1,
    max: 10,
  },
  maxAllowed: {
    type: Number,
    default: 8,
    max: 20,
    // max: [
    //   mailBoxes.maxAllowedEmailsPerDay,
    //   (value, path, model, kind) =>
    //     `Can't send more than ${mailBoxes.maxAllowedEmailsPerDay} emails per day! Input=${value} @${path} in Model: ${model}. More: ${kind}`,
    // ],
  },
  nextEmailAt: {
    type: Date,
    default: 0,
  },
  // for ramp up
  lastWarmupUpdate: {
    type: Date,
    default: Date.now,
  },
  // to reset emails sent count
  lastReset: {
    type: Date,
    default: Date.now,
  },
  // to determine cooling
  lastActive: {
    type: Date,
    default: 0,
  },
  cooling: {
    type: Boolean,
    default: false,
  },
  free: {
    type: Boolean,
    default: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});
const mailboxesModel = mongoose.model('mailbox', mailboxesSchema);
module.exports = mailboxesModel;

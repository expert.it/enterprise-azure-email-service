const mongoose = require('mongoose');

const dailyUpdateSchema = new mongoose.Schema({
  from: { type: String, required: true },
  to: { type: String, required: true },
  email: {
    type: Object,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});
const DailyUpdate_Model = mongoose.model('dailyUpdate', dailyUpdateSchema);
module.exports = DailyUpdate_Model;

const mongoose = require("mongoose");
const URLSchema = new mongoose.Schema({
    uniqueId: String,
    originalURL: String
});
const URLModel = mongoose.model('urlmodel', URLSchema);
module.exports = URLModel;
const mongoose = require('mongoose');

const sentSchema = new mongoose.Schema({
  _id: { type: mongoose.Schema.Types.ObjectId, require: true },
  partMessageId: { type: String, default: null },
  messageId: { type: String },
  campaignId: { type: String, required: true },
  from: { type: String, required: true },
  to: { type: String, required: true },
  leadId: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
  },
  email: {
    type: Object,
    required: true,
  },
  followUp: {
    type: Boolean,
    default: false,
  },
  opened: {
    type: [Date],
    default: [],
  },
  clicked: {
    type: [Date],
    default: [],
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});
const Sent_Model = mongoose.model('sentEmail', sentSchema);
module.exports = Sent_Model;

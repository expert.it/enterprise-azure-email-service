const mongoose = require('mongoose');

const campaignSchema = new mongoose.Schema({
  organisationName: { type: String, required: true },
  clientEmail: {
    type: String,
    required: true,
  },
  // Used to infer campaign completion (both fetching leads and sending emails)
  campaignStatus: { type: Boolean, default: false },
  // For resuming or haulting the email sending process
  paused: { type: Boolean, default: true },
  operationStat: {
    type: {
      // No. of leads to fetch today, could be inconsistent: is handled in Email service !
      remToday: {
        type: Number,
        required: true,
      },
      lastActive: {
        type: Date,
      },
      leadsGen: {
        type: Number,
        default: 0,
      },
      // page param in the apollo req, helps managing duplicacy
      nextPage: { type: Number, default: 1 },
      nextDomainPage: { type: Number, default: 1 },
      totalEmailed: { type: Number, default: 0 },
      emailsOpened: { type: Number, default: 0 },
      meetings: { type: Number, default: 0 },
    },
    required: true,
  },
  subscription: {
    type: {
      dailyLimit: {
        type: Number,
        required: true,
      },
    },
    required: true,
  },
  addFilters: {
    type: {
      compSize: { type: [String] },
      keywords: { type: [String] },
      revenue: { type: [Number] },
      funding: { type: [Number] },
      location: { type: [String] },
      blockedDomains: { type: [String] },
      blockedIndustries: { type: [String] },
      blockedLocations: { type: [String] },
      blockedKeywords: { type: [String] },
    },
    default: {
      compSize: [],
      keywords: [],
      revenue: [],
      funding: [],
      location: [],
      blockedDomains: [],
      blockedIndustries: [],
      blockedLocations: [],
      blockedKeywords: [],
    },
  },
  avoidLeadsFromDomains: {
    type: [
      {
        domain: { type: String, required: true },
        count: { type: Number, default: 0, required: true },
        lastSent: { type: Date, required: true },
      },
    ],
    default: [],
  },
  avoidEmailToDomains: {
    type: [
      {
        domain: { type: String, required: true },
        count: { type: Number, default: 0, required: true },
        lastSent: { type: Date, required: true },
      },
    ],
    default: [],
  },
  leads: {
    // type: [
    //   {
    //     firstName: { type: String, required: true },
    //     lastName: { type: String, required: true },
    //     email: { type: String, required: true },
    //     linkedin_url: { type: String, required: true },
    //     description: { type: String, required: true },
    //     organization: { type: String, required: true },
    //     industry: { type: String, required: true },
    //     address: {
    //       type: {
    //         city: { type: String, required: true },
    //         state: { type: String, required: true },
    //         country: { type: String, required: true },
    //       },
    //     },
    //     // 1 -> sent, 0 -> not sent, 2 -> blocked
    //     emailStatus: { type: Number, default: 0 },
    //     emailBody: { type: String },
    //     // set to true once email is sent
    //     followUpRequired: { type: Boolean, default: false },
    //     opened: { type: Boolean, default: false },
    //     sentAt: { type: Date },
    //   },
    // ],
    type: [mongoose.Schema.Types.ObjectId],
    default: [],
  },
  senderName: {
    type: String,
    required: true,
  },
  senderTitle: {
    type: String,
    required: true,
  },
  companyName: {
    type: String,
    required: true,
  },
  companyLocation: {
    type: String,
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  productDesc: {
    type: String,
    required: true,
  },
  industry: {
    type: [String],
    required: true,
  },
  targets: {
    type: [String],
    required: true,
  },
  campaignDesc: {
    type: String,
    required: true,
  },
  link: {
    type: Object,
    default: {
      // url: {def: ""}
      // enabled: {Boolean, def: false}
    },
  },
  // Indicates status for leads generation process
  // status -> 0: failed, 1: in-progress, 2: success
  status: {
    type: Number,
    default: 1,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});
const campaign = mongoose.model('campaign', campaignSchema);
module.exports = campaign;

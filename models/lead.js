const mongoose = require('mongoose');

const leadSchema = new mongoose.Schema({
  firstName: { type: String, required: true },
  lastName: { type: String, default: '' },
  profile_pic: { type: String, default: null },
  email: { type: String, default: null },
  linkedin_url: { type: String, default: '' },
  description: { type: String, default: '' },
  organization: { type: String, required: true },
  website_domains: { type: String, default: [] },
  industry: { type: String, required: true },
  address: {
    type: {
      city: { type: String, default: '' },
      state: { type: String, default: '' },
      country: { type: String, default: '' },
    },
  },
  rocketReach_id: {
    type: Number,
  },
  campaignIds: {
    type: [mongoose.Schema.Types.ObjectId],
    default: [],
  },
  // 1 -> sent, 0 -> not sent, 2 -> blocked
  emailStatus: { type: Number, default: 0 },
  emailBody: { type: String },
  // set to true once email is sent
  // enum: 'initial, 'conversation', 'meeting', 'none'
  enrolledFor: {
    type: String,
    default: 'initial',
  },
  // enum: 'step{X}', none
  nextFollowUpStep: {
    type: String,
    default: '1',
  },
  nextFollowUpAt: {
    type: Date,
    default: null,
  },
  emailSeqStatus: {
    type: Object,
    default: {
      // initial: {
      //   client: Object,
      //   prospect: {
      //     step1: emailId, // all the objects are similar to this
      //     step2: emailId,
      //     ...
      //   },
      // },
      // conversation: {
      //   client: Object,
      //   prospect: Object,
      // },
      // meeting: {
      //   client: Object,
      //   prospect: Object,
      // },
    },
  },
  followUpRequired: { type: Boolean, default: false },
  opened: { type: Boolean, default: false },
  openedAt: { type: [Date], default: [] },
  sentAt: { type: Date },
});
const leadModel = mongoose.model('lead', leadSchema);
module.exports = leadModel;

[
  {
    _id: '64f86e776b098b0056e47688',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 88 },
      { date: '2023-09-27', count: 82 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '650a0ea1e8ffb40057091a96',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 14 },
      { date: '2023-09-27', count: 8 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '64dfbc414fd4a2005213053f',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 64 },
      { date: '2023-09-27', count: 40 },
      { date: '2023-09-26', count: 28 },
    ],
  },
  {
    _id: '6501db1541e1030057015bf0',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 64 },
      { date: '2023-09-27', count: 2 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '650af941d6bd720056000bfe',
    emailsSent: [
      { date: '2023-09-29', count: 5 },
      { date: '2023-09-28', count: 19 },
      { date: '2023-09-26', count: 18 },
    ],
  },
  {
    _id: '64bd9c05c29f0400501e6a14',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 51 },
      { date: '2023-09-27', count: 98 },
      { date: '2023-09-26', count: 26 },
    ],
  },
  {
    _id: '650305039a9f51005707116e',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 6 },
      { date: '2023-09-27', count: 4 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '650303d49a9f5100570710eb',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 39 },
      { date: '2023-09-27', count: 92 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '6501bf7e41e1030057014e92',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 81 },
      { date: '2023-09-27', count: 58 },
      { date: '2023-09-26', count: 21 },
    ],
  },
  {
    _id: '6501d9a141e1030057015989',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 32 },
      { date: '2023-09-27', count: 2 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '6509df9e6cd19b0057bd87d4',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 70 },
      { date: '2023-09-27', count: 58 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '6501b7f741e1030057014bc1',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 30 },
      { date: '2023-09-27', count: 58 },
      { date: '2023-09-26', count: 17 },
    ],
  },
  {
    _id: '64f8e1d18ac29600579fb7ed',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 104 },
      { date: '2023-09-27', count: 113 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '64badbe26696950051c6f6f2',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 97 },
      { date: '2023-09-27', count: 75 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '64f864b06b098b0056e46e81',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 83 },
      { date: '2023-09-27', count: 71 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '64f8bbe38ac29600579fa4a3',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 53 },
      { date: '2023-09-27', count: 36 },
      { date: '2023-09-26', count: 26 },
    ],
  },
  {
    _id: '6501da5b41e1030057015a6f',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 45 },
      { date: '2023-09-27', count: 74 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '64bada896696950051c6f621',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 36 },
      { date: '2023-09-27', count: 54 },
      { date: '2023-09-26', count: 28 },
    ],
  },
  {
    _id: '650b4b6648a76e0056cbcb29',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 22 },
      { date: '2023-09-27', count: 31 },
      { date: '2023-09-26', count: 20 },
    ],
  },
  {
    _id: '64be4538e015980051c5648e',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 85 },
      { date: '2023-09-27', count: 123 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '64e6471619ef4400528b670f',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 117 },
      { date: '2023-09-27', count: 68 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '650c50c848a76e0056cc2ca6',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 48 },
      { date: '2023-09-27', count: 52 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '64bd95edc29f0400501e676a',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 79 },
      { date: '2023-09-27', count: 99 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '6511330d242ede0056ad34f2',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 29 },
      { date: '2023-09-27', count: 94 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '64f7295aace6fe0056dbcccb',
    emailsSent: [{ date: '2023-09-26', count: 21 }],
  },
  {
    _id: '64f9bb534fb0ab00573cb286',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 75 },
      { date: '2023-09-27', count: 92 },
      { date: '2023-09-26', count: 26 },
    ],
  },
  {
    _id: '64f9b2d34fb0ab00573cac09',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 60 },
      { date: '2023-09-27', count: 46 },
      { date: '2023-09-26', count: 26 },
    ],
  },
  {
    _id: '6501ca2941e1030057015352',
    emailsSent: [
      { date: '2023-09-27', count: 12 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '650329af9a9f510057073018',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 94 },
      { date: '2023-09-27', count: 68 },
      { date: '2023-09-26', count: 21 },
    ],
  },
  {
    _id: '650331769a9f5100570735c4',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 11 },
      { date: '2023-09-27', count: 2 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '650c50b348a76e0056cc2c6d',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 12 },
      { date: '2023-09-27', count: 18 },
      { date: '2023-09-26', count: 20 },
    ],
  },
  {
    _id: '650de24fe03ad30056f2865c',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 73 },
      { date: '2023-09-27', count: 1 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '64e645f219ef4400528b66c6',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 89 },
      { date: '2023-09-27', count: 101 },
      { date: '2023-09-26', count: 26 },
    ],
  },
  {
    _id: '650de2a9e03ad30056f287ba',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 76 },
      { date: '2023-09-27', count: 47 },
      { date: '2023-09-26', count: 19 },
    ],
  },
  {
    _id: '6501ebf641e1030057016bfb',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 61 },
      { date: '2023-09-27', count: 82 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '65034e5ca857170056a31d69',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 36 },
      { date: '2023-09-27', count: 62 },
      { date: '2023-09-26', count: 20 },
    ],
  },
  {
    _id: '64de46bef3518c0051072d72',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 74 },
      { date: '2023-09-27', count: 36 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '64be2c2621a5c40051a790b1',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 53 },
      { date: '2023-09-27', count: 123 },
      { date: '2023-09-26', count: 26 },
    ],
  },
  {
    _id: '6504a693cdcc190057bc36c9',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 19 },
      { date: '2023-09-27', count: 36 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '650afa64d6bd720056000d8e',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 13 },
      { date: '2023-09-27', count: 1 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '64f8e1978ac29600579fb7cc',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 13 },
      { date: '2023-09-27', count: 54 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '64f8bc128ac29600579fa4d1',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 118 },
      { date: '2023-09-27', count: 34 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '650b38690edad800568b9a82',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 62 },
      { date: '2023-09-27', count: 39 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '65009238e18c4f0056342240',
    emailsSent: [
      { date: '2023-09-29', count: 1 },
      { date: '2023-09-28', count: 46 },
      { date: '2023-09-27', count: 36 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '64fa064ad6029a00588680d5',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 120 },
      { date: '2023-09-27', count: 78 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '64dd25544dc90a0051abc721',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 58 },
      { date: '2023-09-27', count: 122 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '650de1ece03ad30056f285be',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 11 },
      { date: '2023-09-27', count: 1 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '64be6b9b0e12210050407800',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 37 },
      { date: '2023-09-27', count: 69 },
      { date: '2023-09-26', count: 29 },
    ],
  },
  {
    _id: '64d6aa6cbf48340050a102e1',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 105 },
      { date: '2023-09-27', count: 39 },
      { date: '2023-09-26', count: 29 },
    ],
  },
  {
    _id: '6501ec41d66ffd005736eb29',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 50 },
      { date: '2023-09-27', count: 85 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '64e6446b19ef4400528b6637',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 52 },
      { date: '2023-09-27', count: 40 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '650b4b1848a76e0056cbca8d',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 12 },
      { date: '2023-09-26', count: 15 },
    ],
  },
  {
    _id: '64bd9d2121fb1f0051944aca',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 45 },
      { date: '2023-09-27', count: 51 },
      { date: '2023-09-26', count: 29 },
    ],
  },
  {
    _id: '64de6d5ff3518c005107439d',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 88 },
      { date: '2023-09-27', count: 114 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '64f9bdc1e4089a0056f9627c',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 114 },
      { date: '2023-09-27', count: 26 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '64b6e15776a2bb00500781a4',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 131 },
      { date: '2023-09-27', count: 43 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '64bd88f0c29f0400501e60c5',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 68 },
      { date: '2023-09-27', count: 94 },
      { date: '2023-09-26', count: 27 },
    ],
  },
  {
    _id: '64de4621f3518c0051072d14',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 39 },
      { date: '2023-09-27', count: 87 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '6500930ce18c4f005634235b',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 54 },
      { date: '2023-09-27', count: 8 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '64be38e911dfb0005056c288',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 45 },
      { date: '2023-09-27', count: 114 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '64dfbde94fd4a20052130778',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 107 },
      { date: '2023-09-27', count: 90 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '65125075bbd6610057bc16b3',
    emailsSent: [
      { date: '2023-09-28', count: 18 },
      { date: '2023-09-27', count: 76 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '6504690859365a0056f53e77',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 64 },
      { date: '2023-09-27', count: 19 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '650c50f748a76e0056cc2d05',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 55 },
      { date: '2023-09-27', count: 54 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '64f8bb188ac29600579fa35e',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 41 },
      { date: '2023-09-27', count: 20 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '64e4d46af05550005109c7aa',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 90 },
      { date: '2023-09-27', count: 18 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '64ba88e15a101c0050efe3c2',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 96 },
      { date: '2023-09-27', count: 123 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '64f9bcfde4089a0056f961c4',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 124 },
      { date: '2023-09-27', count: 72 },
      { date: '2023-09-26', count: 27 },
    ],
  },
  {
    _id: '64fa065ad6029a00588680f5',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 32 },
      { date: '2023-09-27', count: 98 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '64ee3f11918d8d0055a323f7',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 29 },
      { date: '2023-09-27', count: 112 },
      { date: '2023-09-26', count: 28 },
    ],
  },
  {
    _id: '64dd2d494dc90a0051abd32f',
    emailsSent: [
      { date: '2023-09-28', count: 70 },
      { date: '2023-09-27', count: 76 },
      { date: '2023-09-26', count: 20 },
    ],
  },
  {
    _id: '65008938e18c4f0056341b57',
    emailsSent: [
      { date: '2023-09-29', count: 1 },
      { date: '2023-09-28', count: 21 },
      { date: '2023-09-27', count: 83 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '6500893be18c4f0056341b59',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 21 },
      { date: '2023-09-27', count: 2 },
      { date: '2023-09-26', count: 20 },
    ],
  },
  {
    _id: '6501db6641e1030057015cb2',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 6 },
      { date: '2023-09-27', count: 49 },
      { date: '2023-09-26', count: 21 },
    ],
  },
  {
    _id: '64e8c63ee91350005862c9b5',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 79 },
      { date: '2023-09-27', count: 81 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '65008af2e18c4f0056341ce7',
    emailsSent: [
      { date: '2023-09-29', count: 1 },
      { date: '2023-09-28', count: 21 },
      { date: '2023-09-27', count: 2 },
      { date: '2023-09-26', count: 21 },
    ],
  },
  {
    _id: '64de10e0f3518c005106f69a',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 121 },
      { date: '2023-09-27', count: 68 },
      { date: '2023-09-26', count: 27 },
    ],
  },
  {
    _id: '650090dbe18c4f0056342164',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 13 },
      { date: '2023-09-27', count: 108 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '6501ebbf41e1030057016bad',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 24 },
      { date: '2023-09-27', count: 86 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '64f8e1e88ac29600579fb7ff',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 80 },
      { date: '2023-09-27', count: 109 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '6500b8bcaa74e50057386049',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 13 },
      { date: '2023-09-27', count: 2 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '6509dfb86cd19b0057bd8864',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 20 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '6509e0106cd19b0057bd894a',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 66 },
      { date: '2023-09-27', count: 1 },
      { date: '2023-09-26', count: 20 },
    ],
  },
  {
    _id: '650088f0e18c4f0056341b28',
    emailsSent: [
      { date: '2023-09-29', count: 1 },
      { date: '2023-09-28', count: 120 },
      { date: '2023-09-27', count: 47 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '65008ed5e18c4f0056341e57',
    emailsSent: [
      { date: '2023-09-29', count: 1 },
      { date: '2023-09-28', count: 32 },
      { date: '2023-09-27', count: 72 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '64f9bc25893b9d005619c90e',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 85 },
      { date: '2023-09-27', count: 62 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '64be6c670e12210050407863',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 38 },
      { date: '2023-09-27', count: 96 },
      { date: '2023-09-26', count: 26 },
    ],
  },
  {
    _id: '650b4b4548a76e0056cbcae5',
    emailsSent: [
      { date: '2023-09-28', count: 52 },
      { date: '2023-09-27', count: 61 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '650d89e91fe630005605af0e',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 19 },
      { date: '2023-09-27', count: 38 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '64de4505f3518c0051072c82',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 34 },
      { date: '2023-09-27', count: 35 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '6501ca6a41e1030057015406',
    emailsSent: [
      { date: '2023-09-27', count: 29 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '650afa41d6bd720056000d32',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 20 },
      { date: '2023-09-26', count: 10 },
    ],
  },
  {
    _id: '64f9e5aed6029a0058865989',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 43 },
      { date: '2023-09-27', count: 57 },
      { date: '2023-09-26', count: 21 },
    ],
  },
  {
    _id: '650b4b8c48a76e0056cbcb6e',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 69 },
      { date: '2023-09-26', count: 10 },
    ],
  },
  {
    _id: '650de28ae03ad30056f28733',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 22 },
      { date: '2023-09-27', count: 9 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '6514792772adb4005c2e3beb',
    emailsSent: [{ date: '2023-09-28', count: 1 }],
  },
  {
    _id: '65008b21e18c4f0056341d10',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 26 },
      { date: '2023-09-27', count: 2 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '64f7285aace6fe0056dbcb7a',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 68 },
      { date: '2023-09-26', count: 4 },
    ],
  },
  {
    _id: '64dfbf0f4fd4a200521307e5',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 79 },
      { date: '2023-09-27', count: 92 },
      { date: '2023-09-26', count: 26 },
    ],
  },
  {
    _id: '64f9ff1cd6029a0058867c51',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 36 },
      { date: '2023-09-27', count: 40 },
      { date: '2023-09-26', count: 27 },
    ],
  },
  {
    _id: '64dd272c4dc90a0051abc9de',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 62 },
      { date: '2023-09-27', count: 51 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '650087fae18c4f0056341ad1',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 14 },
      { date: '2023-09-27', count: 2 },
      { date: '2023-09-26', count: 26 },
    ],
  },
  {
    _id: '651478f272adb4005c2e3a3b',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 1 },
    ],
  },
  {
    _id: '6501daba41e1030057015b1a',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 11 },
      { date: '2023-09-27', count: 2 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '650330fc9a9f510057073549',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 21 },
      { date: '2023-09-27', count: 12 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '6500883fe18c4f0056341af8',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 67 },
    ],
  },
  {
    _id: '650304709a9f510057071112',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 38 },
      { date: '2023-09-27', count: 93 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '64e4d407f05550005109c779',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 61 },
      { date: '2023-09-27', count: 47 },
      { date: '2023-09-26', count: 28 },
    ],
  },
  {
    _id: '64f860a4be1cbc00572b66b8',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 11 },
    ],
  },
  {
    _id: '64f8bbfc8ac29600579fa4b5',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 81 },
      { date: '2023-09-27', count: 36 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '64f9c43d8bec6900562791a7',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 102 },
      { date: '2023-09-27', count: 100 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '64f9ec18d6029a005886642b',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 16 },
      { date: '2023-09-26', count: 12 },
    ],
  },
  {
    _id: '650305429a9f5100570711bf',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 17 },
      { date: '2023-09-27', count: 24 },
      { date: '2023-09-26', count: 21 },
    ],
  },
  {
    _id: '64b6e27776a2bb0050078202',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 102 },
      { date: '2023-09-27', count: 99 },
      { date: '2023-09-26', count: 28 },
    ],
  },
  {
    _id: '64e8c531e91350005862c83d',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 33 },
      { date: '2023-09-27', count: 58 },
      { date: '2023-09-26', count: 26 },
    ],
  },
  {
    _id: '650328cc9a9f510057072f41',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 89 },
      { date: '2023-09-27', count: 68 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '650afa9cd6bd720056000dc6',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 51 },
      { date: '2023-09-27', count: 39 },
      { date: '2023-09-26', count: 21 },
    ],
  },
  {
    _id: '650d92281fe630005605b8ed',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 11 },
      { date: '2023-09-27', count: 57 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '64f8e2a68ac29600579fb8b4',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 52 },
      { date: '2023-09-27', count: 113 },
      { date: '2023-09-26', count: 28 },
    ],
  },
  {
    _id: '650328b89a9f510057072f1a',
    emailsSent: [
      { date: '2023-09-29', count: 1 },
      { date: '2023-09-28', count: 2 },
      { date: '2023-09-27', count: 28 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '650331629a9f51005707359a',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 11 },
      { date: '2023-09-27', count: 42 },
      { date: '2023-09-26', count: 26 },
    ],
  },
  {
    _id: '64f72970ace6fe0056dbcce5',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 56 },
      { date: '2023-09-27', count: 51 },
      { date: '2023-09-26', count: 28 },
    ],
  },
  {
    _id: '650de263e03ad30056f286c0',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 62 },
      { date: '2023-09-27', count: 70 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '64f9ead8d6029a0058866223',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 17 },
      { date: '2023-09-26', count: 7 },
    ],
  },
  {
    _id: '650a0eb0e8ffb40057091ac5',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 69 },
      { date: '2023-09-27', count: 1 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '650de276e03ad30056f286fe',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 21 },
      { date: '2023-09-27', count: 32 },
      { date: '2023-09-26', count: 20 },
    ],
  },
  {
    _id: '64bd914ec29f0400501e6525',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 34 },
    ],
  },
  {
    _id: '64f870a96b098b0056e47948',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 31 },
      { date: '2023-09-27', count: 88 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '650b38340edad800568b9a5c',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 117 },
      { date: '2023-09-27', count: 63 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '64bd8e5dc29f0400501e630f',
    emailsSent: [
      { date: '2023-09-29', count: 5 },
      { date: '2023-09-28', count: 123 },
      { date: '2023-09-27', count: 85 },
      { date: '2023-09-26', count: 28 },
    ],
  },
  {
    _id: '650af968d6bd720056000c63',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 11 },
      { date: '2023-09-27', count: 36 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '65117633bbd6610057bafdff',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 54 },
      { date: '2023-09-27', count: 24 },
      { date: '2023-09-26', count: 20 },
    ],
  },
  {
    _id: '64f86090be1cbc00572b66a4',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 53 },
      { date: '2023-09-27', count: 54 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '6503312b9a9f510057073573',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 81 },
      { date: '2023-09-27', count: 34 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '64bd9c9dc29f0400501e6a38',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 129 },
      { date: '2023-09-27', count: 68 },
      { date: '2023-09-26', count: 26 },
    ],
  },
  {
    _id: '6504a6becdcc190057bc3727',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 51 },
      { date: '2023-09-27', count: 70 },
      { date: '2023-09-26', count: 20 },
    ],
  },
  {
    _id: '650a0e80e8ffb40057091a31',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 50 },
      { date: '2023-09-27', count: 94 },
      { date: '2023-09-26', count: 19 },
    ],
  },
  {
    _id: '64de443df3518c0051072c17',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 29 },
      { date: '2023-09-27', count: 114 },
      { date: '2023-09-26', count: 28 },
    ],
  },
  {
    _id: '64dfbb294fd4a200521304d2',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 53 },
      { date: '2023-09-27', count: 73 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '650de237e03ad30056f28634',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 5 },
      { date: '2023-09-27', count: 26 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '64e8c47ce91350005862c64f',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 71 },
      { date: '2023-09-27', count: 60 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '64e8c3cae91350005862c5f8',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 96 },
      { date: '2023-09-27', count: 28 },
      { date: '2023-09-26', count: 31 },
    ],
  },
  {
    _id: '64dd21534dc90a0051abc222',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 72 },
      { date: '2023-09-27', count: 100 },
      { date: '2023-09-26', count: 28 },
    ],
  },
  {
    _id: '64ee405b918d8d0055a324b6',
    emailsSent: [
      { date: '2023-09-28', count: 55 },
      { date: '2023-09-27', count: 56 },
      { date: '2023-09-26', count: 28 },
    ],
  },
  {
    _id: '64fb586b07f9ee0056f68e7b',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 15 },
      { date: '2023-09-27', count: 33 },
      { date: '2023-09-26', count: 26 },
    ],
  },
  {
    _id: '650d89a41fe630005605adbf',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 37 },
      { date: '2023-09-27', count: 1 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '6514731b72adb4005c2e1cb5',
    emailsSent: [
      { date: '2023-09-29', count: 1 },
      { date: '2023-09-28', count: 3 },
    ],
  },
  {
    _id: '64e63ea819ef4400528b652b',
    emailsSent: [
      { date: '2023-09-29', count: 1 },
      { date: '2023-09-28', count: 74 },
      { date: '2023-09-27', count: 116 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '64e8c885e91350005862ca28',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 115 },
      { date: '2023-09-27', count: 64 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '64bd925ac29f0400501e65c7',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 48 },
      { date: '2023-09-27', count: 35 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '64f86e056b098b0056e47636',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 66 },
      { date: '2023-09-27', count: 63 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '6501eb8c41e1030057016b96',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 19 },
      { date: '2023-09-27', count: 56 },
      { date: '2023-09-26', count: 20 },
    ],
  },
  {
    _id: '650b4af348a76e0056cbca56',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 38 },
      { date: '2023-09-27', count: 1 },
      { date: '2023-09-26', count: 20 },
    ],
  },
  {
    _id: '6503051e9a9f510057071194',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 87 },
      { date: '2023-09-27', count: 75 },
      { date: '2023-09-26', count: 26 },
    ],
  },
  {
    _id: '64dd285e4dc90a0051abccc7',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 98 },
      { date: '2023-09-27', count: 77 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '64f865536b098b0056e46ee4',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 101 },
      { date: '2023-09-27', count: 66 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '64f72930ace6fe0056dbcca3',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 21 },
    ],
  },
  {
    _id: '64fa066ad6029a0058868116',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 74 },
      { date: '2023-09-27', count: 31 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '64d6ac38bf48340050a10442',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 29 },
      { date: '2023-09-27', count: 2 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '64f86cb86b098b0056e4742d',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 63 },
      { date: '2023-09-27', count: 76 },
      { date: '2023-09-26', count: 27 },
    ],
  },
  {
    _id: '64f86150be1cbc00572b672b',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 33 },
      { date: '2023-09-27', count: 71 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '64bd9030c29f0400501e6429',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 54 },
      { date: '2023-09-27', count: 120 },
      { date: '2023-09-26', count: 27 },
    ],
  },
  {
    _id: '64bd9662c29f0400501e67a9',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 62 },
      { date: '2023-09-27', count: 97 },
      { date: '2023-09-26', count: 28 },
    ],
  },
  {
    _id: '64f86de96b098b0056e47623',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 33 },
      { date: '2023-09-27', count: 110 },
      { date: '2023-09-26', count: 20 },
    ],
  },
  {
    _id: '64fb57df07f9ee0056f68e15',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 93 },
      { date: '2023-09-27', count: 36 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '6501dc1b41e1030057015e8a',
    emailsSent: [
      { date: '2023-09-29', count: 1 },
      { date: '2023-09-28', count: 55 },
      { date: '2023-09-27', count: 28 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '64f9bdf6e4089a0056f962af',
    emailsSent: [
      { date: '2023-09-27', count: 57 },
      { date: '2023-09-26', count: 26 },
    ],
  },
  {
    _id: '64f9b0984fb0ab00573ca88a',
    emailsSent: [{ date: '2023-09-26', count: 5 }],
  },
  {
    _id: '64bd91e0c29f0400501e657b',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 85 },
      { date: '2023-09-27', count: 113 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '65008a9ee18c4f0056341c9c',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 25 },
      { date: '2023-09-27', count: 50 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '6501ea4241e1030057016870',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 14 },
      { date: '2023-09-27', count: 78 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '64bd87f1c29f0400501e6022',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 114 },
      { date: '2023-09-27', count: 76 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '65008752e18c4f0056341a56',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 42 },
      { date: '2023-09-27', count: 2 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '65032ba39a9f51005707305d',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 89 },
      { date: '2023-09-27', count: 63 },
      { date: '2023-09-26', count: 19 },
    ],
  },
  {
    _id: '64f5668bb864862fb0e562c4',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 91 },
      { date: '2023-09-27', count: 98 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '650c509948a76e0056cc2c31',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 42 },
      { date: '2023-09-27', count: 49 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '650de227e03ad30056f28618',
    emailsSent: [{ date: '2023-09-26', count: 21 }],
  },
  {
    _id: '65114a62242ede0056ad3b89',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 43 },
      { date: '2023-09-27', count: 90 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '64bd8a68c29f0400501e61b6',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 102 },
      { date: '2023-09-27', count: 88 },
      { date: '2023-09-26', count: 29 },
    ],
  },
  {
    _id: '64bd9075c29f0400501e646f',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 39 },
      { date: '2023-09-27', count: 78 },
      { date: '2023-09-26', count: 28 },
    ],
  },
  {
    _id: '64d6a878bf48340050a10186',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 14 },
    ],
  },
  {
    _id: '64f86e2c6b098b0056e4764c',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 57 },
      { date: '2023-09-27', count: 98 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '64d61e83bf48340050a0d122',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 43 },
      { date: '2023-09-27', count: 55 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '64db82e8dba9e500510d63df',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 43 },
      { date: '2023-09-27', count: 53 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '6509e0796cd19b0057bd8a9c',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 5 },
      { date: '2023-09-27', count: 15 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '64dd34524dc90a0051abd88f',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 87 },
      { date: '2023-09-27', count: 87 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '6501ec92d66ffd005736ebc3',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 18 },
      { date: '2023-09-27', count: 2 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '6501bfa341e1030057014ea4',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 19 },
      { date: '2023-09-26', count: 2 },
    ],
  },
  {
    _id: '65034e3da857170056a31d4f',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 12 },
      { date: '2023-09-27', count: 2 },
      { date: '2023-09-26', count: 25 },
    ],
  },
  {
    _id: '6501da0041e10300570159f9',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 14 },
      { date: '2023-09-27', count: 19 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '650afab9d6bd720056000e09',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 51 },
      { date: '2023-09-27', count: 15 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '64be429ee015980051c56346',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 88 },
      { date: '2023-09-27', count: 124 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '64da3fe04be3fe00513b374e',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 84 },
      { date: '2023-09-27', count: 126 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '650d8a261fe630005605b1d4',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 63 },
      { date: '2023-09-27', count: 70 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '64f7459bff47d90056658b3e',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 132 },
      { date: '2023-09-27', count: 19 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '65116e6abbd6610057baf377',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 108 },
      { date: '2023-09-27', count: 46 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '650afae6d6bd720056000e4b',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 23 },
      { date: '2023-09-27', count: 22 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '64f8e2c38ac29600579fb8d0',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 77 },
      { date: '2023-09-27', count: 89 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '64f9e2fed6029a00588656de',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 88 },
      { date: '2023-09-27', count: 78 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '650b38a70edad800568b9ab4',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 58 },
      { date: '2023-09-27', count: 95 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '650b4bb048a76e0056cbcbb2',
    emailsSent: [
      { date: '2023-09-29', count: 4 },
      { date: '2023-09-28', count: 34 },
      { date: '2023-09-27', count: 57 },
      { date: '2023-09-26', count: 21 },
    ],
  },
  {
    _id: '6515ef4cc7756e005c59d23b',
    emailsSent: [{ date: '2023-09-29', count: 2 }],
  },
  {
    _id: '64be6aaf0e12210050407796',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 59 },
      { date: '2023-09-27', count: 94 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '64db9651dba9e500510d6fcd',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 70 },
      { date: '2023-09-27', count: 93 },
      { date: '2023-09-26', count: 28 },
    ],
  },
  {
    _id: '6504a6dccdcc190057bc3784',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 10 },
      { date: '2023-09-27', count: 20 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '650de218e03ad30056f28600',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 13 },
      { date: '2023-09-27', count: 1 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '6509e0546cd19b0057bd8a0f',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 40 },
      { date: '2023-09-27', count: 21 },
      { date: '2023-09-26', count: 20 },
    ],
  },
  {
    _id: '64f9e92ad6029a0058865f65',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 40 },
      { date: '2023-09-27', count: 102 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '6514791072adb4005c2e3b0a',
    emailsSent: [
      { date: '2023-09-29', count: 1 },
      { date: '2023-09-28', count: 1 },
    ],
  },
  {
    _id: '64fb581b07f9ee0056f68e2f',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 6 },
      { date: '2023-09-27', count: 11 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '650d88321fe630005605accd',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 78 },
      { date: '2023-09-27', count: 91 },
      { date: '2023-09-26', count: 21 },
    ],
  },
  {
    _id: '64e6430e19ef4400528b655d',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 71 },
      { date: '2023-09-27', count: 76 },
      { date: '2023-09-26', count: 28 },
    ],
  },
  {
    _id: '650de29ae03ad30056f28773',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 63 },
      { date: '2023-09-27', count: 78 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '64fb584e07f9ee0056f68e65',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 37 },
      { date: '2023-09-27', count: 74 },
      { date: '2023-09-26', count: 24 },
    ],
  },
  {
    _id: '6501ea7a41e103005701693c',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 61 },
      { date: '2023-09-27', count: 66 },
      { date: '2023-09-26', count: 20 },
    ],
  },
  {
    _id: '650474c059365a0056f54439',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 66 },
      { date: '2023-09-27', count: 9 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '650c511d48a76e0056cc2db1',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 71 },
      { date: '2023-09-27', count: 30 },
      { date: '2023-09-26', count: 22 },
    ],
  },
  {
    _id: '650b38860edad800568b9a98',
    emailsSent: [
      { date: '2023-09-29', count: 3 },
      { date: '2023-09-28', count: 41 },
      { date: '2023-09-27', count: 57 },
      { date: '2023-09-26', count: 23 },
    ],
  },
  {
    _id: '651478d472adb4005c2e3950',
    emailsSent: [
      { date: '2023-09-29', count: 1 },
      { date: '2023-09-28', count: 1 },
    ],
  },
  {
    _id: '64d6823cbf48340050a0f032',
    emailsSent: [
      { date: '2023-09-29', count: 2 },
      { date: '2023-09-28', count: 93 },
      { date: '2023-09-27', count: 45 },
      { date: '2023-09-26', count: 27 },
    ],
  },
];

res1 = res.map((obj) =>
  obj.emailsSent.map((obj2) => ({ [`${obj._id} | ${obj2.date}`]: obj2.count })),
);

res1.forEach(arr => res2.push(...arr))
const mongoose = require('mongoose');
const http = require('http');
const express = require('express');
const { Server } = require('socket.io');
const colors = require('colors');
const cors = require('cors');
require('dotenv').config();

const main = require('./routes/main.js');
const {
  statusHandler,
  freeResources,
  startPolling,
  stopPolling,
} = require('./services/createEmailWorkers');

const followUp = require('./services/createFollowUpWorkers');

const { MongoURI: db } = require('./config/keys');

const initialize = () => {
  freeResources();
  const endPolling = startPolling();
  // const endFollowUpPolling = followUp.startPolling()
};

mongoose
  .connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log('MongoDB Connected'.green.bold);
    initialize();
  })
  .catch((err) => console.log(err));

// Route Files

const app = express();
const server = http.createServer(app);
const io = new Server(server, {
  cors: {
    origin: '*',
    credentials: true,
  },
});

const emailSocket = io.of('/emailStats');

emailSocket.on('connection', (socket) => {
  // const email = socket.handshake.query.email
  // const socketID = socket.id
  statusHandler(socket);
});

app.use(
  cors({
    origin: true,
    credentials: true,
  }),
);

app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: true }));

// test route
app.get('', (req, res) => {
  res.send('Working');
});
app.get('/api/v1/main/stopPolling-60b8a2b7c3b9', (req, res) => {
  const {status, message} = stopPolling();
  if (status) {
    res.status(200).json({ status: 'success', message });
  } else {
    res.status(500).json({ status: 'failed', message });
  }
});

app.get('/api/v1/main/startPolling-60b8a2b7c3b9', (req, res) => {
  const clearPollInt = startPolling();
  if (clearPollInt) {
    res.status(200).json({ status: 'success', message: 'Polling started' });
  } else {
    res.status(500).json({ status: 'failed', message: 'Polling already started' });
  }
});

// Routing for API Service
app.use('/api/v1/main', main);
const PORT = process.env.PORT || 8085;

server.listen(PORT, console.log(`Server running on port ${PORT}`.yellow.bold));

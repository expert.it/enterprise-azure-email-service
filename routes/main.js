const express = require('express');
const router = express.Router();
const communicate = require('../controllers/communicate');

const followUp = require('../services/createFollowUpWorkers');
const { getServiceState } = require('../services/createEmailWorkers');

// communicate
router.get('/getServiceState', (req, res) => {
  const state = getServiceState();
  // console.log(state);
  const followUpState = followUp.getServiceState();
  // console.log(followUpState);
  res.status(200).json({
    emailService: state,
    followUpService: followUpState,
  });
});
router.get('/campaignUpdated', communicate.processOnCampUnpause);
router.post('/sendEmails', communicate.sendEmails);
router.post('/getSampleEmail', communicate.getSampleEmail);

module.exports = router;

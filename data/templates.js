const { default: axios } = require('axios');
const {
  CUSTOM_GPT_URL,
  API_SERVICE_BACKEND,
  EMAIL_TRACKING_SERVICE,
  frontEndURL,
} = require('../config/config');
const { random } = require('../utils/misc');
const { htmlToText } = require('../utils/parser');
const URLModel = require('../models/urlMapping');
const { v4: uuidv4 } = require('uuid');

// returns a random no. between 0-(n-1)
// const random = (n) => Math.floor(Math.random() * n + 1) - 1;

const spamDict = `Access, Access now, Act, Act immediately, Act now, Act now!, Action, Action required, Apply here, Apply now, Apply now!, Apply online, Become a member, Before it's too late, Being a member, Buy, Buy direct, Buy now, Buy today, Call, Call free, Call free/now, Call me, Call now, Call now!, Can we have a minute of your time?, Cancel now, Cancellation required, Claim now, Click, Click below, Click here, Click me to download, Click now, Click this link, Click to get, Click to remove, Contact us immediately, Deal ending soon, Do it now, Do it today, Don't delete, Don't hesitate, Don't waste time, Don’t delete, Exclusive deal, Expire, Expires today, Final call, For instant access, For Only, For you, Friday before [holiday], Get it away, Get it now, Get now, Get paid, Get started, Get started now, Great offer, Hurry up, Immediately, Info you requested, Information you requested, Instant, Limited time, New customers only, Now, Now only, Offer expires, Once in a lifetime, Only, Order now, Order today, Please read, Purchase now, Sign up free, Sign up free today, Supplies are limited, Take action, Take action now, This won’t last, Time limited, Today, Top urgent, Trial, Urgent, What are you waiting for?, While supplies last, You are a winner, 0 down, All, All natural, All natural/new, All new, All-natural, All-new, Allowance, As seen on, As seen on Oprah, At no cost, Auto email removal, Avoice bankruptcy, Avoid, Beneficial offer, Beneficiary, Bill 1618, Brand new pager, Bulk email, Buying judgements, Buying judgments, Cable converter, Calling creditors, Can you help us?, Cancel at any time, Cannot be combined, Celebrity, Cell phone cancer scam, Certified, Chance, Cheap, Cheap meds, Cialis, Claims, Claims not to be selling anything, Claims to be in accordance with some spam law, Claims to be legal, Clearance, Collect, Collect child support, Compare, Compare now, Compare online, Compare rates, Compete for your business, Confidentiality, Congratulations, Consolidate debt and credit, Consolidate your debt, Copy accurately, Copy DVDs, COVID, Cures, Cures baldness, Diagnostic, DIAGNOSTICS, Diet, Dig up dirt on friends, Direct email, Direct marketing, Eliminate debt, Explode your business, Fast viagra delivery, Finance, Financial, Financial advice, Financial independence, Financially independent, For new customers only, Foreclosure, Free, Free access/money/gift, Free bonus, Free cell phone, Free DVD, Free grant money, Free information, Free installation, Free Instant, Free iPhone, Free laptop, Free leads, Free Macbook, Free offer, Free priority mail, Free sample, Free website, Free!, Get, Gift card, Gift certificate, Gift included, Give it away, Giving away, Giving it away, Gold, Great, Great deal, Greetings of the day, Growth hormone, Guarantee, Guaranteed deposit, Guaranteed income, Guaranteed payment, Have you been turned down?, Hello (with no name included), Hidden charges, Hidden costs, Hidden fees, High score, Home based business, Home mortgage, Human, Human growth hormone, If only it were that easy, Important information, Important notification, Instant weight loss, Insurance Lose weight, Internet marketing, Investment decision, Invoice, It’s effective, Job alert, Junk, Lambo, Laser printer, Last Day, Legal, Legal notice, Life, Life insurance, Lifetime access, Lifetime deal, Limited, Limited amount, Limited number, Limited offer, Limited supply, Limited time offer, Limited time only, Loan, Long distance phone number, Long distance phone offer, Lose weight, Lose weight fast, Lose weight spam, Lottery, Lower interest rate, Lower interest rates, Lower monthly payment, Lower your mortgage rate, Lowest insurance rates, Lowest interest rate, Lowest rate, Lowest rates, Luxury, Luxury car, Mail in order form, Main in order form, Mark this as not junk, Mass email, Medical, Medicine, Meet girls, Meet me, Meet singles, Meet women, Member, Member stuff, Message contains disclaimer, Message from, Millionaire, Millions, MLM, Multi-level marketing, Name, Near you, Never before, New, New domain extensions, Nigerian, No age restrictions, No catch, No claim forms, No cost, No credit check, No credit experience, No deposit required, No disappointment, No experience, No fees, No gimmick, No hidden, No hidden costs, No hidden fees, No hidden сosts, No interest, No interests, No inventory, No investment, No investment required, No medical exams, No middleman, No obligation, No payment required, No purchase necessary, No questions asked, No selling, No strings attached, No-obligation, Nominated bank account, Not intended, Not junk, Not scam, Not spam, Notspam, Number 1, Obligation, Off, Off everything, Off shore, Offer extended, Offers, Offshore, One hundred percent, One-time, Online biz opportunity, Online degree, Online income, Online job, Open, Opportunity, Opt-in, Order, Order shipped by, Order status, Orders shipped by, Orders shipped by shopper, Outstanding value, Outstanding values, Password, Passwords, Pay your bills, Per day/per week/per year, Per month, Perfect, Performance, Phone, Please, Please open, Presently, Print form signature, Print from signature, Print out and fax, Priority mail, Privately owned funds, Prizes, Problem with shipping, Problem with your order, Produced and sent out, Profit, Promise you, Purchase, Pure Profits, Quotes, Rate, Real thing, Rebate, Reduce debt, Refinance home, Refinanced home, Refund, Regarding, Removal instructions, Removes, Removes wrinkles, Replica watches, Request, Request now, Request today, Requires initial investment, Requires investment, Reverses aging, Risk free, Rolex, Round the world, S 1618, Safeguard notice, Sale, Sales, Save, Save $, Save €, Save big, Save big month, Save money, Save now, Score with babes, Search engine optimisation, Section 301, See for yourself, Seen on, Serious, Serious case, Serious offer, Serious only, Sex, Shop now, Shopper, Shopping spree, Snoring, Social security number, Soon, Spam, Spam free, Special deal, Special discount, Special for you, Special offer, Stainless steel, Stock alert, Stock disclaimer statement, Stock pick, Stocks/stock pick/stock alert, Stop calling me, Stop emailing me, Stop further distribution, Stop snoring, Strong buy, Stuff on sale, Subject to, Subject to cash, Subscribe, Subscribe for free, Subscribe now, Super promo, Supplies, Tack action now, Talks about hidden charges, Talks about prizes, Tells you it’s an ad, Terms, The best rates, The email asks for a credit card, The following form, They make a claim or claims that they're in accordance with spam law, They try to keep your money no refund, They’re just giving it away, This isn't junk, This isn't spam, This isn’t a scam, This isn’t junk, This isn’t spam, Timeshare, Timeshare offers, Traffic, Trial unlimited, U.S. dollars, Undisclosed, Undisclosed recipient, University diplomas, Unsecured credit, Unsecured debt, Unsolicited, Unsubscribe, Urgent response, US dollars / Euros, Vacation, Vacation offers, Valium, Viagra, Vicodin, VIP, Visit our website, Wants credit card, Warranty expired, We hate spam, We honor all, Website visitors, Weekend getaway, Weight loss, What’s keeping you?, While available, While in stock, While stocks last, While you sleep, Who really wins?, Win, Winner, Winning, Won, Xanax, XXX, You have been chosen, You have been selected, Your chance, Your status, Zero chance, Zero percent, Zero risk, #1, %, % free, % Satisfied, 0%, 0% risk, 100%, 100% free, 100% more, 100% off, 100% satisfied, 99.90%, 99%, Access for free, Additional income, Amazed, Amazing, Amazing offer, Amazing stuff, Be amazed, Be surprised, Be your own boss, Believe me, Best bargain, Best deal, Best offer, Best price, Best rates, Big bucks, Bonus, Boss, Can’t live without, Cancel, Consolidate debt, Double your cash, Double your income, Drastically reduced, Earn extra cash, Earn money, Eliminate bad credit, Expect to earn, Extra, Extra cash, Extra income, Fantastic, Fantastic deal, Fantastic offer, FAST, Fast cash, Financial freedom, Free access, Free consultation, Free gift, Free hosting, Free info, Free investment, Free membership, Free money, Free preview, Free quote, Free trial, Full refund, Get out of debt, Giveaway, Guaranteed, Increase sales, Increase traffic, Incredible deal, Join billions, Join millions, Join millions of Americans, Join thousands, Lower rates, Lowest price, Make money, Million, Million dollars, Miracle, Money back, Month trial offer, More Internet Traffic, Number one, Once in a lifetime, One hundred percent guaranteed, One time, Pennies a day, Potential earnings, Prize, Promise, Pure profit, Risk-free, Satisfaction guaranteed, Save big money, Save up to, Special promotion, The best, Thousands, Unbeatable offer, Unbelievable, Unlimited, Unlimited trial, Wonderful, You will not believe your eyes, $$$, €€€, £££, 50% off, A few bob, Accept cash cards, Accept credit cards, Affordable, Affordable deal, Avoid bankruptcy, Bad credit, Bank, Bankruptcy, Bargain, Billing, Billing address, Billion, Billion dollars, Billionaire, Card accepted, Cards accepted, Cash, Cash bonus, Cash out, Cash-out, Cashcashcash, Casino, Cents on the dollar, Check, Check or money order, Claim your discount, Cost, Costs, Credit, Credit bureaus, Credit card, Credit card offers, Credit or Debit, Deal, Debt, Discount, Dollars, Double your, Double your wealth, Earn, Earn $, Earn cash, Earn extra income, Earn from home, Earn monthly, Earn per month, Earn per week, Earn your degree, Easy income, Easy terms, F r e e, For free, For just $, For just $ (amount), For just $xxx, Get Money, Get your money, Hidden assets, Huge discount, Income, Income from home, Increase revenue, Increase sales/traffic, Increase your chances, Initial investment, Instant earnings, Instant income, Insurance, Investment, Investment advice, Lifetime, Loans, Make $, Money, Money making, Money-back guarantee, Money-making, Monthly payment, Mortgage, Mortgage rates, Offer, One hundred percent free, Only $, Price, Price protection, Prices, Profits, Quote, Rates, Refinance, Save $, Serious cash, Subject to credit, US dollars, Why pay more?, Your income, Acceptance, Accordingly, Account-based marketing (ABM), Accounts, Addresses, Addresses on CD, Beverage, Confidentiality on all orders, Confidentiality on all orders, Content marketing, Dear [email address], Dear [email/friend/somebody], Dear [first name], Dear [wrong name], Digital marketing, Dormant, Email extractor, Email harvest, Email marketing, Extract email, Form, Freedom, Friend, Here, Hidden, Home, Home based, Home employment, Home-based, Home-based business, Homebased business, If you no longer wish to receive, Important information regarding, In accordance with laws, Increase your sales, Internet market, Leave, Lose, Maintained, Marketing, Marketing solution, Marketing solutions, Medium, Message contains, Multi level marketing, Never, One time mailing, Online marketing, Online pharmacy, Opt in, Per day, Per week, Pre-approved, Problem, Removal, Remove, Reserves the right, Reverses, Sample, Satisfaction, Score, Search engine, Search engine listings, Search engines, Sent in compliance, Solution, Stop, Success, Teen, Terms and conditions, Warranty, Web traffic, Wife, Work at home, Work from home`;

const introPitches = [
  // `I hope this email finds you well. I noticed that your company has been investing a lot of effort into email sending, but it seems like the conversion stats might not be as high as you'd like them to be. That's a common challenge faced by many businesses in your industry.`,
  // `I noticed your company's impressive portfolio and would like to discuss how we can help you increase your business growth. Our pricing is half of what competitors charge, with no long-term commitment required. We can start delivering results for you quickly, as our onboarding process is short.`,
  // `I hope you're doing well. We don't require any commitments and charge half the price of our competitors without compromising on quality. Our onboarding period is short to deliver results sooner. I would be thrilled to discuss how unique approach can benefit in business.`,
  `I hope this email finds you well. I noticed that your company has invested a lot of effort into email marketing. Let us take care of it for you.`,
  `The services you offer impressed me, and I would like to discuss how we can help. No commitments, half the price, same great quality and quick onboarding for faster results.`,
  `I came across your company and noticed an opportunity to help you acquire more clients. We can start delivering results for you quickly with our short onboarding process.`,
  `Hope you're well! No commitments, half the price, same great quality. Quick onboarding for faster results. Let's chat about how our unique approach can benefit your business.`,
];

const emailSignature = ({ campaign, userOrgName }) => {
  const spaceBetweenEmailContentAndSign = `<p></p><p></p><p></p><p></p>`;

  const signOptions = [
    `<p>__________<br/>Best regards,<br/>${campaign.senderName}<br/>${userOrgName}</p>`,
  ];

  return (
    spaceBetweenEmailContentAndSign + signOptions[random(signOptions.length)]
  );
};

const options = ({ userOrgName, leadId, link: salesGPTURL }) => {
  return {
    address: '30 N Gould St, Sheridan, WY 82801',
    locationURL:
      'https://www.google.com/maps/place/30%20N%20Gould%20St,%20Sheridan,%20WY%2082801',
    privacyPolicyURL: 'https://www.b2brocket.ai/legal/privacy-policy',
    // unsubscribeURL : `${frontEndURL}/unsubscribe/${leadId}`,
    unsubscribeURL: `${frontEndURL}/unenroll/${leadId}`,
    optOutURL: `${frontEndURL}/optout/${leadId}`,
  };
};
const disclaimerFormatted = ({ userOrgName, leadId, link: salesGPTURL }) => {
  const url = frontEndURL;
  const address = '30 N Gould St, Sheridan, WY 82801';
  const locationURL =
    'https://www.google.com/maps/place/30%20N%20Gould%20St,%20Sheridan,%20WY%2082801';
  const privacyPolicyURL = 'https://www.b2brocket.ai/legal/privacy-policy';
  // const unsubscribeURL = `${frontEndURL}/unsubscribe/${leadId}`;
  const unsubscribeURL = `${frontEndURL}/unenroll/${leadId}`;
  const optOutURL = `${frontEndURL}/optout/${leadId}`;

  const spaceBetweenSignAndDisclaimer = '<br/><br/><br/>';

  const disclaimerOptions = [
    // `<p><small style="color:gray"><a href=${url} title=${url}>B2B Rocket</a> AI agents have written & sent this email on behalf of ${userOrgName}. If you do not wish to receive these emails, you can <a href=${unsubscribeURL} title=${unsubscribeURL}>unenroll</a> or respond to this email to not hear from them again. We solely process publicly available information. We DO NOT track or store any of your personal information. You may visit our <a href=${privacyPolicyURL} title=${privacyPolicyURL}>Privacy Policy</a> if you want to understand more about how we process and use data. Our mission is to help businesses grow using the power of AI. We strictly comply with CCPA, GDPR, and applicable laws. You can <a href=${optOutURL} title=${optOutURL}>opt-out</a> of our data processing anytime. We are B2B Rocket, located at <a href=${locationURL} title=${locationURL}>${address}</a>.</small></p>`;

    // `<p><span style="color:gray"><a href=${url} target="_blank">B2B Rocket</a> AI agents have written & sent this email on behalf of ${userOrgName}. If you do not wish to receive these emails, you can <a href=${unsubscribeURL} target="_blank">unenroll</a> or respond to this email to not hear from them again. We solely process publicly available information. We DO NOT track or store any of your personal information. You may visit our <a href=${privacyPolicyURL} target="_blank">Privacy Policy</a> if you want to understand more about how we process and use data. Our mission is to help businesses grow using the power of AI. We strictly comply with CCPA, GDPR, and applicable laws. You can <a href=${optOutURL} target="_blank">opt-out</a> of our data processing anytime. We are B2B Rocket, located at <a href=${locationURL} target="_blank">${address}</a>.</span></p>`;

    // `<p><span style="color:gray"><a href=${url} target="_blank">B2B Rocket</a> AI agents have written & sent this email on behalf of ${userOrgName}. If you do not wish to receive these emails, you can <a href=${unsubscribeURL} target="_blank">unenroll</a> or respond to this email to not hear from them again. We solely process publicly available information. We DO NOT track or store any of your personal information. You may visit our <a href=${privacyPolicyURL} target="_blank">Privacy Policy</a> if you want to understand more about how we process and use data. Our mission is to help businesses grow using the power of AI. We strictly comply with CCPA, GDPR, and applicable laws. You can <a href=${optOutURL} target="_blank">opt-out</a> of our data processing anytime. We are B2B Rocket, located at <a href=${locationURL} target="_blank">${address}</a>.</span></p>`;

    // `<p><span style="color:gray">This email is written and sent on behalf of ${userOrgName} by <a href=${url} target="_blank">B2B Rocket AI</a> at <a href=${locationURL} target="_blank">${address}</a>. Our mission is to help businesses grow using the power of AI. We only use publicly accessible data and store no personal information. For more details on how we handle data, please see our <a href=${privacyPolicyURL} target="_blank">Privacy Policy</a>. We are committed to businesses and adhere to CCPA, GDPR, and all other applicable data protection laws. <a href=${optOutURL} target="_blank">Unenroll</a> from ${userOrgName} communications & data processing anytime.</span></p>`;

    // `<p><span style="color:gray">This email is written and sent on behalf of ${userOrgName} by <a href=${url} target="_blank">B2B Rocket AI</a> at <a href=${locationURL} target="_blank">${address}</a>. Our mission is to help businesses grow using the power of AI. We only use publicly accessible data and store no personal information. For more details on how we handle data, please see our <a href=${privacyPolicyURL} target="_blank">Privacy Policy</a>. We are committed to businesses and adhere to CCPA, GDPR, and all other applicable data protection laws. <a href=${unsubscribeURL} target="_blank">Unenroll</a> from ${userOrgName} communications & data processing anytime.</span></p>`,

    // // used previosly
    // `<p><span style="color:gray"><a href=${url} target="_blank"><b>B2B Rocket</b></a> AI agents have generated this email on behalf of ${userOrgName}. We are B2B Rocket, located at <a href=${locationURL} target="_blank">${address}</a>. Our focus is to help businesses grow using the power of AI. We use publicly accessible data and store no personal information. For more details on how we handle data, you may view our <a href=${privacyPolicyURL} target="_blank">Privacy Policy</a>. We are committed to businesses and adhere to the CCPA, GDPR, and other applicable data protection laws. <a href=${unsubscribeURL} target="_blank">Unenroll</a> from ${userOrgName} communications & data processing anytime.</span></p>`,

    // currently in use
    `<p><span style="color:gray"><a href=${unsubscribeURL} target="_blank"><b>Opt-out here</b></a>. This email was sent by <b>${userOrgName}</b>. Our sending servers are located at <a href=${locationURL} target="_blank">${address}</a>.</span></p>`

    // `<p><span style="color:gray"><a href=${url} target="_blank"><b>B2B Rocket AI</b></a> agents have crafted and dispatched this email on behalf of ${userOrgName}. We are B2B Rocket, located at <a href=${locationURL} target="_blank">${address}</a>. If you prefer not to receive these emails, you can <a href=${unsubscribeURL} target="_blank">unenroll</a> to cease further communications. We use publicly accessible data and store no personal information. For more details on how we handle data, you may see our <a href=${privacyPolicyURL} target="_blank">Privacy Policy</a>. Our core focus is to assist businesses in streamlining organic meeting outreach and boosting revenue through the application of AI technology. We are committed to assisting businesses and adhere to the CCPA, GDPR, and other applicable data protection laws. You retain the ability to <a href=${optOutURL} target="_blank">opt-out</a> of our data processing at any time.</span></p>`,
  ];

  return (
    spaceBetweenSignAndDisclaimer +
    disclaimerOptions[random(disclaimerOptions.length)]
  );
};

const subjectTemplates = [
  `Invitation to Chat: [USER_COMPANY]`,
  `Chat Invitation: [USER_COMPANY]`,
  `Invitation from [USER_COMPANY]`,
  `Let's Chat: [USER_COMPANY]`,
];

const templateVariables = [
  {
    var: '[PROSPECT_FIRST_NAME]',
    pattern: '\\[PROSPECT_FIRST_NAME\\]',
    section: 'pFName',
  },
  {
    var: '[PROSPECT_TITLE]',
    pattern: '\\[PROSPECT_TITLE\\]',
    section: 'pTitle',
  },
  {
    var: '[PROSPECT_INDUSTRY]',
    pattern: '\\[PROSPECT_INDUSTRY\\]',
    section: 'pIndustry',
  },
  {
    var: '[PROSPECT_LOCATION]',
    pattern: '\\[PROSPECT_LOCATION\\]',
    section: 'pLocation',
  },
  {
    var: '[PROSPECT_COMPANY]',
    pattern: '\\[PROSPECT_COMPANY\\]',
    section: 'pCompany',
  },
  {
    var: '[PROSPECT_COMPANY_MISSION]',
    pattern: '\\[PROSPECT_COMPANY_MISSION\\]',
    section: 'pMission',
  },
  {
    var: '[PROSPECT_COMPANY_SPECIFIC_ACHIEVEMENT]',
    pattern: '\\[PROSPECT_COMPANY_SPECIFIC_ACHIEVEMENT\\]',
    section: 'pAchievements',
  },
  {
    var: '[PROSPECT_COMPANY_EXPERTISE]',
    pattern: '\\[PROSPECT_COMPANY_EXPERTISE\\]',
    section: 'pExpertise',
  },
  {
    var: '[PROSPECT_AREA_OF_IMPROVEMENT]',
    pattern: '\\[PROSPECT_AREA_OF_IMPROVEMENT\\]',
    section: 'pImprovement',
  },
  {
    var: '[CHALLENGE_RELATED_TO_SERVICE/PRODUCT]',
    pattern: '\\[CHALLENGE_RELATED_TO_SERVICE/PRODUCT\\]',
    section: 'challenges',
  },
  {
    var: '[USER_COMPANY_DESCRIPTION]',
    pattern: '\\[USER_COMPANY_DESCRIPTION\\]',
    section: 'description',
  },
  {
    var: '[USER_SERVICE/PRODUCT_DESCRIPTION]',
    pattern: '\\[(USER_SERVICE/PRODUCT_DESCRIPTION|STRONG_SELLING_POINTS)\\]',
    section: 'productDesc',
  },
  {
    var: '[USER_EXPERTISE]',
    pattern: '\\[USER_EXPERTISE\\]',
    section: 'expertise',
  },
  {
    var: '[USER_COMPANY_CASE_STUDIES]',
    pattern: '\\[USER_COMPANY_CASE_STUDIES\\]',
    section: 'caseStudy',
  },
  {
    var: '[USER_COMPANY]',
    pattern: '\\[USER_COMPANY\\]',
    section: 'userOrgName',
  },
  {
    var: '[USER_VALUE_PROPOSITION]',
    pattern: '\\[USER_VALUE_PROPOSITION\\]',
    section: 'valueProposition',
  },
  {
    var: '[TIE_IN_TO_WHY_WE_ARE_REACHING_OUT]',
    pattern: '\\[TIE_IN_TO_WHY_WE_ARE_REACHING_OUT\\]',
    section: 'connection',
  },
  { var: '[USER_SIGNATURE]', pattern: '\\[USER_SIGNATURE\\]', section: 'sign' },
  { var: '[DISCLAIMER]', pattern: '\\[DISCLAIMER\\]', section: 'disclaimer' },
];
const emailTemplates = [
  // `Hi [PROSPECT_FIRST_NAME],<br/><br/>As the [PROSPECT_TITLE] of #salesGPT#[PROSPECT_COMPANY]#!salesGPT#, you're likely aware that [CHALLENGE_RELATED_TO_SERVICE/PRODUCT] can be difficult.<br/><br/>That's where our team at #salesGPT#[USER_COMPANY]#!salesGPT# comes in. [USER_COMPANY_DESCRIPTION].<br/><br/>[USER_SERVICE/PRODUCT_DESCRIPTION]. Have you considered what [USER_COMPANY] can do for your [PROSPECT_COMPANY]?<br/><br/>We'd like to invite you to #salesGPT#<b>learn more in an exclusive session</b>#!salesGPT#, where we'll discuss [USER_VALUE_PROPOSITION].<br/><br/>Enjoy the rest of your week!<br/><br/>__________<br/>[USER_SIGNATURE]<br/><br/><br/>[DISCLAIMER]`,

  // `Hi [PROSPECT_FIRST_NAME],<br/><br/>I've been thoroughly impressed with #salesGPT#[PROSPECT_COMPANY]'s#!salesGPT# dedication to [PROSPECT_COMPANY_MISSION]. Your organization's commitment to that mission is truly commendable.<br/><br/>At #salesGPT#[USER_COMPANY]#!salesGPT#, we specialize in [USER_COMPANY_DESCRIPTION]. We believe our services could be a fit for [PROSPECT_COMPANY] to further enhance your existing strategies.<br/><br/>Would you be interested in joining an #salesGPT#<b>exclusive chat</b>#!salesGPT# to discuss how [USER_COMPANY] can support your goals?<br/><br/>Looking forward to collaborating.<br/><br/>__________<br/>[USER_SIGNATURE]<br/><br/><br/>[DISCLAIMER]`,

  // `Hi [PROSPECT_FIRST_NAME],<br/><br/>We've taken note of #salesGPT#[PROSPECT_COMPANY]'s#!salesGPT# commitment to [PROSPECT_COMPANY_MISSION]. The dedication you've demonstrated is truly remarkable.<br/><br/>#salesGPT#[USER_COMPANY]#!salesGPT# specializes in [USER_SERVICE/PRODUCT_DESCRIPTION], and we are confident that this could significantly benefit [PROSPECT_COMPANY] by enhancing your [PROSPECT_AREA_OF_IMPROVEMENT].<br/><br/>#salesGPT#<b>I'd like to invite you for a discussion</b>#!salesGPT# on how [USER_COMPANY] can contribute to your goals. Are you available for a chat?<br/><br/>__________<br/>[USER_SIGNATURE]<br/><br/><br/>[DISCLAIMER]`,

  // `Hi [PROSPECT_FIRST_NAME],<br/><br/>I recently came across #salesGPT#[PROSPECT_COMPANY]#!salesGPT# and your commitment to [PROSPECT_COMPANY_MISSION] caught my attention. It is indeed inspiring.<br/><br/>We, at #salesGPT#[USER_COMPANY]#!salesGPT#, specialize in [USER_SERVICE/PRODUCT_DESCRIPTION]. I firmly believe our offerings could provide significant value to [PROSPECT_COMPANY] by boosting your [PROSPECT_AREA_OF_IMPROVEMENT].<br/><br/>#salesGPT#<b>Would you join a conversation</b>#!salesGPT# to discuss how [USER_COMPANY] can help achieve your objectives?<br/><br/>__________<br/>[USER_SIGNATURE]<br/><br/><br/>[DISCLAIMER]`,

  // `Hi [PROSPECT_FIRST_NAME],<br/><br/>I'm genuinely impressed by #salesGPT#[PROSPECT_COMPANY]'s#!salesGPT# dedication to [PROSPECT_COMPANY_MISSION]. Your commitment to excellence is clearly evident.<br/><br/>At #salesGPT#[USER_COMPANY]#!salesGPT#, our focus is on [USER_SERVICE/PRODUCT_DESCRIPTION], and we believe our expertise could contribute significantly to [PROSPECT_COMPANY], particularly in enhancing your [PROSPECT_AREA_OF_IMPROVEMENT].<br/><br/>I'd welcome a chat to discuss how [USER_COMPANY] could support your goals. #salesGPT#<b>Are you available to join an exclusive chat?</b>#!salesGPT#<br/><br/>__________<br/>[USER_SIGNATURE]<br/><br/><br/>[DISCLAIMER]`,

  // `Hi [PROSPECT_FIRST_NAME],<br/><br/>Your company, #salesGPT#[PROSPECT_COMPANY]#!salesGPT#, has caught our attention with its commitment to [PROSPECT_COMPANY_MISSION]. The level of dedication is indeed commendable.<br/><br/>Our team at #salesGPT#[USER_COMPANY]#!salesGPT# specializes in [USER_SERVICE/PRODUCT_DESCRIPTION] and we are confident that our services could be an asset for [PROSPECT_COMPANY] in boosting your [PROSPECT_AREA_OF_IMPROVEMENT].<br/><br/>I'd love to #salesGPT#<b>schedule a chat</b>#!salesGPT# with you to explore how [USER_COMPANY] can align with your objectives. Does that sound good to you?<br/><br/>__________<br/>[USER_SIGNATURE]<br/><br/><br/>[DISCLAIMER]`,

  // `Hi [PROSPECT_FIRST_NAME],<br/><br/>It's hard not to notice #salesGPT#<b>[PROSPECT_COMPANY]'s</b>#!salesGPT# impressive dedication to [PROSPECT_COMPANY_MISSION]. It's truly inspiring.<br/><br/>At #salesGPT#<b>[USER_COMPANY]</b>#!salesGPT#, specialize in [USER_SERVICE/PRODUCT_DESCRIPTION], and we believe that our offerings could significantly contribute to [PROSPECT_COMPANY] by enhancing your [PROSPECT_AREA_OF_IMPROVEMENT].<br/><br/>Would you be #salesGPT#<b>available for a discussion</b>#!salesGPT# to explore how [USER_COMPANY] can support your objectives?<br/><br/>__________<br/>[USER_SIGNATURE]<br/><br/><br/>[DISCLAIMER]`,
  // previously used
  // `<p>Hi [PROSPECT_FIRST_NAME],</p><p></p><p>It's hard not to notice #salesGPT#<b>[PROSPECT_COMPANY]'s</b>#!salesGPT# impressive dedication to [PROSPECT_COMPANY_MISSION]. It's truly inspiring.</p><p>At #salesGPT#<b>[USER_COMPANY]</b>#!salesGPT#, specialize in [USER_SERVICE/PRODUCT_DESCRIPTION], and we believe that our offerings could significantly contribute to [PROSPECT_COMPANY] by enhancing your [PROSPECT_AREA_OF_IMPROVEMENT].</p><p>Would you be #salesGPT#<b>available for a discussion</b>#!salesGPT# to explore how [USER_COMPANY] can support your objectives?</p><p></p><p></p><p></p><p></p>[USER_SIGNATURE][DISCLAIMER]`,

  // with optional text
  `<p>Hi [PROSPECT_FIRST_NAME],</p><p></p><p>I've been closely following #salesGPT#<b>[PROSPECT_COMPANY]'s</b>#!salesGPT# impressive journey in the [PROSPECT_INDUSTRY]. Your commitment to [PROSPECT_COMPANY_MISSION]${
    /* and your role as a [PROSPECT_TITLE] based in [PROSPECT_LOCATION] certainly stands out */ ''
  } is impressive.</p><p>[TIE_IN_TO_WHY_WE_ARE_REACHING_OUT]</p><p>At #salesGPT#<b>[USER_COMPANY]</b>#!salesGPT#, we specialize in [STRONG_SELLING_POINTS]. Notably, our work with [USER_COMPANY_CASE_STUDIES] showcases how we've worked with businesses in overcoming [CHALLENGE_RELATED_TO_SERVICE/PRODUCT]. With our track record, I'm confident we could bring forward-thinking strategies to #salesGPT#<b>[PROSPECT_COMPANY]</b>#!salesGPT#.</p><p><span id="text-chat">I'd like to invite you to an #salesGPT#<b>exclusive chat</b>#!salesGPT# session where we will share a few strategies and ideas for [PROSPECT_COMPANY], and would love it if you could #salesGPT#<b>book a meeting</b>#!salesGPT# with me to discuss further.</span><span id="text-video">I've put together an #salesGPT#<b>exclusive video</b>#!salesGPT# where I share a few strategies and ideas for [PROSPECT_COMPANY], and would love to #salesGPT#<b>book a meeting</b>#!salesGPT# with you to discuss further.</span></p>[USER_SIGNATURE][DISCLAIMER]`,

  // w/o optional text
  `<p>Hi [PROSPECT_FIRST_NAME],</p><p></p><p>I've been closely following #salesGPT#<b>[PROSPECT_COMPANY]'s</b>#!salesGPT# impressive journey in the [PROSPECT_INDUSTRY]. Your commitment to [PROSPECT_COMPANY_MISSION]${
    /* and your role as a [PROSPECT_TITLE] based in [PROSPECT_LOCATION] certainly stands out */ ''
  } is impressive.</p><p>At #salesGPT#<b>[USER_COMPANY]</b>#!salesGPT#, we’re skilled in [STRONG_SELLING_POINTS]. Notably, our work with [USER_COMPANY_CASE_STUDIES] showcases how we've worked with businesses in overcoming [CHALLENGE_RELATED_TO_SERVICE/PRODUCT]. With our track record, I'm confident we could bring forward-thinking strategies to #salesGPT#<b>[PROSPECT_COMPANY]</b>#!salesGPT#.</p><p><span id="text-chat">I'd like to invite you to an #salesGPT#<b>exclusive chat</b>#!salesGPT# session where we will share a few strategies and ideas for [PROSPECT_COMPANY], and would love it if you could #salesGPT#<b>book a meeting</b>#!salesGPT# with me to discuss further.</span><span id="text-video">I've put together an #salesGPT#<b>exclusive video</b>#!salesGPT# where I share a few strategies and ideas for [PROSPECT_COMPANY], and would love to #salesGPT#<b>book a meeting</b>#!salesGPT# with you to discuss further.</span></p>[USER_SIGNATURE][DISCLAIMER]`,

  // `Hi [PROSPECT_FIRST_NAME],<br/><br/>We've been following #salesGPT#[PROSPECT_COMPANY]#!salesGPT# closely and are impressed by your commitment to [PROSPECT_COMPANY_MISSION]<br/><br/>[TIE_IN_TO_WHY_WE_ARE_REACHING_OUT]<br/><br/>At #salesGPT#[USER_COMPANY]#!salesGPT#, we specialize in [USER_SERVICE/PRODUCT_DESCRIPTION].<br/><br/>We even propose an #salesGPT#<b>exclusive consultation</b>#!salesGPT# where we can discuss [USER_EXPERTISE] strategies tailored specifically for [PROSPECT_COMPANY].<br/><br/>Hope you have a productive day ahead!<br/><br/>__________<br/>[USER_SIGNATURE]<br/><br/><br/>[DISCLAIMER]`,

  // `Hi [PROSPECT_FIRST_NAME],<br/><br/>We've been exploring #salesGPT#[PROSPECT_COMPANY]#!salesGPT# and we're impressed with [PROSPECT_COMPANY_EXPERTISE]. [PROSPECT_COMPANY_MISSION]<br/><br/> At #salesGPT#[USER_COMPANY]#!salesGPT#, we specialize in [USER_SERVICE/PRODUCT_DESCRIPTION].<br/><br/>We'd love to invite you to an exclusive consultation where we can discuss [${
  //   /*HOW_USER_SERVICE_BENEFITS_PROSPECT_COMPANY*/ 'USER_VALUE_PROPOSITION'
  // }]. Would you be interested in #salesGPT#<b>joining us for a chat</b>#!salesGPT#?<br/><br/>I hope you have an excellent rest of your week!<br/><br/>__________<br/>[USER_SIGNATURE]<br/><br/><br/>[DISCLAIMER]`,

  // `Hi [PROSPECT_FIRST_NAME],<br/><br/>  #salesGPT#[PROSPECT_COMPANY]#!salesGPT#, we've been following your recent news with high interest. It's fascinating to see [PROSPECT_COMPANY_MISSION].<br/><br/> At #salesGPT#[USER_COMPANY]#!salesGPT#, we understand the importance of [USER_SERVICE/PRODUCT_DESCRIPTION].<br/><br/>[${
  //   /*HOW_USER_SERVICE_BENEFITS_PROSPECT_COMPANY*/ 'USER_VALUE_PROPOSITION'
  // }]<br/><br/> We would love to propose an #salesGPT#<b>exclusive chat consultation</b>#!salesGPT# where we can discuss how [USER_COMPANY] can contribute to [USER_EXPERTISE].<br/><br/>We look forward to connecting with you!<br/><br/>__________<br/>[USER_SIGNATURE]<br/><br/><br/>[DISCLAIMER]`,
];

// const emailSections = {
//   Greeting: {
//     prompt: `Give me a short warm greeting for the prospect using PROSPECT INFORMATION.`,
//   },
//   Opening: {
//     prompt: `Write a personalized compliment based on PROSPECT INFORMATION.`,
//   },
//   Connection: {
//     prompt: `Add a statement, connecting the compliment with why the COMPANY is reaching out.`,
//   },
//   Why: {
//     prompt: `Concisely explain what products/services the company offers and how it could specifically help the prospect using COMPANY INFORMATION.`,
//   },
//   // Closing: {
//   //   prompt: `Write a closing sentence with a placeholder to replace it with a link to schedule a discussion call. Use the following notation for placeholder: [link]`,
//   // },
//   Expertise: {
//     prompt: `Describe the COMPANY expertise in it's industry using COMPANY INFORMATION.`,
//   },
//   Challenges: {
//     prompt: `Quickly explain the challenges that are solved by the products/services offered by company using COMPANY INFORMATION.`,
//   },
//   ValueProposition: {
//     prompt: `Give user value propostion for products/services offered by company using the PROSPECT INFORMATION and COMPANY INFORMATION`,
//   },
//   Description: {
//     prompt: `Writ a short description under 25 words using the Company Description provided in COMPANY INFORMATION.`,
//   },
//   ProductDescription: {
//     prompt: `Writ a short product description under 25 words using the Product/Service Description provided in COMPANY INFORMATION.`,
//   },
//   PMission: {
//     prompt: `Write a statement describing prospect's company mission using PROSPECT INFORMATION`,
//   },
//   PAchievement: {
//     prompt: `Highlight and quantify the prospect's achievements using PROSPECT INFORMATION`,
//   },
//   PExpertise: {
//     prompt: `Describe the knowledge and expertise of the prospect in it's industry using PROSPECT INFORMATION`,
//   },
//   PAreaOfImprovement: {
//     prompt: `Highlight the areas of improvement for prospect using PROSPECT INFORMATION`,
//   },
//   'P.S.': {
//     prompt: `Based on the location of the prospect mentioned in PROSPECT INFORMATION, provide a short closing remark involving weather or popular nearby places. Do not add any closing greet or sender signature at the end.`,
//   },
// };
const emailSections = {
  Greeting: {
    prompt: `Compose a brief and friendly greeting tailored to the prospect, incorporating relevant PROSPECT INFORMATION.`,
  },
  Opening: {
    prompt: `Craft a personalized compliment based on the provided PROSPECT INFORMATION.`,
  },
  // why
  Connection: {
    // prompt: `Create a statement that establishes a connection between the compliment and the reason why the COMPANY is reaching out.`,
    prompt: `Explain in brief why the prospect and company are an ideal match. Mention specific aspects from the PROSPECT INFORMATION that highlight the alignment of their goals with COMPANY's product/service and how it can benefit them.`,
  },
  // Why: {
  //   prompt: `Concisely explain the products/services offered by the company and how they can specifically benefit the prospect, utilizing COMPANY INFORMATION.`,
  // },
  // Closing: {
  //   prompt: `Compose a closing statement that encourages the prospect to take action, with a placeholder to replace it with a link to schedule a discussion call. Use the following notation for the placeholder: [link].`,
  // },
  Expertise: {
    prompt: `Describe the COMPANY's industry expertise, drawing from COMPANY INFORMATION.`,
  },
  CaseStudies: {
    // prompt: `Generate the testimonial for the company using the provided COMPANY INFORMATION. Provide only the testimonial content without any double quotes or attribution.`,
    prompt: `Create a brief and concise case study that spotlights a successful outcome or significant achievement by utilizing the "testimonials" provided in the COMPANY INFORMATION. The case study should provide a brief description of the positive result or impact.`,
  },
  Challenges: {
    prompt: `Provide a quick explanation of the challenges addressed by the company's products/services, supported by COMPANY INFORMATION.`,
  },
  ValueProposition: {
    prompt: `Offer the prospect a compelling value proposition for the company's products/services, integrating both PROSPECT INFORMATION and COMPANY INFORMATION.`,
  },
  Description: {
    prompt: `Write a concise description, containing no more than 25 words, using the Company Description provided in COMPANY INFORMATION.`,
  },
  ProductDescription: {
    prompt: `Craft a brief product/service description, under 25 words, utilizing the Product/Service Description provided in COMPANY INFORMATION.`,
  },
  PIndustry: {
    prompt: `Infer the industry name to which the prospect's company belongs, using the information in PROSPECT_INFORMATION.`,
  },
  PMission: {
    prompt: `Write a statement describing the prospect's company mission, drawing from PROSPECT INFORMATION.`,
  },
  PAchievement: {
    prompt: `Highlight and quantify the prospect's achievements, utilizing PROSPECT INFORMATION.`,
  },
  PExpertise: {
    prompt: `Describe the knowledge and expertise of the prospect in their industry, incorporating PROSPECT INFORMATION.`,
  },
  PAreaOfImprovement: {
    prompt: `Highlight the areas where the prospect can improve, based on PROSPECT INFORMATION.`,
  },
  'P.S.': {
    prompt: `Based on the prospect's location mentioned in PROSPECT INFORMATION, provide a brief closing remark involving weather or popular nearby places. Exclude any closing greetings or sender signatures.`,
  },
};

module.exports = {
  spamDict,
  introPitches,
  emailTemplates,
  templateVariables,
  emailSignature,
  disclaimerFormatted,
  options,

  getFolllowUpMail: ({ campaign, recipient, aiGen }) => {
    // {address, displayName} = recipient
    // {subject, body, conclusion} = aiGen

    const customEmail = {
      subject: `Follow Up`,
      body: `Hi ${recipient.displayName},
      <br/><br/>
      Just a friendly reminder !
      <br/><br/>
      Best regards,
      <br/>
      ${campaign.senderName}`,
    };

    return customEmail;
  },

  getSubjectFromTemps: ({ userOrgName }) => {
    const sub = subjectTemplates[random(subjectTemplates.length)];
    const modSub = sub.replace(
      new RegExp(`\\[USER_COMPANY\\]`, 'ig'),
      userOrgName,
    );
    return modSub;
  },

  getCustomEmail: ({
    aiGen,
    senderName,
    recipient,
    additionalInfo,
    userOrgName,
  }) => {
    // {address, displayName} = recipient
    // {subject, introPitch, body, conclusion} = aiGen
    // {campaignId, organization, firstName, lastName} = additionalInfo: clientInfo / leadInfo

    const customEmail = {
      subject: `${aiGen.subject}`, // [Generate Subject Line for the Email as per the Product Description]
      text: ``,
      body: `Hi ${recipient.displayName},
      <br/><br/>
      ${introPitches[random(introPitches.length)]}${
        /* `${aiGen.introPitch}` */ ''
      }
      <br/><br/>
      ${aiGen.body}
      <br/><br/>
      We would like to extend an invitation for you to explore <a href="${CUSTOM_GPT_URL}/${
        additionalInfo.campaignId
      }?leadEmail=${recipient.address}&fname=${
        additionalInfo.firstName
      }&lname=${
        additionalInfo.lastName
      }" target="_blank">${userOrgName}'s strategy session for ${
        additionalInfo.organization
      }</a>. Please let us know if you have any questions or concerns about the session.
      <br/><br/>
      ${aiGen.conclusion}
      <br/><br/>
      ____________________
      <br/>
      Best regards,
      <br/>
      ${senderName}
      <br/>
      ${userOrgName}
      <br/>
      <br/>
      Reply with 'No' to unsubscribe
      <br/>
      <img src="${EMAIL_TRACKING_SERVICE}/track/${
        recipient.address
      }?campaignId=${additionalInfo.campaignId}" height="1" width="1">`,
    };

    return customEmail;
  },

  getEmailFromSections: ({
    sections: rawTextSecs,
    link,
    leadId,
    userOrgName,
    senderName,
    userCompanyLocation,
  }) => {
    // {greeting, opening, connection, why, closing, p.s.} = sections
    const sections = { ...rawTextSecs };
    // Object.keys(rawTextSecs).forEach((key) => {
    //   sections[key] = rawTextSecs[key].replace(/\\n/, '</br>');
    // });

    const linkStr = `<a href=${link} target="_blank">Book your call</a>`;
    sections.closing = sections.closing.replace(
      new RegExp(/\[(here|link)\]/, 'ig'),
      linkStr,
    );

    const customEmail = {
      text: ``,
      // body: `${sections.greeting}<br/><br/>${sections.opening}<br/><br/>${sections.connection}<br/><br/>${sections.why}<br/><br/>${sections.closing}<br/><br/>${sections['p.s.']}<br/><br/><small style="color:gray"><a href=${frontEndURL}><b>B2B Rocket AI</b></a> sales agents have written & sent this email on behalf of ${userOrgName} as they've requested us to send this to you. If you no longer wish to receive these emails, simply unsubscribe to never hear from them again, respond to them here, or let us know if you find any errors in the AI content generation. We only process publicly available information limited to Name (first and last) and company information (location, the domain, website metadata such as technology, and any relevant keywords). We DO NOT track or store any of your personal information. Please visit our TLDR privacy policy if you want to understand more about how we process data. Our mission is to help businesses create opportunities, particularly helping small businesses to thrive with the help of AI-driven stories. We strictly comply with CCPA, GDPR, and CAN-SPAM laws. You can opt-out of our data processing anytime. We are <a href=${frontEndURL}>B2B Rocket</a>, located at 30 N Gould St Ste R Sheridan, WY 82801.</small>`,
      // body: `${sections.greeting}<br/><br/>${sections.opening}<br/><br/>${sections.connection}<br/><br/>${sections.why}<br/><br/>${sections.closing}<br/><br/>${sections['p.s.']}<br/><br/><p><small style="color:gray"><a href=${frontEndURL} title=${frontEndURL}>B2B Rocket</a> AI agents have written &amp; sent this email at the request of ${userOrgName}. If you do not wish to receive these emails, you can <a href=${frontEndURL}/unsubscribe title=${frontEndURL}/unsubscribe>unsubscribe</a> or respond to this email to never hear from them again. We only process publicly available information. We DO NOT track or store any of your personal information. Please visit our <a href="https://www.b2brocket.ai/legal/privacy-policy" title="https://www.b2brocket.ai/legal/privacy-policy">Privacy Policy</a> if you want to understand more about how we process and use data. Our mission is to help businesses book new meetings on autopilot and bring in new revenue using the power of AI. We strictly comply with CCPA, GDPR, and CAN-SPAM laws. You can <a href=${frontEndURL}/optout title=${frontEndURL}/optout >opt-out</a> of our data processing anytime. We are B2B Rocket, located at 30 N Gould St, Sheridan, WY 82801.</small></p>`,
      body: `${sections.greeting}<br/><br/>${sections.opening}<br/><br/>${
        sections.connection
      }<br/><br/>${sections.why}<br/><br/>${
        sections.closing
      }<br/><br/>--------------------<br/>Best Regards,<br/>${senderName}<br/>${userOrgName}<br/>${
        userCompanyLocation || '#UNKNOWN'
      }<br/><br/><br/>${disclaimerFormatted({ userOrgName, leadId, link })}`,
    };

    return customEmail;
  },

  // getEmailFromTemps: ({
  //   sections: rawTextSecs,
  //   link,
  //   userOrgName,
  //   senderName,
  //   userCompanyLocation,
  // }) => {
  //   const htmlBody = emailTemplates[random(emailTemplates.length)];
  //   const modBody = '';
  //   return modBody;
  // },

  // getTrackingPixel: ({ recipient, customerObj, emailId }) =>
  //   `<img src="${EMAIL_TRACKING_SERVICE}/track/${recipient.address}?campaignId=${customerObj.campaignId}&emailId=${emailId}" height="1" width="1">`,

  getTrackingPixel: ({ leadId, emailId }) =>
    `<img alt="" src="${API_SERVICE_BACKEND}/images/${leadId}?version=${emailId}" height="1" width="1">`,

  // sales GPT link
  getEmailStatusLink: async ({ customerObj, emailId }) => {
    const base = `${CUSTOM_GPT_URL}/private`;
    const link = `${base}/${customerObj.campaignId}/${customerObj.leadId}/${emailId}?pos=invitation`;
    // return link;

    // const shortenURLRes = await axios.get(`${API_SERVICE_BACKEND}/shorten`, {
    //   params: {
    //     url: encodeURI(link),
    //   },
    // });

    let urlDoc = await URLModel.findOne({ originalURL: link });
    if (!urlDoc) {
      const uniqueId = uuidv4();
      const uuidWithoutDashes = uniqueId.replace(/-/g, '');
      urlDoc = new URLModel({
        uniqueId: uuidWithoutDashes,
        originalURL: link,
      });
      urlDoc = await urlDoc.save();
    }
    const urlId = urlDoc.uniqueId;

    if (!urlId) {
      throw new Error('Error in generating short url');
    }
    return `${base}/${urlId}?pos=invitation`;
  },

  getGPT_Prompt: {
    subject: ({ productDesc }) =>
      `Generate a captivating subject line for a campaign email promoting our revolutionary product using the following product description, '${productDesc}'. Create a subject line that grabs attention, highlights the unique selling points, and entices recipients to open the email. Note: Keep it short and concise having no mare than 5 words`,

    body: ({ userOrgName, productDesc, campaignDesc }) =>
      `Generate a compelling email body for a campaign promoting a product from the company, "${userOrgName}". Craft an engaging email body that highlights the key benefits, and convinces recipients of our product's value to their business. Avoid using any greetings and focus solely on describing the product and its advantages. Use the campaign description and the product description provided to generate the content, campaign description: "${campaignDesc}", product description: "${productDesc}". Follow these instructions to complete the task: 1) Remember not to include any greetings, notes, disclaimers, or conclusions, and strictly keep the content short, ideally within 25 to 30 words.`,

    introPitch: ({ campaignDesc, productDesc, customerObj }) =>
      `Craft a one-liner compliment/greeting and compelling product pitch for a prospective customer using the following data - Product description: ${productDesc}, Customer details: ${JSON.stringify(
        customerObj,
      )}. Remember, the content must be short ideally not more than 1 line. Also don't add any additional helper texts or quotes around the generated content and just return the text.`,

    conclusion: ({
      userOrgName,
      customerObj,
    }) => `Write a short, personalized conclusion on behalf of the company "${userOrgName}" for a sales email to the customer.
       Add a lighthearted message based on the customer's address. Here is the customer data: ${JSON.stringify(
         customerObj,
       )}.
       Remember, no greetings needed in the beginning and the content must be short not more than 1 line`,

    replceSpammyWords: (body) =>
      `Rewrite the HTML provided by replacing any and all occurences of the following words: ${spamDict
        .map((word) => `"${word}"`)
        .join(
          ', ',
        )} in it with appropriate synonyms. Strictly don't alter the original html structure and formatting in any way and only output the modified html as a text string without any additional helper texts or quotes around it. HTML: "${body}"`,

    wholeEmail: ({
      userLink,
      senderName,
      senderTitle,
      userOrgName,
      recipient,
      campaignDesc,
      productDesc,
      customerObj,
    }) => {
      const address = [];
      if (customerObj?.address) {
        ['city', 'state', 'country'].forEach((key) => {
          if (customerObj.address[key]) {
            address.push(customerObj.address[key]);
          }
        });
      } else {
        address.push('#UNKNOWN');
      }

      // const link =
      //   userLink ||
      //   `${CUSTOM_GPT_URL}/${customerObj.campaignId}?leadEmail=${recipient.address}&fname=${customerObj.firstName}&lname=${customerObj.lastName}`;
      // const link = `${CUSTOM_GPT_URL}/${customerObj.campaignId}?leadEmail=${recipient.address}&fname=${customerObj.firstName}&lname=${customerObj.lastName}`;
      const link = `${CUSTOM_GPT_URL}/private/${customerObj.campaignId}/${customerObj.leadId}?pos=invitation`;

      return `I will give you example emails that we are looking to create by giving ChatGPT prompts. I want you to help me create an single email based on the given information similar to these examples. The goal is to use the information about the company to send cold emails to prospects using the prospect information with the goal of booking an intro call. Strictly keep the email short and concise and only return the email body in accordance with the email structure provided below wrapped inside a div tag with appropriate styling.

      COMPANY INFORMATION:
      Company Name – ${userOrgName}
      Representative Name – ${senderName}
      Representative Designation – ${senderTitle}
      Link for Booking Call – ${link}
      Company Description – ${campaignDesc + '\n' + productDesc}
      
      PROSPECT INFORMATION:
      First-Name – ${customerObj.firstName ? customerObj.firstName : '#UNKNOWN'}
      Last-Name – ${customerObj.lastName ? customerObj.lastName : '#UNKNOWN'}
      Title – ${customerObj.description ? customerObj.description : '#UNKNOWN'}
      Company – ${
        customerObj.organization ? customerObj.organization : '#UNKNOWN'
      }
      Location – ${address.join(',')}
      
      OTHER NOTES:
      If there is no specific news articles, reviews, or other information, DO NOT reference that. Instead, just provide a more general compliment. 
      
      
      EMAIL STRUCTURE:
      Greeting – Hi {{First-Name}},
      Opening – {{personalized compliment based on prospect information}}
      Connection – {{connecting the compliment with why we are reaching out}}
      Why – {{explaining what service/product we offer and how it could specifically help the prospect}}
      Closing – {{closing sentence with a link to schedule a discussion call by incorporating the link to book call provided under company information}}
      P.S. – {{based on the location of the person provide a short closing remark}}
      
      
      EXAMPLE EMAIL #1
      <div style="font-family: Arial, sans-serif; font-size: 14px; line-height: 1.6;">
        <p>Hi {{First Name}},</p>
        
        <p>We've taken note of {{Prospect's Company}}'s commitment to {{Prospect's Industry-specific compliment or achievement}}. The dedication you've demonstrated is truly remarkable.</p>
        
        <p>{{Your Company Name}} specializes in {{Your Service}}, and we are confident that this could significantly benefit {{Prospect's Company}} by enhancing your {{specific area to improve}}.</p>
        
        <p>I'd like to invite you for a discussion on how <a href={{Link for Booking Call}} target="_blank">{{Your Company Name}}</a> can contribute to your goals. Are you available for a chat? </p>
        
        <p>By the way, I hope you're enjoying the {{weather or location-specific remark}} in {{Location}}.</p>
      
        <p>
          Best Regards,
          <br />
          {{Your Company Representative Name}}
          <br />
          {{Your Company Representative Designation}}
        </p>
      </div>
      
      
      EXAMPLE EMAIL #2
      <div style="font-family: Arial, sans-serif; font-size: 14px; line-height: 1.6;">
        <p>Hello {{First Name}},</p>
        
        <p>I recently came across {{Prospect's Company}} and your commitment to {{Prospect's Industry-specific compliment or achievement}} caught my attention. It is indeed inspiring.</p>
        
        <p>We, at {{Your Company Name}}, specialize in {{Your Service}}. I firmly believe our offerings could provide significant value to {{Prospect's Company}} by boosting your {{specific area to improve}}.</p>
        
        <p>Would you be open to a conversation to discuss how <a href={{Link for Booking Call}} target="_blank">{{Your Company Name}}</a> can help achieve your objectives? </p>
        
        <p>P.S. I trust you're making the most of the {{weather or location-specific remark}} in {{Location}}.</p>
      
        <p>
          Kind Regards,
          <br />
          {{Your Company Representative Name}}
          <br />
          {{Your Company Representative Designation}}
        </p>
      </div>


      EXAMPLE EMAIL #3
      <div style="font-family: Arial, sans-serif; font-size: 14px; line-height: 1.6;">
        <p>Dear {{First Name}},</p>
        
        <p>I'm genuinely impressed by {{Prospect's Company}}'s dedication to {{Prospect's Industry-specific compliment or achievement}}. Your commitment to excellence is clearly evident.</p>
        
        <p>At {{Your Company Name}}, our focus is on {{Your Service}}, and we believe our expertise could contribute significantly to {{Prospect's Company}}, particularly in enhancing your {{specific area to improve}}.</p>
        
        <p>I'd welcome the opportunity to discuss how <a href={{Link for Booking Call}} target="_blank">{{Your Company Name}}</a> could support your goals. Are you available for a call?</p>
        
        <p>By the way, I hope the {{weather or location-specific remark}} in {{Location}} is to your liking.</p>
      
        <p>
          Best,
          <br />
          {{Your Company Representative Name}}
          <br />
          {{Your Company Representative Designation}}
        </p>
      </div>
      

      EXAMPLE EMAIL #4
      <div style="font-family: Arial, sans-serif; font-size: 14px; line-height: 1.6;">
        <p>Hi {{First Name}},</p>
        
        <p>Your company, {{Prospect's Company}}, has caught our attention with its commitment to {{Prospect's Industry-specific compliment or achievement}}. The level of dedication is indeed commendable.</p>
        
        <p>Our team at {{Your Company Name}} specializes in {{Your Service}} and we are confident that our services could be a great asset for {{Prospect's Company}} in boosting your {{specific area to improve}}.</p>
        
        <p>I'd love to schedule a chat with you to explore how <a href={{Link for Booking Call}} target="_blank">{{Your Company Name}}</a> can align with your objectives. Does that sound good to you?</p>
        
        <p>P.S. I hope you're enjoying the lovely {{weather or location-specific remark}} in {{Location}}.</p>
      
        <p>
          Best Wishes,
          <br />
          {{Your Company Representative Name}}
          <br />
          {{Your Company Representative Designation}}
        </p>
      </div>
      
      
      EXAMPLE EMAIL #5
      <div style="font-family: Arial, sans-serif; font-size: 14px; line-height: 1.6;">
        <p>Hello {{First Name}},</p>
        
        <p>It's hard not to notice {{Prospect's Company}}'s impressive dedication to {{Prospect's Industry-specific compliment or achievement}}. It's truly inspiring.</p>
        
        <p>We at {{Your Company Name}}, specialize in {{Your Service}}, and we believe that our offerings could significantly contribute to {{Prospect's Company}} by enhancing your {{specific area to improve}}.</p>
        
        <p>Would you be available for a discussion to explore how <a href={{Link for Booking Call}} target="_blank">{{Your Company Name}}</a> can support your objectives? </p>
        
        <p>P.S. I trust you're taking advantage of the {{weather or location-specific remark}} in {{Location}}.</p>
      
        <p>
          Warm Regards,
          <br />
          {{Your Company Representative Name}}
          <br />
          {{Your Company Representative Designation}}
        </p>
      </div>

      
      ${
        /*`Here's a few examples of the HTML formatted emails:

      EXAMPLE #1

      <div style="font-family: Arial, sans-serif; font-size: 14px; line-height: 1.6;">
          <p>
              Hi Joshua,
          </p>
          <p>
              We've taken note of ChargeItSpot's commitment to the industry, and your role as VP of Business Development is truly remarkable.
          </p>
          <p>
              B2B Rocket is an AI-driven email marketing agency that automates B2B meeting scheduling by sending hyper-personalized emails to ideal prospects, leading to increased revenue opportunities.
          </p>
          <p>
              Our cutting-edge AI technology crafts personalized emails for targeted prospects like yourself, ensuring higher engagement rates. The direct inbox delivery bypasses spam filters, increasing the likelihood of a response. Additionally, meetings can be scheduled with target prospects within 30 seconds, streamlining the sales process and saving time for both parties. To top it off, we offer a free call consultation to discuss how our service can elevate business growth and generate new revenue streams for you.
          </p>
          <p>
              I'd like to invite you for a discussion on how <a href="https://react-sales-agent.vercel.app/chat/64bf8a11eb67ae4504b1f7a9?leadEmail=anonymous&fname=NA&lname=NA" target="_blank">B2B Rocket</a> can contribute to ChargeItSpot's goals. Are you available for a chat?
          </p>
          <p>
              Best Regards,
              <br>
             Oliver M.
          </p>
          <p>
              P.S. I hope you're enjoying Philadelphia, PA, and its unique charm.
          </p>
      </div>

      
      EXAMPLE #2
      
      <div style="font-family: Arial, sans-serif; font-size: 14px; line-height: 1.6;">
    <p>
        Hi Shivanshu,
    </p>
    <p>
        Your company, Leadingly, has caught our attention with its commitment to generating automated leads and boost sales. The level of dedication is indeed commendable.
    </p>
    <p>
        PyC specializes in AI-based strategies that deliver highly accurate and personalized email campaigns, resulting in increased leads, reduced no-shows, and more closed deals for your team.
    </p>
    <p>
        Our unique AI-based strategy is incredibly accurate and precise, leading to high email deliverability and a level of personalization that is unmatched. The results are increased leads, reduced no-shows, and more closed deals for your team.
    </p>
    <p>
        I'd like to invite you for a discussion on how <a href="https://react-sales-agent.vercel.app/chat/64bf8a11eb67ae4504b1f7a9?leadEmail=shiva@leadingly.io&fname=Shivanshu&lname=Gupta" target="_blank">PyC</a> can contribute to Leadingly's goals. Are you available for a chat?
    </p>
    <p>
        Best Regards,
        <br>
        Oliver M.
    </p>
    <p>
        P.S. I hope you're enjoying the location you're currently in.
    </p>
    </div>`*/ ''
      }`;
    },

    emailSections: ({
      userLink,
      senderName,
      userOrgName,
      recipient,
      campaignDesc,
      productDesc,
      testimonials,
      customerObj,
      emailTemplateIndex,
    }) => {
      const address = [];
      if (customerObj?.address) {
        ['city', 'state', 'country'].forEach((key) => {
          if (customerObj.address[key]) {
            address.push(customerObj.address[key]);
          }
        });
      } else {
        address.push('#UNKNOWN');
      }

      const emailTemplate = emailTemplates[emailTemplateIndex];

      // const link = userLink || `${CUSTOM_GPT_URL}/private/${customerObj.campaignId}/${customerObj.leadId}?pos=invitation`;

      const sectionsArray = Object.keys(emailSections);

      // return `I will give you prompts for each of the following sections: **Greeting**, **Opening**, **Connection**, **Why**, **Closing** and **P.S.**. I want you to help me write these sections for an email based on the given prompts, so that I can easily replace the information and have a new unique personalized email based on the new information. The goal is to use the information about the company to send cold emails to prospects using the prospect information with the goal of booking an intro call. Be careful do not add any placeholders unless explicitly specified otherwise.

      // SENDER INFORMATION:
      // Name: ${senderName}
      // Title: #UNKNOWN

      // COMPANY INFORMATION:
      // Company Name – ${userOrgName}
      // Company Description – ${campaignDesc}
      // Product/Service Description – ${productDesc}

      // PROSPECT INFORMATION:
      // First Name – ${customerObj.firstName ? customerObj.firstName : '#UNKNOWN'}
      // Last Name – ${customerObj.lastName ? customerObj.lastName : '#UNKNOWN'}
      // Title – ${customerObj.description ? customerObj.description : '#UNKNOWN'}
      // Company – ${
      //   customerObj.organization ? customerObj.organization : '#UNKNOWN'
      // }
      // Location – ${address.join(',')}
      // ${
      //   /*'Industry – secure cell phone charging stations for retail stores'*/ ''
      // }

      // **Greeting**
      // - Prompt: Give me a short warm greeting for the prospect using PROSPECT INFORMATION.

      // **Opening**
      // - Prompt: Write a personalized compliment based on PROSPECT INFORMATION.

      // **Connection**
      // - Prompt: Add a statement, connecting the compliment with why the company is reaching out.

      // **Why**
      // - Prompt: Concisely explain what products/services the company offers and how it could specifically help the prospect using COMPANY INFORMATION.

      // **Closing**
      // - Prompt: Write a closing sentence with a placeholder to replace it with a link to schedule a discussion call. Use the following notation for placeholder: [link]

      // **P.S.**
      // - Prompt: Based on the location of the prospect mentioned in PROSPECT INFORMATION, provide a short closing remark involving weather or popular nearby places. Do not add any closing greet or sender signature at the end.

      // OUTPUT FORMAT to follow:

      // **Greeting**
      // {{output}}

      // **Opening**
      // {{output}}

      // **Connection**
      // {{output}}

      // **Why**
      // {{output}}

      // **Closing**
      // {{output}}

      // **P.S.**
      // {{output}}`;

      // add blacklist!
      // I want you to help me write these sections for an email based on the given prompts, so that I can easily replace the placeholders (placeholders are represented by [section_name]) in template and have a new unique personalized email based on the new information. Write the sections without using any of the words listed in the provided BLOCK LIST and such that they can be filled into the EMAIL TEMPLATE seamlessly wihout encountering any grammatical erorrs or affecting it's readabilty.Please ensure that the generated text is coherent and meaningful, while strictly adhering to the restriction of avoiding the use of any words from the blocklist. The goal is to use the information about the company to send cold emails to prospects using the prospect information with the goal of booking an intro call. Be careful do not add any placeholders unless explicitly specified otherwise.

      // BLOCK LIST:
      // ${spamDict}

      return `I will give you the EMAIL TEMPLATE that i want to use as well as prompts for each of the following sections: ${sectionsArray
        .map((section) => `**${section}**`)
        .join(
          ', ',
        )}. I want you to help me write these sections for an email based on the given prompts, so that I can easily replace the placeholders (placeholders are texts enclosed within square brackets) in template and have a new unique personalized email based on the new information. Write the sections such that they can be filled into the EMAIL TEMPLATE seamlessly wihout encountering any grammatical erorrs or affecting it's readabilty. The goal is to use the information about the company to send cold emails to prospects using the prospect information with the goal of booking an intro call. Be careful do not add any placeholders unless explicitly specified otherwise. Make sure to write each of the sections concisely in under 20 words.

      EMAIL TEMPLATE:
      ${emailTemplate}

      SENDER INFORMATION:
      Name: ${senderName}
      Title: #UNKNOWN

      COMPANY INFORMATION:
      Company Name – ${userOrgName}
      Company Description – ${htmlToText(campaignDesc)}
      Product/Service Description – ${productDesc}
      Testimonials – ${testimonials}

      PROSPECT INFORMATION:
      First Name – ${customerObj.firstName ? customerObj.firstName : '#UNKNOWN'}
      Last Name – ${customerObj.lastName ? customerObj.lastName : '#UNKNOWN'}
      Title – ${customerObj.description ? customerObj.description : '#UNKNOWN'}
      Company – ${
        customerObj.organization ? customerObj.organization : '#UNKNOWN'
      }
      Location – ${address.join(',')}
      ${
        /*'Industry – secure cell phone charging stations for retail stores'*/ ''
      }

      ${Object.entries(emailSections)
        .map(([key, value]) => `**${key}**\n-Prompt: ${value.prompt}`)
        .join('\n\n')}

      OUTPUT FORMAT to follow:

      ${sectionsArray
        .map((section) => `**${section}**\n{{output}}`)
        .join('\n\n')}`;

      // return `I will give prompts for each of the following sections: ${sectionsArray
      //   .map((section) => `**${section}**`)
      //   .join(
      //     ', ',
      //   )}. I want you to help me write these sections for an email based on the given prompts, so that I can easily replace information and have a new unique personalized email based on the new information. The goal is to use the information about the company to send cold emails to prospects using the prospect information with the goal of booking an intro call. Be careful do not add any placeholders unless explicitly specified otherwise.

      // SENDER INFORMATION:
      // Name: ${senderName}
      // Title: #UNKNOWN

      // COMPANY INFORMATION:
      // Company Name – ${userOrgName}
      // Company Description – ${campaignDesc}
      // Product/Service Description – ${productDesc}

      // PROSPECT INFORMATION:
      // First Name – ${customerObj.firstName ? customerObj.firstName : '#UNKNOWN'}
      // Last Name – ${customerObj.lastName ? customerObj.lastName : '#UNKNOWN'}
      // Title – ${customerObj.description ? customerObj.description : '#UNKNOWN'}
      // Company – ${
      //   customerObj.organization ? customerObj.organization : '#UNKNOWN'
      // }
      // Location – ${address.join(',')}
      // ${
      //   /*'Industry – secure cell phone charging stations for retail stores'*/ ''
      // }

      // ${Object.entries(emailSections)
      //   .map(([key, value]) => `**${key}**\n-Prompt: ${value.prompt}`)
      //   .join('\n\n')}

      // OUTPUT FORMAT to follow:

      // ${sectionsArray
      //   .map((section) => `**${section}**\n{{output}}`)
      //   .join('\n\n')}`;
    },

    improveEmail: ({ emailTemplate, emailBody, spamList: spamListStr }) => {
      //   return `I will give you an email as html string that we are looking to send to our customers. I want you to help me examine and rewrite this email so that it does not contain any semantic or grammatical errors. Rephrase the sentences whereever required to make them meaningful while making minimal changes to provided content. Do not add any placeholders or unnecessary words in the process. The goal is to use this to send cold emails to prospects with the goal of booking an intro call. Retain the html formatting of the email provided and return the improvised email as an html string without affecting the embedded links. Do not add any helper text in the output, strictly only return the modified html email string wrapped inside a html 'div' tag.
      // HTML EMAIL STRING:
      // ${emailBody}`;

      // // currently in use
      // return `Given an email in HTML format, which may contain semantic or grammatical errors, your goal is to rewrite and improve the email so that it is error-free, coherent, and appealing to potential customers while ensuring it does not exceed the limit of 130 words. Please avoid using any monetary terms or discussing financial matters. Retain the HTML formatting of the provided email and ensure that embedded links remain intact. Do not add any placeholders or unnecessary words during the rewriting process.
      // Wrap the modified HTML email within an HTML 'div' tag, and return it as an HTML string.

      // Email to be improved and rewritten (in HTML format):
      // ${emailBody}

      // Output:
      // [Your improved HTML email under 130 words and enclosed within a 'div' tag]`;

      // currently in use modified
      return `Given an email in HTML format, which may contain semantic or grammatical errors, your goal is to rewrite and improve the email so that it is error-free, coherent, and appealing to potential customers. Limit the word count to a maximum of 140 words while preserving the essential message. Please avoid using any monetary terms or discussing financial matters. Retain the HTML formatting of the provided email and ensure that embedded links remain intact. Do not add any placeholders or unnecessary words during the rewriting process.
      Wrap the modified HTML email within an HTML 'div' tag, and return it as an HTML string.

      Email to be improved and rewritten (in HTML format):
      ${emailBody}

      Output:
      [Your improved HTML email under 130 words and enclosed within a 'div' tag]`;

      // // with email template
      // return `Given the HTML email template where variables are enclosed within square brackets, for example, [VARIABLE_NAME]:
      // ${emailTemplate}

      // And the HTML email generated from template with variable values filled in:
      // ${emailBody}

      // Refine the generated HTML email, ensuring grammatical correctness, coherence, and a natural tone. Limit the revised version to a maximum of 140 words while preserving the essential message. Please avoid using any monetary terms or discussing financial matters. Do not alter the HTML structure of the generated email and ensure that all embedded links remain unchanged. Provide the refined HTML email without any annotations or additional helper texts.

      // Output:
      // [Your refined HTML email under 140 words]
      // `;

      // // without email template
      // return `Given the HTML email:
      // ${emailBody}

      // Refine the generated HTML email, ensuring grammatical correctness, coherence, and a natural tone. Limit the revised version to a maximum of 140 words while preserving the essential message. Please avoid using any monetary terms or discussing financial matters. Do not alter the HTML structure of the generated email and ensure that all embedded links remain unchanged. Provide the refined HTML email without any annotations or additional helper texts.

      // Output:
      // [Your refined HTML email under 140 words]`;

      // // with blackList
      // return `Given an email in HTML format, which may contain semantic or grammatical errors, your goal is to rewrite without using any of the words listed in the provided BLOCK LIST and improve the email so that it is error-free, coherent, and appealing to potential customers. Please ensure to do this while strictly adhering to the restriction of avoiding the use of any words from the BLOCK LIST. Retain the HTML formatting of the provided email and ensure that embedded links remain intact. Do not add any placeholders or unnecessary words during the rewriting process.
      // Wrap the modified HTML email within an HTML 'div' tag, and return it as an HTML string.
      // BLOCK LIST:
      // ${spamListStr}
      // Email to be improved (in HTML format):
      // ${emailBody}
      // Output:
      // [Your improved HTML email within a 'div' tag]`;

      //   return `Improve the provided email content, which is given as an HTML string, for sending cold emails to potential customers with the objective of booking an introductory call. Your task is to examine the email, correcting any semantic or grammatical errors while making minimal changes to the original content. Preserve the original HTML formatting of the email and ensure that embedded links remain unchanged. Do not add any placeholders or extraneous words during the revision process.
      // Please return the modified HTML email content wrapped inside an HTML 'div' tag. The final output should strictly consist of the improved email content, maintaining the original structure and links. Avoid including any additional helper text or elements in the output.`;
    },

    getSpamAlternatives: ({ spamList, emailBody }) => {
      return `Generate at least 10 non-spammy alternatives for each of the word/phrase specified in the list of spammy words/phrases by cross-referencing publicly available lists of spam words from the web. The goal is to ensure that the provided email remains free from spammy words and doesn't trigger Gmail's spam filters. Please generate suitable alternatives for each of the spam words, making sure they seamlessly replace the spammy words in the provided email. The alternatives should fit naturally, maintain coherence, and exhibit grammatical accuracy within the context of the email content.

      **Special Caution:** Avoid suggesting alternatives in monetary terms and strictly avoid mentioning any specific amount or currency.
      ${
        /*`Absolutely avoid the use of monetary terms and make it a priority to eliminate any mention of specific amounts or currencies.`*/ ''
      }

      List of spammy words/phrases:
      ${spamList.map((word) => `"${word}"`).join(', ')}
      
      Email Content:
      ${emailBody}
      
      Deliver the result in JSON format, with spam words serving as keys and arrays of 10 alternatives as their corresponding values.`;
    },

    removeSpam: ({ emailBody, spamAlt }) => {
      // with blackList
      return `Given an email in HTML format, your primary task is to ensure that every string specified in the list of strings is replaced with its alternative in the email. This means that each and every occurrence of the strings in the list should be replaced with their alternatives within the email. Ensure that the replacements maintain the email's overall coherence and grammatical accuracy. Retain the HTML formatting of the provided email and ensure that embedded links remain intact. Do not add any placeholders or unnecessary strings during the rewriting process.

      **Important Note**: Ensure that each and every occurence of a string in the email is replaced with its alternative without exception.

      List of strings to be replaced with their corresponding alternatives:
      ${JSON.stringify(spamAlt)}
      
      Email to be improved and rewritten (in HTML format):
      ${emailBody}
      
      Output:
      [Your improved HTML email]
      `;
    },

    // removeSpam: ({ emailBody, spamList: spamListStr }) => {
    //   const defaultSpamList = ['free'];

    //   // with blackList
    //   return `Within the given email in HTML format, your task is to rephrase it while ensuring that none of the words from the provided BLOCK LIST are used. Your aim is to maintain coherence throughout the email while strictly adhering to the restriction of avoiding any words from the BLOCK LIST. Please retain the HTML formatting of the provided email and ensure that embedded links remain intact. The final result should consist solely of the email content, free from any words present in the BLOCK LIST, while preserving the original structure and links. Do not introduce any additional helper text or elements in the output.
    //   BLOCK LIST:
    //   ${spamListStr}, ${defaultSpamList.join(', ')}
    //   Email to be improved (in HTML format):
    //   ${emailBody}
    //   Output:
    //   [Your improved HTML email within a 'div' tag]`;

    //   // return `Within the HTML-formatted email provided, your objective is to rewrite it while strictly avoiding the use of words listed in the provided BLOCK LIST. The paramount aim is to ensure the email retains its coherence, even while adhering meticulously to the prohibition of employing BLOCK LIST words. Maintain the HTML formatting of the email, including embedded links. The final outcome should exclusively comprise the email content, devoid of any BLOCK LIST words, while preserving the original structure and links. Avoid the inclusion of any additional text or elements in the output.

    //   // BLOCK LIST:
    //   // ${spamListStr}, ${defaultSpamList.join(', ')}

    //   // Email to be improved (in HTML format):
    //   // ${emailBody}

    //   // Output:
    //   // [Your improved HTML email within a 'div' tag]`;
    // },
  },
};

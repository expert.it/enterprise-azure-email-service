class Filter {
  static regexChars = [
    '^',
    '$',
    '.',
    '|',
    '?',
    '*',
    '+',
    '(',
    ')',
    '[',
    ']',
    '{',
    '}',
  ];

  constructor() {
    this.blockList = [];
  }

  setBlockList(blockList) {
    this.blockList = blockList.split(',').map((word) => {
      word = word.trim().toLowerCase();
      word = String.raw`${word}`;
      regexChars.forEach((char) => {
        word = word.replace(char, `\\${char}`);
      });
      return word;
    });
  }

  addWord(word) {
    this.blockList.push(word);
  }

  removeWord(word) {
    const index = this.blockList.indexOf(word);
    if (index >= 0) {
      this.blockList.splice(index, 1);
    }
  }

//   getSpamList = ({ email }, options = {}) => {
//     const { ignoreList = [], isHTML = false } = options;
//     if (isHTML) email = htmlToText(email);
//     let spamList = [];
  
//     spamPatterns.forEach((pattern) => {
//       spamList.push(...(email.match(new RegExp(pattern, 'ig')) || []));
//     });
  
//     spamListArr.forEach((spamWord) => {
//       // const pattern = new RegExp(`\\b\\w*${spamWord}\\w*\\b`, 'ig');
//       const pattern = new RegExp(
//         `(?<=^|\\b|\\s|[,.()])${spamWord}(?=\\b|\\s|[,.()]|$)`,
//         'ig',
//       );
//       // console.log(pattern);
//       let matches = email.match(pattern) || [];
//       // matches.length && console.log(matches);
//       // if (spamBool) {
//       spamList.push(...matches.map((match) => match.trim()));
//       // }
//     });
//     spamList = spamList.filter((word) => Boolean(word?.length));
//     spamList = spamList.filter((word) => !ignoreList.includes(word));
//     return Array.from(new Set(spamList));
//   };
}

module.exports = Filter;
